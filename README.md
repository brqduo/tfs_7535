## Utilizacao do enviroment

Para rodar o component de enviroment rode "npm i cross-env -g" 

para utilizar, inclua a instrução cross-env ENV=tst antes do comando. Os ambientes podem ser tst, prd, hom e dsv
Ex: 
cross-env ENV=tst ionic cordova prepare ios  -- --buildFlag="-UseModernBuildSystem=0" 
cross-env ENV=tst ionic serve
Para utilizar em outros fontes deve se levar o arquivo \scripts\setEnvironment.js
Referência: https://scotch.io/@chaitanyamankala/adding-environment-for-ionic-23-projects

