npm install --unsafe-perm=true
cd platforms/ios
pod install --repo-update
cd ../..
cross-env ENV=$1 ionic cordova prepare ios -- --buildFlag="-UseModernBuildSystem=0" 
if [ "$?" != "0" ]; then
  echo "Erro ao compilar. Não prosseguirá com a distribuição" 1>&2
  cd ..
  exit 1
fi
cd automatic-build
# export FASTLANE_APPLE_APPLICATION_SPECIFIC_PASSWORD=xxxxxxxx (foi definido no bash)
#fastlane betaios
fastlane enterprise
cd ..