import { MsgModalPage } from './msg-modal';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared/shared.module';

@NgModule({
  declarations: [
  
  ],
  imports: [
    IonicPageModule.forChild(MsgModalPage),
    SharedModule
  ],
  exports: [

  ] 
})
    
export class MsgModalPageModule { }
