import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-msg-modal',
  templateUrl: 'msg-modal.html'
})
export class MsgModalPage {
  title: string;
  message: string;
  color: string;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.title = params.get('title');
    this.message = params.get('message');
    this.color = params.get('color');
    console.log(this);
  }
 
  dismiss() {
    this.viewCtrl.dismiss();
  }
  
}
