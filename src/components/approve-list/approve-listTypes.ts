
export class ApproveListPermissions {
    canApprove: boolean;
    canReprove: boolean;
    canList: boolean;
}   