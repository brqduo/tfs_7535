import { Component, Input, Output, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { ApproveListPermissions } from './approve-listTypes';
import { List } from "ionic-angular";

@Component({
    selector: 'approve-list',
    templateUrl: 'approve-list.html'
})
export class ApproveList {
    @Input() items: any[];
    @Input() templateHeader;
    @Input() templateExpanded;
    @Input() itemExpandHeight;
    @Input() actionDelay;
    @Input() permissions = new ApproveListPermissions();
    @Input() mainColor = 'black';
    @Output() approveReproveAction: EventEmitter<{ item: any, approved: boolean }> = new EventEmitter();
    @Output() expanded: EventEmitter<{ item: any, expanded: boolean }> = new EventEmitter();
    @Output() canceled: EventEmitter<any> = new EventEmitter();
    @Input() beforeReprove: Function;

    @ViewChild(List) expandList: List;
    private intervalHourGlass;
    private tempoHourGlass;
    private maxHourGlass;
 



    constructor() {
    }

    ngOnInit() {

        this.intervalHourGlass = 250; // intervalo (ms) entre atualizacoes na tela
        this.tempoHourGlass = this.actionDelay; // tempo em segundos de exibicao antes de efetivar operacao
        this.maxHourGlass = (1000 / this.intervalHourGlass) * this.tempoHourGlass; // qtd de intervalos
    }
    ngOnDestroy() {

        if (this.items)
            this.items.forEach(element => {
                if (element && element.inAction) {
                    this.cancel(element);
                }
            });
    }

    /* expande/contrai o componente */

    expandItem(item) {
        this.items.map(listItem => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                this.expanded.emit({ "item": item, "expanded": item.expanded });
            } else {
                listItem.expanded = false;
            }

        });
    }

    /* reformata contadores na troca das listas */

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["items"] && changes["items"].currentValue != changes["items"].previousValue) {
            this.items = changes["items"].currentValue;
            this.configureItensStyle(this.items);
        }
    }
    configureItensStyle(value: any[]) {
     
        value.forEach(item => {
            // CardBlock 
            item.show = false as Boolean;
            item.backColorCard = null as string;
            item.LetterColor = null as Object;
            item.showText = null as string;
            // SVG countDown
            if (this.maxHourGlass > 1)
                item.max = this.maxHourGlass;
            else
                item.max = 40;
            item.radius = 40;
            item.color = "#ffffff";
            item.background = "transparent";
            item.stroke = 5;
            item.clockwise = false;
            item.animation = "linearEase";
            item.interval = null;
            if (!item.mainColor)
                item.mainColor = this.mainColor;

        });
        this.itemExpandHeight = value;
    }

    /* formata contador para aprovar/reprovar */

    onApproveReprove(item, approved: boolean) {

        if (approved) {
            this.runApprove(item)
        }
        else {
            if (this.beforeReprove) {

                this.beforeReprove(item).subscribe(
                    resp => {
                        item = resp;
                        this.runReprove(item);
                    },
                    err => {
                        console.log(err);
                    }
                )

            }
            else {
                this.runReprove(item);
            }
        }
    }
    activeExternalApproveReprove(item) {
        //precisa aguardar pela tela atualizar para aplicar o efeito do relogio
        setTimeout(() => {
            this.items.forEach(element => {
                if (element.id == item.id) {
                    for(var k in item) element[k]=item[k];
                    if (item.Approve){

                        this.runApprove(element);
                    }
                    else{
                
                        this.runReprove(element);
                    }
                }
            }
            );


       }, 1000);
    }
    runApprove(item) {
    
        item.expanded = false;
        item.inAction = true;
        item.busy = false;
        item.showText = "Aprovação";
        //item.LetterColor = { color: "#8ca58c" }; /* para que serve? */
        item.backColorCard = { 'background-color': '#486018' }; // antigo '#60823d'
        item.acao = "aprovar";
        this.holdCard(item, true);

    }
    runReprove(item) {
    
        item.expanded = false;
        item.inAction = true;
        item.busy = false;
        item.showText = "Reprovação";
        //item.LetterColor = { 'color': '#aba9a9' } /* para que serve ? */
        item.backColorCard = { 'background-color': '#d10429' }; // antigo '#e88195'
        item.acao = "reprovar";
        this.holdCard(item, false);
    }
    /* mostra contador aguardando tempo determinado */

    holdCard(itemSelected, approved: boolean) {
        itemSelected.current = this.maxHourGlass;
        console.log("Aqui em Ativar>", itemSelected);
        itemSelected.show = true;
        this.expandList.closeSlidingItems();

        itemSelected.interval = setInterval(() => {
            console.log("Em interval");
            itemSelected.current = itemSelected.current - 1;
            if (itemSelected.current <= 0) {
                console.log("Em interval 2");
                /* obs: reformata contador ~ configurarEstiloItens() mantendo showText e interval */
                this.expandList.sliding = true;
                itemSelected.show = false;
                itemSelected.backColorCard = null;
                itemSelected.LetterColor = null;
                itemSelected.current = this.maxHourGlass;
                itemSelected.max = this.maxHourGlass;
                /* redundante... */
                //itemSelected.color = "#ffffff";
                //itemSelected.background = "transparent";
                //itemSelected.stroke = 5;
                //itemSelected.clockwise = false;
                //itemSelected.animation = "linearEase";
                clearInterval(itemSelected.interval);
                this.approveReproveAction.emit({ "item": itemSelected, "approved": approved });
                itemSelected.inAction = false;
            }
        }, this.intervalHourGlass);
    }

    /* cancela espera e esconde contador */

    cancel(itemSelected) {
        // this.expandList.sliding = true;
        console.log("Aqui em cancelar>", itemSelected);
        clearInterval(itemSelected.interval);
        /* obs: reformata contador ~ configurarEstiloItens() */
        itemSelected.show = false;
        itemSelected.inAction = false;
        itemSelected.backColorCard = null;
        itemSelected.LetterColor = null;
        itemSelected.current = this.maxHourGlass;
        itemSelected.max = this.maxHourGlass;

        this.canceled.emit(itemSelected);
    }

}