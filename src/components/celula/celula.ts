import { Component, Input } from '@angular/core';

@Component({
    selector: 'celula',
    templateUrl: 'celula.html'
})
export class CelulaComponent {

    @Input() templateCelula;
    @Input() templateHeader;
    @Input() item;
    @Input() page;
    @Input() color: string = '#A6A6A6';

    constructor() {

    }

}
