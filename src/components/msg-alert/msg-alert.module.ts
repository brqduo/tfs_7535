import { MsgAlertPage } from './msg-alert';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared/shared.module';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(MsgAlertPage),
    SharedModule
  ],
  exports: [
  
  ] 
})
    
export class MsgAlertPageModule { }
