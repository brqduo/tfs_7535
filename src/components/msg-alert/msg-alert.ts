import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-msg-alert',
  templateUrl: 'msg-alert.html'
})
export class MsgAlertPage {
  title: string;
  message: string;
  color: string;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.title = params.get('title');
    this.message = params.get('message');
    this.color = params.get('color');
  }
 
  dismiss() {
    this.viewCtrl.dismiss();
  }
  
}
