import {
  Component,
  Input,
  ViewChild,
  Output,
  EventEmitter  
} from "@angular/core";
import { ItemSliding } from "ionic-angular";
import { AproveItem } from "./approve-itemtypes";

/**
 * Generated class for the ApproveItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "approve-item",
  templateUrl: "approve-item.html"
})
export class ApproveItemComponent{
  @Input() id;  
  @Input() item: any;
  @Input() actionDelay;
  @Input() permissions;

  @Output() approveReproveAction: EventEmitter<{item: any;approved: boolean;    }> = new EventEmitter();
  @Output() canceled: EventEmitter<any> = new EventEmitter();

  @ViewChild(ItemSliding) idItemSliding: ItemSliding;

  
  busy: boolean;
  selected: boolean;    
  private approveItem: AproveItem;

  constructor() 
  {        
  }

  ngOnInit() {
    this.approveItem = new AproveItem(this.actionDelay);

    this.busy = false;
    this.selected = false;
  }

  ngOnDestroy() {
  
    if (this.selected) {
       this.cancel();
    }
        
  }

  isBusy() {
    return this.busy;
  }

  showBusy() {
    this.busy = true;
  }

  hideBusy() {
    this.busy = false;
  }

  onApproveReprove(approved: boolean) {
    this.selected = true;    
    if (approved) {      
      this.approveItem.approve();
      this.holdCard(true);
    } else {      
      this.approveItem.disapprove();
      this.holdCard(false);
    }
  }

  holdCard(approved: boolean) {    

    this.approveItem.show();    
    this.idItemSliding.close();

    this.approveItem.interval = setInterval(() => {
      console.log("Em interval");
      this.approveItem.current = this.approveItem.current - 1;

      if (this.approveItem.current <= 0) {
        console.log("Em interval 2");

        this.approveItem.hide();
        clearInterval(this.approveItem.interval);
        
        this.approveReproveAction.emit({
          item: this.item,
          approved: approved          
        });

        this.selected = false;                        
      }
    }, this.approveItem.intervalHourGlass);
  }

  cancel() {

    console.log("Aqui em cancelar>", this.item);

    clearInterval(this.approveItem.interval);
    this.approveItem.hide();    

    this.canceled.emit(this.item);

    this.selected = false;
    this.busy = false;
  }
  
}
