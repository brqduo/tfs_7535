export class ApproveItemPermissions {
  canApprove: boolean;
  canReprove: boolean;
  canList: boolean;
  
  /**
   *
   */
  constructor(canApprove: boolean,canReprove: boolean, canList: boolean) {    
      this.canApprove = canApprove;
      this.canReprove = canReprove;
      this.canList = canList;
  }
}   

export class AproveItem {
  public intervalHourGlass; // intervalo (ms) entre atualizacoes na tela
  private tempoHourGlass = 10; // tempo em segundos de exibicao antes de efetivar operacao
  private maxHourGlass;// qtd de intervalos

  public showProgress: boolean;
  public showText: string;
  public backColorCard: object;

  public current: number;
  public color: string;
  public radius: number;
  public stroke: number;
  public background: string;
  public clockwise: boolean;
  public max: number;

  public interval: number;

  constructor(actionDelay: number) {

    this.showProgress = false;
    this.backColorCard = null;
    this.max = 40;
    this.radius = 40;
    this.color = "#ffffff";
    this.background = "transparent";
    this.stroke = 5;
    this.clockwise = false;
    this.showText = "";
    
    this.intervalHourGlass = 250; 
    if (actionDelay > 0)
      this.tempoHourGlass = actionDelay; 

    this.maxHourGlass = (1000 / this.intervalHourGlass) * this.tempoHourGlass; 
  }

  approve() {
    this.showText = "Aprovação";
    this.backColorCard = { "background-color": "#486018" }; // antigo '#60823d'
  }

  disapprove() {
    this.showText = "Reprovação";
    this.backColorCard = { "background-color": "#d10429" };
  }

  show(){
      this.current = this.maxHourGlass;
      this.showProgress = true;
  }

  hide(){
    this.showProgress = false;
    this.backColorCard = null;
    this.current = this.maxHourGlass;
    this.max = this.maxHourGlass;
  }

}
