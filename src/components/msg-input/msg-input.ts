import { Component, ViewChild} from "@angular/core";
import { IonicPage, NavParams, ViewController } from "ionic-angular";


@IonicPage()
@Component({
  selector: "page-msg-input",
  templateUrl: "msg-input.html"
})
export class MsgConfirmPage {
  title: string;
  message: string;
  color: string;




  @ViewChild('msgInputBox') inputToFocus;
  constructor(public viewCtrl: ViewController, public params: NavParams) {
    this.title = params.get("title");
    this.message = params.get("message");
    this.color = params.get("color");

    console.log(this);
  }


  ionViewDidEnter(){
    console.log('força focus');
    setTimeout(() => {
      this.inputToFocus.setFocus();
    },200)
    
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  confirm() {
    
    if (!this.message || this.message.length === 0) {
      this.viewCtrl.dismiss({ message: "" });
    } else {
      this.viewCtrl.dismiss({ message: this.message });
    }
    
    console.log('Confirmar');    
  }
}
