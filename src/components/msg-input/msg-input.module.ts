import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MsgConfirmPage } from './msg-input';
import { SharedModule } from '../../app/shared/shared.module';

@NgModule({
  declarations: [
    MsgConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(MsgConfirmPage),
    SharedModule
  ],
  exports:[
    MsgConfirmPage
  ],
})
export class MsgConfirmPageModule {}
