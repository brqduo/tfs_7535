import { Component, Input, HostBinding } from '@angular/core';

/**
 * Generated class for the OnsCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'ons-card',
  templateUrl: 'ons-card.html'
})
export class OnsCardComponent { 

  @Input() color;    
  @HostBinding('class') class: string = "cardBorderline";

  constructor() {
    
  }  
}
