import { Component, Input, EventEmitter, Output } from '@angular/core';
import { LoginService } from '@ons/ons-mobile-login';
import { QuadradoService } from '../../services/quadraros.service';
import { NavParams } from 'ionic-angular';

@Component({
    selector: 'toolbar-header',
    templateUrl: 'toolbar-header.html',
})
export class ToolbarHeaderComponent {

    @Input() title;
    @Input() color;
    @Input() showTab: boolean = true;
    @Output() changeTab: EventEmitter<any> = new EventEmitter();
    @Output() callbackVoltar: EventEmitter<any> = new EventEmitter();

    showBackButton: any;
    segItens: any = [{ title: "Pendentes", value: "Pendentes" }, { title: "Todos", value: "Todos" }];

    segment = 'Pendentes'; // inicializa com uma das abas ativa

    constructor(
        public loginSrv: LoginService,
        public quadradoSrv: QuadradoService,
        public navParams: NavParams) {

        this.showBackButton = navParams.get('showBackButton');
    }

    sair() {
        this.loginSrv.showLogoutOptions('Recebimento Físico', false, this.callbackVoltar);

    }

    voltar(){
        this.callbackVoltar.emit(event);
    }

    onChangeTab(event) {
        this.changeTab.emit(event);
    }
}

