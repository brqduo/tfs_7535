import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'swiper-pagination',
    templateUrl: 'swiper-pagination.html'
})
export class SwiperPaginationComponent {

    @Input() page;
    @Input() templateCelula;
    @Output() changePage: EventEmitter<any> = new EventEmitter();
    @Input() items=[];

    constructor(public navCtrl: NavController) {

    }
    onSlideChanged(event)
    {
        console.log('mudou de pagina');
    }
    ngOnInit() {

    }

    back() {
        this.changePage.emit(DirectionValues.DIRECTION_RIGHT);
    }

    next() {
        this.changePage.emit(DirectionValues.DIRECTION_LEFT);
    }
}

export interface pageType {

    skip: number;
    total: number;
    take: number;
    title: string;
}

export enum ColorValues {
    blue = 1,
    red = 2,
}

export enum DirectionValues {
    DIRECTION_NONE = 1,
    DIRECTION_LEFT = 2,
    DIRECTION_RIGHT = 4,
    DIRECTION_UP = 8,
    DIRECTION_DOWN = 16,
    DIRECTION_HORIZONTAL = 6,
    DIRECTION_VERTICAL = 24,
    DIRECTION_ALL = 30,
}
