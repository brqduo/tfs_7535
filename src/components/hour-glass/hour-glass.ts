import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'hour-glass',
    templateUrl: 'hour-glass.html'
})
export class HourGlassComponent {

    @Input() show: boolean = false;
    @Input() backColorCard: string = 'transparent';
    @Input() showText: string = '';
    @Input() current: number = 27;          //current progress
    @Input() max: number = 40;              //maximum value.
    @Output() canceled: EventEmitter<any> = new EventEmitter();
    
    stroke: number = 5;                     //espessura
    radius: number = 40;                    //raio 
    semicircle: boolean = false;
    rounded: boolean = false;
    responsive: boolean = false;
    clockwise: boolean = false;             //sentido horário
    color: string = '#ffffff';
    background: string = 'transparent';
    duration: number = 800;
    animation: string = 'linearEase';

    constructor() {
    }
}
