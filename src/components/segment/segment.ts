import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'segment',
    templateUrl: 'segment.html'
})
export class SegmentComponent {

    @Input() itens: Array<segItem> = [];
    @Input() selected: string= "Pendentes";
    @Output() changeTab: EventEmitter<any> = new EventEmitter();

    //segment = "Pendentes";

    constructor() {
    }

    onChangeTab() {
        this.changeTab.emit(this.selected);
    }
}

export interface segItem {
    title: string,
    value: string
}
