export const Config = {
  production: false,
  loginServiceConfig: {
    enviroment: "TST",
    urlRemoteConfig: "https://appapitst.ons.org.br/api.loja/"
  },


  API_URL_PEOPLE: 'http://integrationnode.tstapi.ons.org.br',
  API_URL_CATRACA: 'https://appapitst.ons.org.br/API.Mobile.MeuONS',

  Menu: [
    {
      "ordem": 1,
      "nome": "GestorNet",
      "cor": "#4B80BD",
      "icone": "./assets/icon/icon-gestornet.png",
      "destino": "gestornet",
      "AppName": "GestorNetPage",
      "Operations": ["Menu:GestorNet"],
      "DeepLink": "https://onsgestornet.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.gestornet&ibi=br.org.ons.enterprise.gestornet&ius=gestornet&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&efr=1"
    }, {
      "ordem": 2,
      "nome": "Contatos",
      "cor": "#486018",
      "icone": "./assets/icon/icon-contatos.png",
      "destino": "contatos",
      "AppName": "ContatosPage",
      "Operations": ["Menu:Contatos"],
      "DeepLink": "https://onscontatos.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.contatos&ibi=br.org.ons.enterprise.contatos&ius=contatos&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&efr=1"
    }, {
      "ordem": 3,
      "nome": "Recebimento Físico",
      "cor": "#F76C00",
      "destino": "Recebimento_Fisico",
      "AppName": "RecebimentoPage",
      "sigla": "RF",
      "Operations": ["Menu:RecebimentoFísico"]
    }, {
      "ordem": 4,
      "nome": "Aprovação de Férias",
      "cor": "#D10429",
      "destino": "aprovacao-ferias",
      "AppName": "AprovacaoFeriasPage",
      "sigla": "AF",
      "Operations": ["Menu:AprovaçãoFérias"]
    }, {
      "ordem": 5,
      "nome": "Catraca Online",
      "cor": "#4f8ad8",
      "destino": "Catraca_Online",
      "AppName": "CatracaPage",
      "sigla": "CO",
      "Operations": ["Menu:CatracaOnline"]
    }, {
      "ordem": 6,
      "nome": "Aprovação de MO",
      "cor": "green",
      "destino": "aprovacao-mo",
      "AppName": "AprovacaoMOPage",
      "sigla": "MO",
      "Operations": ["Menu:AprovaçãoMO"]
      // "Operations": ["Menu:CatracaOnline"]
    }, {
      "ordem": 7,
      "nome": "Portal de TI",
      "cor": "#a6aab2",
      "destino": "",
      "AppName": "",
      "sigla": "PTI",
      "Operations": ["Menu:PortalTI"],
      //"Operations": ["Menu:CatracaOnline"],
      "externalUrl": "https://onsbrtst.sharepoint.com/sites/portalti"
    }
  ]


};