
import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import moment from 'moment';

import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

import { LoginService, BaseService } from '@ons/ons-mobile-login';
import { Config } from '../../../../enviroment/environment';
import { Parameter } from '../config';




@Injectable()
export class CatracasService {
  objPontos = [];
  d1 = 1
  constructor(
    public utilSrv: UtilService,
    public loginSrv: LoginService,
    private baseService: BaseService,
    private http: HttpClient

  ) {
    moment.locale('pt-br');
    this.baseService.setBaseUrl(Config.API_URL_CATRACA);
  }
  // /**
  //  * getRandomInt
  //  * 
  //  * retorna um número aletaório.
  //  * 
  //  * @param min Valor mínimo
  //  * @param max Valor máximo
  //  */
  // getRandomInt(min, max) {
  //   return Math.floor(Math.random() * (max - min + 1) + min);
  // }

  receiveData(dataRequisitada?) {

    const httpOptions = {
      headers: new HttpHeaders({
        "content-type": "application/json",
        "accept": "application/json",

      })
    };
    if (this.loginSrv.userSrv.User.User_Name == 'ONS\\mvinicius.rerum')
      return this.http.get("./assets/data/catraca.json");
    else
      return this.baseService.executeGet(Parameter.API_SERVICO, Parameter.ENDPOINT_CATRACA + dataRequisitada, httpOptions);



  }
}