
export const Parameter = {
    DEFAULT_DATE_FORMAT: 'YYYY-MM-DD',
    DEFAULT_MONTH_FORMAT: 'MMMM [de] YYYY',
    API_SERVICO: 'api/catracas',
    //URL_SERVICO: 'http://10.2.1.127:72/api/catracas/',
    ENDPOINT_CATRACA: 'data/',
    TOPO_PAG_SCROLL: 60
}