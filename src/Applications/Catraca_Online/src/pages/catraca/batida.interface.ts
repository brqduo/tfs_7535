export interface Batida {
  data: string;
  diaSemana: string;
  diaMes: string;
  mes: string;
  id: number;
  fim_de_semana: boolean;
  gap: Number;
  gap_varios_dias: boolean;
  impar: boolean;
  possue_batidas: boolean;
  batidas: Array<string>
}
