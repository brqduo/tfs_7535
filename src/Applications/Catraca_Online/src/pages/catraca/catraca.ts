import { Batida } from './batida.interface';
import { CatracasService } from '../../services/catracas.service';
import { Component, EventEmitter, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, Refresher } from 'ionic-angular';
import moment from 'moment';
import * as lodash from 'lodash';
import { UtilService } from '../../services/util.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { LoginService, SecurityService } from '@ons/ons-mobile-login';
import { HomePage } from '../../../../../pages/home/home';
import { QuadradoService } from '../../../../../services/quadraros.service';
import { Parameter as config } from '../../config';
import { Observable } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-catraca',
  templateUrl: 'catraca.html'
})
export class CatracaPage {
  registroAnterior: any = {};
  show: boolean;
  batidas: Batida[] = [];
  mes = '';
  lastDate = moment().add('days', 1).format(config.DEFAULT_DATE_FORMAT);
  doRefreshLastData = moment().format('YYYY-MM-DD')
  meseImpressos = [];
  lastId: number = -1;
  totalAppsPermitidos: number = 0;
  showBackButton: any;
  permiteVerMarcacoes: boolean;

  callbackVoltar: EventEmitter<any> = new EventEmitter();


  /* FUNÇÕES DA ANIMAÇÃO DO DISPLAY DE HORAS */
/*   @ViewChild('headerPosZ') headerPos: ElementRef;
  @ViewChild('headerPosZ2') headerPos2: ElementRef;
  @ViewChild('hourDisplay') hourDisplay: ElementRef; */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public catracaSrv: CatracasService,
    public loginSrv: LoginService,
    public AnalyticsSrv: AnalyticsService,
    public utilSrv: UtilService,
    private platform: Platform,
    private quadradoSrv: QuadradoService,
    public alertController: AlertController,
    public zone: NgZone,
    public securitySrv: SecurityService) {
    //this.doRefresh();
    this.startData().subscribe();
    moment.locale('pt-br');
    this.registroAnterior.fim_de_semana = false;
    this.showBackButton = navParams.get('showBackButton');

    this.platform.registerBackButtonAction(() => {
      this.voltar();
    });

    this.callbackVoltar
      .subscribe((u: any) => {
        this.navCtrl.setRoot(HomePage);
      })
  }


  /**
   * Voltar()
   * 
   * Retorna a tela anterior.
   */

  voltar() {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    this.totalAppsPermitidos = this.quadradoSrv.AppsAllowed;
    this.permiteVerMarcacoes = true
  }

  startData(): Observable<string> {
    return Observable.create(observer =>
      this.catracaSrv.receiveData(moment(this.lastDate, config.DEFAULT_DATE_FORMAT).format('YYYY-MM-DD')).subscribe(dados => {

        let list = lodash.filter(dados, item => moment(item.data, config.DEFAULT_DATE_FORMAT) < moment(this.lastDate, config.DEFAULT_DATE_FORMAT));
        
        list.forEach((element, key) => {
          const momentdate = moment(element.data, config.DEFAULT_DATE_FORMAT);
          element.diaSemana = momentdate.format('ddd');
          element.diaMes = momentdate.format('DD');
          element.mes = momentdate.format(config.DEFAULT_MONTH_FORMAT);
          element.id = this.batidas.length + 1;
         // if (i <= 20) {
         //   i++
            this.batidas.push(element)
            this.lastDate = moment(element.data, config.DEFAULT_DATE_FORMAT).format(config.DEFAULT_DATE_FORMAT)
          //}
        })
        this.force = moment(this.batidas[0].data).format(config.DEFAULT_MONTH_FORMAT);
        observer.next(this.batidas);
      }));

  }

  imprimeFimSemana(id): string {
    if (this.batidas[id - 1].fim_de_semana) {
      return 'Fim de Semana'
    } else {
      return '';
    }
  }

  getStyles(dados: Batida) {
    let retorno = '';

    if (dados.fim_de_semana) {
      retorno += 'workingweekend ';
      if (dados.possue_batidas) {

        if (dados.impar) {
          retorno += 'incomplete '
        }
      } else {
       // if (dados.fim_de_semana != true) {
          retorno += 'empty ';
       // }
      }
    }

    if (dados.impar) {
      retorno += ' incomplete ';
    }

    if (dados.gap_varios_dias && dados.fim_de_semana != true) {
      retorno = 'interval';
    }

    if (dados.gap_varios_dias && dados.fim_de_semana == true && dados.gap > 2) {
      retorno = 'interval';
    }

    if (dados.batidas !== null && dados.batidas !== undefined) {
      if (dados.batidas.length === 0 && dados.gap_varios_dias === false) {
        retorno = 'empty ';
      }
    }
    if (dados.batidas === null || dados.batidas === undefined) {
      if (!dados.gap_varios_dias && dados.fim_de_semana != true) {
        retorno = 'empty ';
      }

      if (dados.gap_varios_dias && dados.gap < 3 && dados.fim_de_semana == true) {
        retorno = 'item-none ';
      }
    }
    this.registroAnterior = dados;
    return retorno;
  }

  temMes(item: Batida) {
    return (item.mes !== null && item.mes !== "" && item.mes !== undefined)
  }

  set force(value: string) {
    this.zone.run(() => {
      this.mes = value;
    })
  }


/* FUNÇÃO RESPONSAVEL PELA ANIMAÇÃO EM SI */

/*   displayHourHeader() {
    const PositionHour = this.headerPos2.nativeElement.getBoundingClientRect().y
    const MouthAnnouncer = document.getElementsByClassName('fixed');
    const HeaderHour = document.getElementsByClassName('navbar');
    console.log('Z pos', this.headerPos.nativeElement.style);
    console.log('PositionHour', PositionHour);
    console.log('conta da linha 324 deu', (-1 * (PositionHour)) + 1 + 'px');

    this.hourDisplay.nativeElement.style.top = PositionHour + 1 + 'px';
    this.headerPos.nativeElement.style.top = PositionHour + 62 + 'px';
    if (PositionHour < -63) {
      this.headerPos.nativeElement.style.top = '65px'
    }

    if (PositionHour < 0) {
      console.log('PositionHour', PositionHour);
      console.log('conta da linha 340 deu', (-1 * (PositionHour)) + 1 + 'px');

    }


        if (PositionHour < -70) {
          this.showHourDisplay = true;
          MouthAnnouncer[0].style.top = '65px'
    
        } else {
          this.showHourDisplay = false;
          MouthAnnouncer[0].style.top = '125px'
        } 
  } */


  functionToTriggerOnScroll(event) {
    const list = document.getElementsByClassName("mes");
    /* CHAMADA DA FUNÇÃO QUE CUIDA DA ATIVAÇÃO DA ANIMAÇÃO */
    
    // this.displayHourHeader();
    for (let i = 0; i < list.length; i++) {
      const element: any = list[i];
      if (this.isElementInViewport(element)) {
        this.force = element.title;
      }
    }
  }

  isElementInViewport(el) {
    const rect = el.getBoundingClientRect();
    return (
      rect.y <= config.TOPO_PAG_SCROLL
    );
  }

  returnMonth(mesSelecionado: string, id: number) {
    if (id < this.lastId) {
      this.meseImpressos = [];
    }
    this.lastId = id;
    let exibeMes = false;

    if (this.meseImpressos.indexOf(mesSelecionado) === -1) {
      this.meseImpressos.push(mesSelecionado)
      exibeMes = true;
    } else {
      exibeMes = false
    }
    if (((this.mes !== mesSelecionado) && (exibeMes))) {
      return { 'background-color': 'transparent' }

    } else {
      return {
        'display': 'none'
      }
    }
  }

  /**
   * 
   * doInfinite
   * 
   * Controla o infiniteScroll
   * 
   * @param InfiniteScroll Elemento
   */
  doInfinite(InfiniteScroll) {
    this.startData().subscribe(() => {
      InfiniteScroll.complete()
    })
  }

  doRefresh(refresher?: Refresher) {
    if (refresher !== undefined) {
      this.lastDate = moment().add('days', 1).format(config.DEFAULT_DATE_FORMAT)
      this.batidas = [];
      this.startData().subscribe(() => {
        refresher.complete()
      })
    }

  }

  ionViewDidLeave() {
    this.platform.registerBackButtonAction(() => {
      //
    });
  }
  sair() {
    this.loginSrv.showLogoutOptions('Recebimento Físico', false, this.callbackVoltar);

  }

}

