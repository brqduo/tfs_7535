import { CatracaPage } from './catraca';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CatracasService } from '../../services/catracas.service';
import { UtilService } from '../../services/util.service';
import { BaseService } from '@ons/ons-mobile-login';


@NgModule({
    declarations: [
        CatracaPage,
    ],
    imports: [
        IonicPageModule.forChild(CatracaPage),
    ],
    providers: [
        CatracasService,
        UtilService,
        BaseService
    ],
    exports: [
        CatracaPage
    ]
})

export class CatracaPageModule { }
