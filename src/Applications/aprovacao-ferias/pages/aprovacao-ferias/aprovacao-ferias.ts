import { Component, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { NavController, IonicPage, NavParams, Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import { LoginService, SecurityService } from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { HomePage } from '../../../../pages/home/home';
import { QuadradoService } from '../../../../services/quadraros.service';
import { UIServices } from '../../../../services/ui.service';
import { IFerias } from '../../model/ferias';
import { IPeriodo } from '../../model/periodo';
import { StatusPeriodo } from '../../model/status-periodo';
import { ApproveItemPermissions } from '../../../../components/approve-item/approve-itemtypes';
import { ApproveItemComponent } from '../../../../components/approve-item/approve-item';
import { HelperCor } from '../../model/helper-cor';



/**
 * AprovacaoFerias
 */

@IonicPage()
@Component({
  selector: "page-aprovacao-ferias",
  templateUrl: "aprovacao-ferias.html"
})
export class AprovacaoFeriasPage {


  feriasPendentes: Array<IFerias>;
  feriasOutros: Array<IFerias>;
  periodoSelecionado: IPeriodo;
  StatusPeriodo: typeof StatusPeriodo = StatusPeriodo;

  semFeriasOutros = false;
  semFeriasPendentes = false;

  public escopoAcess = "ONS";
  public permissoesPendentes: ApproveItemPermissions;
  public permissoesTodos: ApproveItemPermissions;

  @ViewChildren(ApproveItemComponent) approveItens: QueryList<
    ApproveItemComponent
  >;

  showSearch: boolean = false;
  showBackButton: any;
  isLoading = false;
  segment = "pendentes";
  borderColorCard = "#A6A6A6"; //cinza;
  backAlowed = false;
  callbackVoltar: EventEmitter<any> = new EventEmitter();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private uiServices: UIServices,
    public http: HttpClient,
    private platform: Platform,
    private securitySrv: SecurityService,
    private analyticSrv: AnalyticsService,
    public QuadradosSrv: QuadradoService,
    public loginSrv: LoginService,
    private quadradoSrv: QuadradoService
  ) {
    this.showBackButton = navParams.get("showBackButton");
    this.backAlowed = this.quadradoSrv.AppsAllowed > 1
    this.platform.registerBackButtonAction(() => {
      this.voltar();
    });

    this.callbackVoltar
      .subscribe((u: any) => {
        this.navCtrl.setRoot(HomePage);
      }) 
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad TesteAproveitemPage");
  }

  ionViewWillEnter() {
    const canApprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Aprovar");
    const canReprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Reprovar");
    const canList = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:ListarFérias");
    this.permissoesPendentes = new ApproveItemPermissions(canApprove, canReprove, canList);
    this.permissoesTodos = new ApproveItemPermissions(false, false, true);

    if (this.permissoesPendentes.canList) {
      this.carregarDados(true);
    }

    this.analyticSrv.sendCustomEvent("Entrou no Aprovacao de férias");

  }

  carregarDados(bloqueiaTela: boolean, refresher?: any) {
    if (refresher) {
      this.analyticSrv.sendCustomEvent(
        "Atualizou tela via Refresher na tela " + this.segment
      );
    }

    this.semFeriasOutros = false;
    this.semFeriasPendentes = false;

    if (bloqueiaTela && !refresher) {
      this.isLoading = true;
    }

    this.http.get("./assets/data/ferias.json").subscribe(
      (dados: Array<IFerias>) => {
        console.log(dados);

        this.carregarListasDeFerias(dados);

        if (this.feriasPendentes.length == 0) {
          this.semFeriasPendentes = true;
        }

        if (this.feriasOutros.length == 0) {
          this.semFeriasOutros = true;
        }

        this.isLoading = false;
        if (refresher) {
          refresher.complete();
        }

        this.analyticSrv.sendCustomEvent("Carregou dados aprovação de férias", {
          Total: dados.length,
          Pendentes: this.feriasPendentes.length,
          Outros: this.feriasOutros.length
        });
      },
      error => {
        this.isLoading = false;
        this.analyticSrv.sendNonFatalCrash(
          "Erro ao carregar dados em aprovação de férias",
          error
        );
        if (error && error.message) {
          this.showAlert("Erro", "Ocorreu um erro ao atualizar a lista.");
        }
        if (error && error.mensagem) {
          this.showAlert("Erro", error.mensagem);
        }
        if (refresher) refresher.complete();
        console.log("resultado com erro em business:" + JSON.stringify(error));
      }
    );
  }

  /**
   * seta a permissao do periodo de acordo com o status
   * @param itemPeriodo 
   */
  private setPermissao(itemPeriodo: IPeriodo) {

    if (itemPeriodo.status === StatusPeriodo.PendenteAprovacao) {
      itemPeriodo.permissoes = new ApproveItemPermissions(
        this.permissoesPendentes.canApprove,
        this.permissoesPendentes.canReprove,
        this.permissoesPendentes.canList
      );
    } else {
      itemPeriodo.permissoes = new ApproveItemPermissions(
        this.permissoesTodos.canApprove,
        this.permissoesTodos.canReprove,
        this.permissoesTodos.canList
      );
    }
  }

  /**
   * seta os valores para a lista de pendentes e outros;
   * @param dados 
   */
  private carregarListasDeFerias(dados: IFerias[]) {

    this.feriasPendentes = [];
    this.feriasOutros = [];
    let periodos = [];

    for (let i = 0; i < dados.length; i++) {
      const dadoOriginal = dados[i];

      if (dadoOriginal.pendente) {
        periodos = dadoOriginal.periodos;
        for (let index = 0; index < periodos.length; index++) {
          let itemPeriodo = periodos[index];
          this.setPermissao(itemPeriodo);
        }
        this.feriasPendentes.push(dadoOriginal);
      } else {
        periodos = dadoOriginal.periodos;
        for (let index = 0; index < periodos.length; index++) {
          let itemPeriodo = periodos[index];
          this.setPermissao(itemPeriodo);
        }
        this.feriasOutros.push(dadoOriginal);
      }
    }
  }


  aprovarReprovar(itemPeriodo: IPeriodo, aprovado: boolean, itemFerias: IFerias) {
    console.log(itemFerias);
    console.log(itemPeriodo);

    if (aprovado) console.log("aprovado");
    else console.log("Reprovado");

    let idApproveItemSelecionado = itemFerias.id + itemPeriodo.ordem;

    //aproveItem selecionado, pego pelo id
    let approveItemSelecionado = this.approveItens.filter(
      approve =>
        approve.id === idApproveItemSelecionado &&
        approve.item.data_inicio === itemPeriodo.data_inicio &&
        approve.item.data_termino === itemPeriodo.data_termino
    )[0];

    if (approveItemSelecionado) {
      approveItemSelecionado.showBusy();

      setTimeout(() => {
        console.log("Simula fim da comunicacao");
        approveItemSelecionado.hideBusy();

        if (aprovado) itemPeriodo.status = StatusPeriodo.Aprovado;
        else itemPeriodo.status = StatusPeriodo.Reprovado;

        itemPeriodo.permissoes.canApprove = false;
        itemPeriodo.permissoes.canReprove = false;
      }, 2000);
    }
  }

  envioCancelado(itemPeriodo: IPeriodo, itemFerias: IFerias) {

    console.log("registar log cancelado envio de " + itemPeriodo);
    console.log("registar log cancelado envio de " + itemFerias);

    //alert("cancelado");
  }

  /* formata e mostra observacao no rodape da pagina */

  mostraObs(periodo: IPeriodo) {
    this.periodoSelecionado = periodo;

    let titulo = "Justificativa";
    let cor = HelperCor.COR_PENDENTE_APROVACAO;
    let mensagem = periodo.justificativa;;

    if (periodo.status === StatusPeriodo.Aprovado) {
      titulo = "Justificativa da Aprovação";
      cor = HelperCor.COR_APROVADO;
    } else {
      if (periodo.status == StatusPeriodo.Reprovado) {
        titulo = "Justificativa da Reprovação";
        cor = HelperCor.COR_REPROVADO;
      }
    }

    if (!periodo.permissoes.canApprove && !periodo.permissoes.canApprove) {
      this.uiServices.showFooterMessage(titulo, mensagem, cor);
    } else {

      this.uiServices.showInputMessage(titulo, mensagem, cor)
        .subscribe(a => {
          this.periodoSelecionado.justificativa = a;
          console.log('Obs: ' + this.periodoSelecionado.justificativa);
        });
    }
  }

  /* mostra/esconde barra de pesquisa */

  searchShow(ev) {
    if (ev === true) {
      this.showSearch = true;
    } else {
      this.showSearch = false;
    }
  }

  /**
   * Voltar()
   *
   * Retorna a tela anterior.
   */

  voltar() {
    this.navCtrl.setRoot(HomePage);
    this.analyticSrv.sendCustomEvent("Aprovação de férias - Voltar para home");
  }

  mudarTab() {
    this.analyticSrv.sendCustomEvent(
      "Aprovação de férias- Selecionou: " + this.segment
    );
  }



  showAlert(titulo: string, mensagem: string) {
    this.uiServices.showAlertMessage(titulo, mensagem, HelperCor.COR_ALERT);
  }

  /**
  * 
  * sair
  * 
  * Exibe opções de sair do aplicativo.
  * 
  */
  sair() {
    this.loginSrv.showLogoutOptions("Catraca", false, this.callbackVoltar);
  }

  ionViewDidLeave() {
    this.platform.registerBackButtonAction(() => {
      //
    });
  }
}
