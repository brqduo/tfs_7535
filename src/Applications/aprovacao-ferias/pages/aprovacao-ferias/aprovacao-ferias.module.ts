
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { ChartsModule } from 'ng2-charts';

import { AprovacaoFeriasPage } from './aprovacao-ferias';
import { SharedModule } from '../../../../app/shared/shared.module';

@NgModule({
    declarations:[
     
  

        AprovacaoFeriasPage
    ],
    imports:[
        ChartsModule,
        RoundProgressModule,
        SharedModule,
        IonicPageModule.forChild(AprovacaoFeriasPage),
    ],
    entryComponents: [
         
    ],
    exports: [
        AprovacaoFeriasPage
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
})

export class AprovacaoFeriasPageModule{ }
