export enum StatusPeriodo{
   
    Aprovado = 1,
    Reprovado = 2,
    PendenteAprovacao = 3    
}