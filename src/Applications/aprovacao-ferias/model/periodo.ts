
import { ApproveItemPermissions } from './../../../components/approve-item/approve-itemtypes';
import { StatusPeriodo } from './status-periodo';

export interface IPeriodo {
    ordem: number;
    data_inicio: string;
    data_termino: string;
    st_adiantamento:boolean;
    st_recebimento_13:boolean;
    status: StatusPeriodo;    
    permissoes: ApproveItemPermissions,
    justificativa: string
  }