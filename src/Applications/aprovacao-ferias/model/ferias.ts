import { IPeriodo } from "./periodo";

export interface IFerias {
  id: string;
  nome: string;
  cargo: string;
  pendente: boolean;
  dt_admissao: string;
  foto: string;
  periodo_aquisitivo: string;
  periodo_fruicao: string;
  periodos: Array<IPeriodo>;
}








