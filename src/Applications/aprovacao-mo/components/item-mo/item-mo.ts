import { Component, Input } from "@angular/core";

@Component({
  selector: "item-mo",
  templateUrl: "item-mo.html"
})
export class ItemMoComponent {
  @Input() itemOrigem: any;
  constructor() {
  }
  ngOnInit() {
    console.log(this.itemOrigem);
  }

  totalOrcado() {
    let total = 0;

    if (this.itemOrigem.valores) {
      this.itemOrigem.valores.forEach((d) => {
        total += parseFloat(d.orcado);
      });
    }

    return total;
  }
 
  totalAretirar() {
    let total = 0;
    if (this.itemOrigem.valores) {
      this.itemOrigem.valores.forEach((d) => {
        total += parseFloat(d.aretirar);
      });
    }

    return total;
  }


}
