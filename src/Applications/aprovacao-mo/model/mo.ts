import { ApproveItemPermissions } from '../../../components/approve-item/approve-itemtypes';
import { ApproveItemStatus } from './approve-item-status';
import { IVerba } from "./verba";

export interface IMO {
  id: string;
  codigo: string;
  status: ApproveItemStatus; 
  data_inicio: string;
  data_fim: string;
  description: string;
  obs: string;
  tipo: string;
  description2: string;
  description3: string;
  origemVerba: Array<IVerba>;
  aplicacaoVerba: Array<IVerba>;
  permissoes: ApproveItemPermissions
}








