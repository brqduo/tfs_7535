export class HelperCor{
  public static COR_APROVADO = "#455E2B"; //verde
  public static COR_REPROVADO = '#D10429'; //Vermelho
  public static COR_PENDENTE = "#868686"; //cinza
  public static COR_ALERT = "#D10429"; // janela de alerta

}