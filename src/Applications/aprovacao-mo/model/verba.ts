
import { IValores } from './valores';

export interface IVerba {
  origem: string,
  ua: string,
  pdpi: string,
  etapa: string,
  ngtg: string,
  valores: Array<IValores>
}
