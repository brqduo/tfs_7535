import { Component, EventEmitter, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { LoginService } from '@ons/ons-mobile-login';
import { QuadradoService } from '../../../services/quadraros.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { HttpClient } from '@angular/common/http';
import { UIServices } from '../../../services/ui.service';

import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';

import { DetalheAprovacaomoPage } from './detalhe-aprovacaomo/detalhe-aprovacaomo';
/* pagina inicial */
import { HomePage } from '../../../pages/home/home';
/* tipo de permissao: pode aprovar, pode reprovar, pode listar */

import { ApproveListPermissions } from '../../../components/approve-list/approve-listTypes';

/* tipos de status: pendente, aprovado, reprovado */
// todo: ver enum
//import { ApproveItemStatus } from '../model/approve-item-status';
/* tipo de item da lista para aprovacao */

/* tipo de dados dos itens */
import { IMO } from '../model/mo';

/* todo: avertar definicoes de cor */
import { HelperCor } from '../model/helper-cor';
import { Observable } from 'rxjs';
import { ApproveList } from '../../../components/approve-list/approve-list';


@IonicPage()
@Component({
    selector: 'page-aprovacao-mo',
    templateUrl: 'aprovacao-mo.html'
})
export class AprovacaoMOPage {

    showBackButton: any; // botao voltar no cabecalho
    callbackVoltar: EventEmitter<any> = new EventEmitter();
    isLoading = false; // esconde spinner
    showSearch = false; // esconde barra de pesquisa
    segment = 'pendentes'; // inicializa com uma das abas ativa
    borderColorCard: any; // cor da borda do cartao
    item: any = {}; // conteudo do cartao

    @ViewChild('approveListPendentes') approveList: ApproveList;



    // permissoes da lista de itens
    public permissoesPendentes: ApproveListPermissions;
    public permissoesTodos: ApproveListPermissions;
    public obemObservacaoCallback: Function;

    // armazena listas de itens
    itensPendentes: Array<IMO>;
    itensTodos: Array<IMO>;

    // indica se as listas estao vazias
    semItensPendentes = false;
    semItensTodos = false;

    // ver mode enum
    ApproveItemStatus = {
        Aprovado: 'aprovado',
        Reprovado: 'reprovado',
        Pendente: 'pendente'
    }


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private platform: Platform,
        public loginSrv: LoginService,
      
        private quadradosSrv: QuadradoService,
        private analyticSrv: AnalyticsService,
        public http: HttpClient,
        private uiServices: UIServices
    ) {

        /**
         *
         * trata navegacao sair/voltar
         *
         */

        this.showBackButton = navParams.get("showBackButton");
        this.platform.registerBackButtonAction(() => {
            this.voltar();
        });
        this.callbackVoltar.subscribe((u: any) => {
            this.navCtrl.setRoot(HomePage);
        })

        // cor da borda do cartao
        this.item = navParams.get('item');
        console.log('==> aprovacao.ts', this.item);
        //        this.borderColorCard = HelperCor['COR_' + this.item.status.toUpperCase()];

    }
    ngOnInit() {

        this.obemObservacaoCallback = this.solicitaObservacao.bind(this);
    }
    /**
     * 
     * mostra/esconde a barra de pesquisa
     * 
     */

    searchShow(ev) {
        if (ev === true) {
            this.showSearch = true;
        } else {
            this.showSearch = false;
        }
    }

    /**
     *  
     * Clicou em uma tab
     * 
     */

    mudarTab() {
        this.analyticSrv.sendCustomEvent(
            "Aprovação de MO - Selecionou: " + this.segment
        );
        console.log('mudarTab:', this.segment);
        console.log('pend:', !this.semItensPendentes, this.itensPendentes);
        console.log('outr:', !this.semItensTodos, this.itensTodos);
    }

    /**
     *
     * Clicou no botao voltar do header
     * 
     */

    voltar() {
        this.navCtrl.setRoot(HomePage);
        this.analyticSrv.sendCustomEvent("Aprovação de MO - Voltar para home");
    }

    /**
     * 
     * ignora botao do voltar do hardware do android
     * 
     */
    ionViewDidLeave() {
        this.platform.registerBackButtonAction(() => {
            // ignore
        });
    }

    /**
     * 
     * Exibe opções de sair do aplicativo.
     * 
     */
    sair() {
        this.loginSrv.showLogoutOptions("MO", false, this.callbackVoltar);
    }

    /**
     * 
     * Exibe mensagem padrao de erro
     * 
     */

    showAlert(titulo: string, mensagem: string) {
        this.uiServices.showAlertMessage(titulo, mensagem, HelperCor.COR_ALERT);
    }

    /**
     * 
     * Verifica permissoes antes de entrar na pagina
     * 
     */

    ionViewWillEnter() {
        // const canApprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Aprovar");
        // const canReprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Reprovar");
        // const canList = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:ListarFérias");
        this.permissoesPendentes = new ApproveListPermissions();
        //todo: trocar por permissoes reais na versao integrada
        this.permissoesPendentes.canList = true;
        this.permissoesPendentes.canApprove = true;
        this.permissoesPendentes.canReprove = true;

        this.permissoesTodos = new ApproveListPermissions();
        this.permissoesTodos.canList = true;
        if (this.permissoesPendentes.canList) {
            this.carregarDados(true);
        }
        this.analyticSrv.sendCustomEvent("Entrou no Aprovacao de MO");

    }



    /**
     * 
     * carrega dados do repositorio
     * 
     */

    carregarDados(bloqueiaTela: boolean, refresher?: any) {
        if (refresher) {
            this.analyticSrv.sendCustomEvent(
                "Atualizou tela via Refresher na tela " + this.segment
            );
        }
        if (bloqueiaTela && !refresher) {
            this.isLoading = true;
        }
        this.itensPendentes = [];
        this.itensTodos = [];
        this.semItensPendentes = false;
        this.semItensTodos = false;
        // busca dados do repositorio
        let listaPendente = [];
        this.http.get("./assets/data/mo.json").subscribe(
            (dados: Array<IMO>) => {
                console.log('==> get:', dados);
                console.log('==> get', dados, this.ApproveItemStatus);
                // carrega dados nas listas
                for (let i = 0; i < dados.length; i++) {
                    const dadoOriginal = dados[i];

                    if (dadoOriginal.status == this.ApproveItemStatus.Pendente) {
                        listaPendente.push(dadoOriginal);
                    }
                    else {
                        this.itensTodos.push(dadoOriginal);
                    }
                }
                this.itensPendentes = listaPendente;
                this.semItensPendentes = (this.itensPendentes.length == 0);
                this.semItensTodos = (this.itensTodos.length == 0);
                this.isLoading = false;
                if (refresher) {
                    refresher.complete();
                }
                this.analyticSrv.sendCustomEvent("Carregou dados Aprovação de MO", {
                    Total: dados.length,
                    Pendentes: this.itensPendentes.length,
                    Todos: this.itensTodos.length
                });
            },
            error => {
                this.isLoading = false;
                this.analyticSrv.sendNonFatalCrash(
                    "Erro ao carregar dados em Aprovação de MO",
                    error
                );
                if (error && error.message) {
                    this.showAlert("Erro", "Ocorreu um erro ao atualizar a lista.");
                }
                if (error && error.mensagem) {
                    this.showAlert("Erro", error.mensagem);
                }
                if (refresher) refresher.complete();
                console.log("resultado com erro em business:" + JSON.stringify(error));
            }
        );
    }

    /**
     * 
     * evento disparado, aprovacao ou reprovacao do item
     * 
     */

    onCancelAction(item: any) { // todo: tipar corretamente
        console.log("==> cancelamento da acao para o item ", item);
    }

    /**
     * 
     * evento disparado, cancelamento da acao
     * 
     */

    onConfirmAction(item: any, aprovado: boolean) { // todo: tipar corretamente

        if (aprovado) {
            console.log("==> item Aprovado", item);
        }
        else {
            console.log("==> item Reprovado ", item);
        }

        // localiza item pelo id
        // let itemSelecionado = this.approveItens.filter(
        //     approve =>
        //         approve.id === item.id
        // )[0];
        // let itemSelecionado=item;

        // // simula alguma atualizacao de dados com delay // todo: testar efeito visual
        // if (itemSelecionado) {
        //     console.log("==> vai simular a atualizacao do item no repositorio");
        //     itemSelecionado.showBusy();
        //     setTimeout(() => {
        //         console.log("==> terminou de simular a atualização");
        //         itemSelecionado.hideBusy();
        //         if (aprovado) item.status = this.ApproveItemStatus.Aprovado;
        //         else item.status = this.ApproveItemStatus.Reprovado;
        //         item.permissoes.canApprove = false;
        //         item.permissoes.canReprove = false;
        //     }, 2000);
        // }

    }

    /**
     * 
     * evento disparado: mostrar tela de detalhes
     * 
     */

    showDetails(item) { // todo: ver

        if (item) {
            // ao chamar a pagina de detalhe, cria um callback para ao voltar, caso tenha sido aprovado/reprovado,
            // acione o metodo de aprovacao/reprovacao do component para iniciar o timer
            console.log('DetalheAprovacaomoPage');
            let pageClosed = (retData) => {
                //console.log('this came back:', retData);


                this.approveList.activeExternalApproveReprove(retData);


            }
            let permiteAprovar = false;
            let permiteReprovar = false;
            if (item.status == this.ApproveItemStatus.Pendente) {

                permiteAprovar = this.permissoesPendentes.canApprove;
                permiteReprovar = this.permissoesPendentes.canReprove;

            }
            else {

                permiteAprovar = this.permissoesTodos.canApprove;
                permiteReprovar = this.permissoesTodos.canReprove;
            }
            this.navCtrl.push(DetalheAprovacaomoPage, { showBackButton: true, item: item, pageClosed: pageClosed, permiteAprovar: permiteAprovar, permiteReprovar: permiteReprovar });
        }

    }

    /* todo: ver

      / * formata e mostra observacao no rodape da pagina * /

  mostraObs(periodo: IPeriodo) {
    this.periodoSelecionado = periodo;

    let titulo = "Justificativa";
    let cor = HelperCor.COR_PENDENTE;
    let mensagem = periodo.justificativa;;

    if (periodo.status === StatusPeriodo.Aprovado) {
      titulo = "Justificativa da Aprovação";
      cor = HelperCor.COR_APROVADO;
    } else {
      if (periodo.status == StatusPeriodo.Reprovado) {
        titulo = "Justificativa da Reprovação";
        cor = HelperCor.COR_REPROVADO;
      }
    }

    if (!periodo.permissoes.canApprove && !periodo.permissoes.canApprove) {
      this.uiServices.showFooterMessage(titulo, mensagem, cor);
    } else {

      this.uiServices.showInputMessage(titulo, mensagem, cor)
        .subscribe(a => {
          this.periodoSelecionado.justificativa = a;
          console.log('Obs: ' + this.periodoSelecionado.justificativa);
        });
    }
  }


  */


    expandido(item: any, expandido: boolean) {
        console.log("registar log ", item, " expandido: " + expandido);
    }

    solicitaObservacao(item):
        Observable<string> {
        return Observable.create(observer => {
            this.uiServices.showInputMessage("Justificativa da Reprovação", "", HelperCor.COR_REPROVADO)
                .subscribe(a => {
                    item.observacao = a;
                    observer.next(item);

                }, err => {
                    observer.error(err);
                }
                );

        });

    }


}
