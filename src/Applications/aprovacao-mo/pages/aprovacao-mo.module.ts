import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AprovacaoMOPage } from './aprovacao-mo';
import { SharedModule } from '../../../app/shared/shared.module';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    AprovacaoMOPage,
  
  ],
  imports: [
    ChartsModule,
    SharedModule,
    IonicPageModule.forChild(AprovacaoMOPage),
  ],
  exports: [
    AprovacaoMOPage
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA
    ]
})
export class AprovacaoMOPageModule {}
