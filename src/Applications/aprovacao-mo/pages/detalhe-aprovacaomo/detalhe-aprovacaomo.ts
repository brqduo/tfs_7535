import { Component, EventEmitter, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { AprovacaoMOPage } from '../aprovacao-mo';
import { pageType } from '../../../../components/swiper-pagination/swiper-pagination';
import { UIServices } from '../../../../services/ui.service';
import { HelperCor } from '../../../aprovacao-ferias/model/helper-cor';

@IonicPage()
@Component({
    selector: 'page-detalhe-aprovacaomo',
    templateUrl: 'detalhe-aprovacaomo.html',
})
export class DetalheAprovacaomoPage {
    @ViewChild('slider1') slides1: Slides;
    @ViewChild('slider2') slides2: Slides;
    showBackButton: any; // botao voltar no cabecalho
    callbackVoltar: EventEmitter<any> = new EventEmitter();
    borderColorCard: any; // cor da borda do cartao
    item: any = {}; // conteudo do cartao

    listTotal: any = {};
    listOrigem: any = {};
    listAplicacao: any = {};
    pageOrigem: pageType;
    pageAplicacao: pageType;
    public permiteAprovar: boolean;
    public permiteReprovar: boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private uiServices: UIServices,
        private platform: Platform
    ) {

        this.showBackButton = navParams.get("showBackButton");
        this.permiteAprovar = navParams.get("permiteAprovar");
        this.permiteReprovar = navParams.get("permiteReprovar");

        this.platform.registerBackButtonAction(() => {
            this.voltar();
        });

        // cor da borda do cartao
        this.item = navParams.get('item');
        this.item.origensVerba.forEach(element => {
            element.index = 0;

        });  
        this.item.index = 0;
        this.borderColorCard = HelperCor['COR_' + this.item.status.toUpperCase()];

    }
    origemAnterior() {
        this.item.index--;
        this.slides1.slidePrev(200, false);
        this.item.origensVerba[this.item.index].index = 0;
    }
    origemProximo() {
        this.item.index++;
        this.slides1.slideNext(200, false);
        this.item.origensVerba[this.item.index].index = 0;
    }
    onSlideChanged() {
        this.item.index = this.slides1.getActiveIndex();
        this.item.origensVerba[this.item.index].index = 0;
        this.slides2.resize();
    }
    origemAnterior2() {
        this.item.origensVerba[this.item.index].index--;
        this.slides2.slidePrev(200, false);
    }
    origemProximo2() {
        this.item.origensVerba[this.item.index].index++;
        this.slides2.slideNext(200, false);
    }

    onSlideChanged2() {
        this.item.origensVerba[this.item.index].index = this.slides2.getActiveIndex();
    }
    /**
     *
     * Clicou no botao voltar do header
     * 
     */

    voltar() {
        this.navCtrl.setRoot(AprovacaoMOPage, { showBackButton: true });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DetalheAprovacaomoPage');

        this.carregaListaOrigem();
        this.carregaListaAplicacao();
    }

    getColor(status: string): String {
        let result = '#868686';
        switch(status) {
            case 'aprovado':
                result = '#455E2B';
                break;
            case 'reprovado':
                result = '#D10429';
                break;
          }
        return result;
    }

    carregaListaOrigem() {

        if (this.listTotal && this.listTotal.imoveis && this.listTotal.imoveis.length > 0) {

            this.listOrigem = [];
            this.listOrigem.origem = '01';
            this.listOrigem.ua = '501';
            this.listOrigem.pdpi = '44411';
            this.listOrigem.etapa = '00';
            this.listOrigem.ngtg = '9111';
            this.listOrigem.imoveis = [];

            let total = this.listTotal.imoveis.length;
            let inicio = this.pageOrigem.skip;
            let fim = (inicio + this.pageOrigem.take) > total ? total : (inicio + this.pageOrigem.take);

            for (let index = inicio; index < fim; index++) {
                this.listOrigem.imoveis.push(this.listTotal.imoveis[index]);
            }
        }
    }

    carregaListaAplicacao() {

        if (this.listTotal && this.listTotal.imoveis && this.listTotal.imoveis.length > 0) {

            this.listAplicacao = [];
            this.listAplicacao.origem = '01';
            this.listAplicacao.ua = '501';
            this.listAplicacao.pdpi = '44411';
            this.listAplicacao.etapa = '00';
            this.listAplicacao.ngtg = '9111';
            this.listAplicacao.imoveis = [];

            let total = this.listTotal.imoveis.length;
            let inicio = this.pageAplicacao.skip;
            let fim = (inicio + this.pageAplicacao.take) > total ? total : (inicio + this.pageAplicacao.take);

            for (let index = inicio; index < fim; index++) {
                this.listAplicacao.imoveis.push(this.listTotal.imoveis[index]);
            }
        }
    }

    onApproveReprove(item: any, approved: boolean) {

        item.Approve = approved;
        if (!approved) {
            this.uiServices.showInputMessage("Justificativa da Reprovação", "", HelperCor.COR_REPROVADO)
                .subscribe(a => {
                    item.observacao = a;
                    this.aprovarReprovar(item, approved);
                });
        }
        else {
            this.aprovarReprovar(item, approved);
        }
    }

    aprovarReprovar(item: any, aprovado: boolean) {

        //item.status = aprovado ? StatusType.Aprovado : StatusType.Reprovado;
        var callback = this.navParams.get('pageClosed');
        if (typeof callback == 'function')
            callback(this.item);
        this.navCtrl.pop();
    }

}
