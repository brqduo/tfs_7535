import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalheAprovacaomoPage } from './detalhe-aprovacaomo';
import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../../../app/shared/shared.module';
import { ItemMoComponent } from '../../components/item-mo/item-mo';


@NgModule({
  declarations: [
    DetalheAprovacaomoPage,
    ItemMoComponent
  ],
  imports: [
    ChartsModule,
    SharedModule,
    IonicPageModule.forChild(DetalheAprovacaomoPage),
  ],
  exports: [
    DetalheAprovacaomoPage
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA
    ]
})
export class DetalheAprovacaomoPageModule {}
