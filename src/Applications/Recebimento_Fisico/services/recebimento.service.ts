
import { Injectable } from "@angular/core";
import { BaseService } from '@ons/ons-mobile-login';
import { Config } from "../../../enviroment/environment";

@Injectable()
export class RecebimentoService {
    constructor(private baseService: BaseService) {
        this.baseService.setBaseUrl(Config.API_URL_PEOPLE);//gestor?
    }

    listarRecebimentos() {

        return this.baseService.executeGet('appgestor/v1', 'recebimento').map(

            (response: any) => {

                return response;
            }
        );

    }
    aprovarReprovarRecebimento(acao: string, parameter: any) {

        return this.baseService.executePatch('appgestor/v1', 'recebimento/' + acao, parameter).map(

            (response: any) => {

                return response;
            }
        );

    }
}