
export interface AttachList {
    Attach: string;
    AttachSystemFile: string;
    AttachUserFile: string;
    DownloadLink: string;
}

export interface RecebimentosJSONArray {
    BusinessUnitRecv: string;
    ReceiverId: string;
    OnsStsRecDsc: string;
    NfBrl: string;
    NfBrlDate: string;
    NfBrlId: string;
    BusinessUnit: string;
    VendorId: string;
    Name1: string;
    CntrctId: string;
    OnsTipoCntrDsc: string;
    BuyerId: string;
    CntrctBeginDt: string;
    CntrctExpireDt: string;
    OnsDtAssinatura: string;
    Objeto: string;
    OnsStsCntrDsc: string;
    OnsTipo: string;
    // AmtCntrctMax: number;
    // AmtReleased: number;
    // ResidualAmount: number;
    BusinessUnitReq: string;
    ReqId: string;
    OnsLicitacaoDsc: string;
    OnsMotivo: string;
    PaidAmt: number;
    OpridApprovedBy: string;
    AttachList: AttachList[];
    TotalRecvAmt: number;
    AmtRemain: number; 
}

export interface RecebimentoRootObject {
    Recebimentos_JSONArray: RecebimentosJSONArray[];
}

