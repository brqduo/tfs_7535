import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecebimentoPage } from './recebimento';
import { ExpandableComponent } from '../../../../components/expandable/expandable';
import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../../../app/shared/shared.module';
import { RecebimentoService } from '../../services/recebimento.service';


@NgModule({
  declarations: [
    RecebimentoPage,
  ],
  imports: [
    ChartsModule,
    SharedModule,
    IonicPageModule.forChild(RecebimentoPage),
  ],
  entryComponents: [
    ExpandableComponent,
  ],
  exports: [
    RecebimentoPage
  ],
  providers: [    
    RecebimentoService
  ]
})
export class RecebimentoPageModule {}
