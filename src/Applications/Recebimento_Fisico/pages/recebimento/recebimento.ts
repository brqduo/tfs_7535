import { Component, ViewChild, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, Refresher, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { FileOpener } from "@ionic-native/file-opener";

import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';

import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { LoginService, SecurityService } from '@ons/ons-mobile-login';
import { QuadradoService } from '../../../../services/quadraros.service';

import { HomePage } from '../../../../pages/home/home';
import { RecebimentoService } from '../../services/recebimento.service';
import { RecebimentoRootObject } from '../../services/recebimentoTypes';

import { ApproveListPermissions } from '../../../../components/approve-list/approve-listTypes';
//import { UIServices } from '../../../../services/ui.service';


@IonicPage()
@Component({
  selector: 'page-recebimento',
  templateUrl: 'recebimento.html',
})
export class RecebimentoPage {
  @ViewChild(Refresher) refresherObj: Refresher; // referencia ao objeto Refresher da pagina
  // como nao ha metodo para disparar o refresher manualmente no Ionic 3 (issues #10267 & #11393)
  // e nao se deve usar *ngIf na tag <ion-refresher> pois pode torna-lo instavel,
  // criamos o metodo refreshStart que refaz os passos de refresher._beginRefresh() 
  // e fizemos o refresher executar carregaDados(), ao contrario do que estava antes */

  showBackButton: any; // se true mostra o botao voltar
  analyticSrvLocator = 'Recebimento Físico'; // texto usado em analyticSrv.sendCustomEvent()
  showSearch: boolean = false // se true mostra o campo de pesquisa

  // controles usados para gerenciar as listas pendentes, todos, outros, etc
  dadosObj: any[] = []; // dados completos
  dadosObjOutros: any[] = []; // dados ref aba outros
  dadosObjPendentes: any[] = []; // dados ref aba pendentes
  semDadosPendentes = true; // se true mostra mensagem correspondente
  semDadosOutros = true; // se true mostra mensagem correspondente
  segment = 'pendentes'; // inicializa com uma das abas ativa

  // setup do grafico
  graficoObj = {
    type: "doughnut",
    options: {
      cutoutPercentage: 70
    },
    colors: [
      {
        backgroundColor: ["#F76C00", "#f99149", "#A6A6A6"]
      }
    ]
  };

  // permissoes
  public escopoAcess = 'ONS'; // escopo das permissoes
  public permiteExibirAnexo = false; // permissao adicional
  public permissoesPendentes: ApproveListPermissions; // lista de permissoes
  public permissoesOutros: ApproveListPermissions; // lista de permissoes
  callbackVoltar: EventEmitter<any> = new EventEmitter();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private platform: Platform,
    public http: HttpClient,

    private toastCtrl: ToastController,
    private transfer: FileTransfer,
    private file: File,
    public loadingCtrl: LoadingController,
    private fileOpener: FileOpener,

    private recebimentoBusiness: RecebimentoService, // servico de dados
    public quadradoSrv: QuadradoService, // usado na tela inicial do app
    private alertCtrl: AlertController, // usado nos dialogs de erro e de saida de tela
    //private uiServices: UIServices, // controles visuais do ONS
    private analyticSrv: AnalyticsService, // controle de auditoria do ONS
    public loginSrv: LoginService, // controle de login do ONS
    private securitySrv: SecurityService, // controle de acesso do ONS

  ) {

    // associa uma acao ao botao voltar

    this.showBackButton = navParams.get('showBackButton');
    this.platform.registerBackButtonAction(() => {
      this.voltar();
    });
    this.callbackVoltar
      .subscribe((u: any) => {
        this.navCtrl.setRoot(HomePage);
      })

  }


  // dispara o refresher manualmente

  refresherStart() {
    this.refresherObj._top = document.getElementById("header").offsetHeight + 'px';
    this.refresherObj.state = 'refreshing';
    this.refresherObj._beginRefresh();
  }


  // mostra/esconde barra de pesquisa

  searchShow(ev) {
    if (ev === true) {
      this.showSearch = true;
    } else {
      this.showSearch = false;
    }
  }


  // retorna para pagina principal

  voltar() {
    this.navCtrl.setRoot(HomePage);
    this.analyticSrv.sendCustomEvent(this.analyticSrvLocator + ' - Voltar para home');
  }


  // exibe opções de sair do aplicativo

  sair() {
    this.loginSrv.showLogoutOptions('Recebimento Físico', false, this.callbackVoltar);

  }



  // mostra mensagem em caixa de dialogo

  mostraAlerta(titulo: string, mensagem: string) {
    // this.uiServices.showAlertMessage(titulo, mensagem, '#D10429');
    let alert = this.alertCtrl.create({
      title: titulo,
      message: mensagem,
      buttons: ["OK"]
    });
    alert.present();
  }



  // executado quando a pagina esta para entrar e se tornar a pagina ativa

  ionViewWillEnter() {//this.mostraAlerta("Mensagem", "Ocorreu um erro qualquer ao atualizar a lista.");

    // verificando permissoes
    this.permissoesPendentes = new ApproveListPermissions();
    this.permiteExibirAnexo = this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', 'RecebimentoFísico:ExibirAnexos');
    this.permissoesPendentes.canApprove = this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', 'RecebimentoFísico:Aprovar');
    this.permissoesPendentes.canReprove = this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', 'RecebimentoFísico:Reprovar');
    this.permissoesPendentes.canList = this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', 'RecebimentoFísico:ListarRecebimentos');
    this.permissoesOutros = new ApproveListPermissions();
    this.permissoesOutros.canApprove = false;
    this.permissoesOutros.canReprove = false;
    this.permissoesOutros.canList = this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', 'RecebimentoFísico:ListarRecebimentos');

    // se receber permissao para listar, dispara o refresher que ira chamar carregarDados()
    if (this.permissoesPendentes.canList) {
      this.refresherStart();
    }

    this.analyticSrv.sendCustomEvent('Entrou no ' + this.analyticSrvLocator);
  }


  // disparado quando nova aba eh selecionada

  changeTab() {
    this.analyticSrv.sendCustomEvent(this.analyticSrvLocator + ' - Selecionou: ' + this.segment);
  }


  // carrega os dados mostrados nos cartoes

  carregarDados(refresher?: any) {
    if (refresher) {
      this.analyticSrv.sendCustomEvent('Atualizou tela via Refresher ' + this.segment);
    }

    this.semDadosPendentes = false;
    this.semDadosOutros = false;

    this.recebimentoBusiness.listarRecebimentos().subscribe(
    //  this.http.get("./assets/data/recebimento-from-ons.json").subscribe(    

      (dados: RecebimentoRootObject) => {
        // ATENÇÃO - é obrigatorio informar uma nova lista e não adicionar a lista valida. 
        // O componente de tela não detecta itens novos na lista, somente se a lista foi trocada.
        this.dadosObjPendentes = [];
        this.dadosObjOutros = [];
        if (!dados.Recebimentos_JSONArray) {
          dados.Recebimentos_JSONArray = [];
        }
        for (let i = 0; i < dados.Recebimentos_JSONArray.length; i++) {
          const dadoOriginal = dados.Recebimentos_JSONArray[i];
          if (!dadoOriginal.TotalRecvAmt) dadoOriginal.TotalRecvAmt = 0;
          if (!dadoOriginal.PaidAmt) dadoOriginal.PaidAmt = 0;
          if (!dadoOriginal.AmtRemain) dadoOriginal.AmtRemain = 0;
          // dadoOriginal.TotalRecvAmt = 20;
          // dadoOriginal.PaidAmt = 50;
          // dadoOriginal.AmtRemain = 30;

          let remainPositivo =
            dadoOriginal.AmtRemain > 0 ? dadoOriginal.AmtRemain : 0;
          let total =
            remainPositivo + dadoOriginal.TotalRecvAmt + dadoOriginal.PaidAmt;
          const dado = {
            objeto: dadoOriginal.Objeto,
            titulo: dadoOriginal.Name1,
            status: dadoOriginal.OnsStsRecDsc,
            valor: "R$" + dadoOriginal.TotalRecvAmt.toLocaleString("pt-BR"),
            valorCompromissado:
              "R$" + dadoOriginal.PaidAmt.toLocaleString("pt-BR"),
            valorRemanescente:
              "R$" + dadoOriginal.AmtRemain.toLocaleString("pt-BR"),
            grafico1: dadoOriginal.PaidAmt,
            grafico2: dadoOriginal.TotalRecvAmt,
            grafico3: remainPositivo,
            percentual1:
              total > 0
                ? Math.round(
                  ((dadoOriginal.TotalRecvAmt + dadoOriginal.PaidAmt) /
                    total) *
                  100
                )
                : 0,
            percentual2:
              total > 0 ? Math.round((dadoOriginal.PaidAmt / total) * 100) : 0,
            unidade: dadoOriginal.BusinessUnitRecv,
            ID: dadoOriginal.NfBrl,
            dataCriado: dadoOriginal.NfBrlDate,
            expanded: false,
            nRecebimento: dadoOriginal.ReceiverId,
            operId: dadoOriginal.OpridApprovedBy,
            onsTipo: dadoOriginal.OnsTipo,
            anexos: [],
            valoresGrafico: [],
            mainColor: '#F76C00'
          };
          dado.valoresGrafico = [dado.grafico1, dado.grafico2, dado.grafico3];

          dados.Recebimentos_JSONArray[i].AttachList.forEach(anexo => {
            dado.anexos.push({
              descricao: this.trataNomeArquivo(anexo.AttachUserFile),
              arquivo: anexo.AttachUserFile,
              link: anexo.DownloadLink
            });
          });
          const element = dado;

          if (dado.onsTipo == "A") {
            this.dadosObjPendentes.push(element);
          } else {
            this.dadosObjOutros.push(element);
          }
        }

        if (this.dadosObjPendentes.length == 0) this.semDadosPendentes = true;
        if (this.dadosObjOutros.length == 0) this.semDadosOutros = true;

        if (refresher) refresher.complete();
        this.analyticSrv.sendCustomEvent('Carregou dados ' + this.analyticSrvLocator, {
          "Total": this.dadosObj.length,
          "Pendentes": this.dadosObjPendentes.length,
          "Outros": this.dadosObjOutros.length
        });

        // debugging
        console.log('Carregou dados ' + this.analyticSrvLocator, this.dadosObj);
      },

      error => {
        this.analyticSrv.sendNonFatalCrash('Erro ao carregar dados em ' + this.analyticSrvLocator, error);
        if (error && error.message) {
          this.mostraAlerta("Erro", "Ocorreu um erro ao atualizar a lista.");
        }
        if (error && error.mensagem) {
          this.mostraAlerta("Erro", error.mensagem);
        }
        if (typeof error === 'string') {
          this.mostraAlerta("Erro", error);
        }
        if (refresher) refresher.complete();
        console.log("resultado com erro em business:" + JSON.stringify(error));
      }

    );

  }


  // disparado quando o item eh expandido ou contraido 

  expandido(item: any, expandido: boolean) {
    console.log("registar log " + item.nome + " expandido: " + expandido);
    this.analyticSrv.sendCustomEvent(item.expanded ? 'Expandiu card' : 'Reduziu card', {
      RecebimentoId: item.nRecebimento,
      UnidadeDeNegocio: item.unidade,
      Status: item.status,
      Fornecedor: item.titulo
    });
  }


  // disparado quando o botao aprovado ou reprovado do item eh pressionado 

  aprovarReprovar(item: any, aprovado: boolean) {
    item.busy = true;
    const textoAcao = item.acao == "aprovar" ? "Aprovação" : "Reprovação";
    let parameter = {
      RecebimentoId: item.nRecebimento,
      UnidadeDeNegocio: item.unidade,
      Comentario:
        item.acao == "aprovar"
          ? "Aprovado via app MeuONS"
          : "Reprovado via app MeuONS"
    };
    item.comunicando = true;

    this.recebimentoBusiness
      .aprovarReprovarRecebimento(item.acao, parameter)
      .subscribe(
        (response: any) => {
          item.comunicando = false;
          if (response.type && response.type == "ERROR") {
            let toast = this.toastCtrl.create({
              message: "Erro ao enviar " + textoAcao + " - " + response.message,
              duration: 5000,
              position: "botton"
            });
            toast.present();
            this.analyticSrv.sendCustomEvent("Não foi possível " + item.acao, response);
          } else {
            this.dadosObjPendentes.forEach((element, index) => {
              if (
                item.nRecebimento == element.nRecebimento &&
                item.unidade == element.unidade
              ) {
                this.dadosObjPendentes.splice(index, 1);
                this.dadosObjPendentes = this.dadosObjPendentes.slice();
              }
            });
            item.busy = false;
            console.log("Atualização de status concluída");
            let toast = this.toastCtrl.create({
              message: textoAcao + " enviada com sucesso",
              duration: 3000,
              position: "botton"
            });
            toast.present();
            this.analyticSrv.sendCustomEvent(textoAcao + " enviada com sucesso", {
              RecebimentoId: item.nRecebimento,
              UnidadeDeNegocio: item.unidade,
              Status: item.status,
              Fornecedor: item.titulo
            });
            this.carregarDados(false);
          }
        },

        (error: any) => {
          item.comunicando = false;
          item.busy = false;
          let mensagemErro = textoAcao + " não efeturada. Ocorreu um erro.";
          if (error && error.mensagem) {
            mensagemErro = error.mensagem;
          }
          let toast = this.toastCtrl.create({
            message: mensagemErro,
            duration: 3000,
            position: "botton"
          });
          toast.present();
          this.analyticSrv.sendNonFatalCrash(textoAcao + " não efeturada. Ocorreu um erro.", error)
        }
      );


  }


  // disparado quando o botao cancelar do evento de aprovacao ou reprovacao do item eh pressionado 

  envioCancelado(item: any) {
    console.log("registar log cancelado envio de " + item.nome);
    this.analyticSrv.sendCustomEvent('Cancelou evento', {
      RecebimentoId: item.nRecebimento,
      UnidadeDeNegocio: item.unidade,
      Status: item.status,
      Fornecedor: item.titulo
    });
  }


  // retorna o nome do arquivo

  trataNomeArquivo(nome: string) {
    let retorno = "";
    if (nome) {
      let posicaoUltimaBarra = nome.lastIndexOf("/");
      if (posicaoUltimaBarra > 0) {
        retorno = nome.substring(posicaoUltimaBarra + 1);
      } else {
        retorno = nome;
      }
      retorno = retorno.replace(/-/g, " ").replace(/_/g, " ");
    }
    return retorno;
  }


  // baixa e abre o arquivo anexo

  public mimeTypeArray: { [key: string]: string } = {
    png: "image/png",
    pdf: "application/pdf",
    jpg: "image/jpeg",
    jpeg: "image/jpeg",
    jpe: "image/jpeg"
  };

  removerAcentos(s) {
    const map = {
      â: "a",
      Â: "A",
      à: "a",
      À: "A",
      á: "a",
      Á: "A",
      ã: "a",
      Ã: "A",
      ê: "e",
      Ê: "E",
      è: "e",
      È: "E",
      é: "e",
      É: "E",
      î: "i",
      Î: "I",
      ì: "i",
      Ì: "I",
      í: "i",
      Í: "I",
      õ: "o",
      Õ: "O",
      ô: "o",
      Ô: "O",
      ò: "o",
      Ò: "O",
      ó: "o",
      Ó: "O",
      ü: "u",
      Ü: "U",
      û: "u",
      Û: "U",
      ú: "u",
      Ú: "U",
      ù: "u",
      Ù: "U",
      ç: "c",
      Ç: "C"
    };
    return s.replace(/[\W\[\] ]/g, function (a) {
      return map[a] || a;
    });
  }

  downloadAnexo(item) {
    if (!this.permiteExibirAnexo) {
      this.mostraAlerta('Falta permissão', 'Sem permissão de acesso ao anexo');
      this.analyticSrv.sendCustomEvent('Tentou fazer o download sem permissão de acesso.');
      return;
    }
    let extension = item.arquivo
      .substring(item.arquivo.lastIndexOf(".") + 1)
      .toLowerCase();
    const fileTransfer: FileTransferObject = this.transfer.create();

    let path = null;

    if (this.platform.is("ios")) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.dataDirectory;
    }
    let loading = this.loadingCtrl.create({
      spinner: "crescent",
      content: "Aguarde..."
    });
    loading.present();
    fileTransfer
      .download(item.link, path + this.removerAcentos(item.arquivo), false, {
        headers: {
          Authorization: "Bearer " + this.loginSrv.tokenSrv.getToken().access_token
        }
      })
      .then(
        entry => {
          loading.dismiss();
          let entryBis = entry.toURL();
          let mime = this.mimeTypeArray[extension];
          this.fileOpener
            .open(entryBis, mime)
            .then(() => {
              console.log("File is opened");
              this.analyticSrv.sendCustomEvent('Abriu anexo', { "Arquivo": item.arquivo });
            })
            .catch(e => {
              console.log("Error opening file", e);
              if (e.message.indexOf("Activity not found") >= 0) {
                this.mostraAlerta(
                  "Erro",
                  "Não existe um aplicativo padrão para abrir arquivos ." +
                  extension
                );
                this.analyticSrv.sendNonFatalCrash("Não existe um aplicativo padrão para abrir arquivos ." +
                  extension, e);
              } else {
                this.mostraAlerta(
                  "Erro",
                  "Erro ao tentar abrir o arquivo: " + e.message
                );
                this.analyticSrv.sendNonFatalCrash("Erro ao tentar abrir o arquivo: " + item.arquivo, e);
              }
            });
        },
        error => {
          loading.dismiss();
          this.mostraAlerta(
            "Erro",
            "Não foi possível fazer o download do anexo."
          );
          console.log(JSON.stringify(error));
          this.analyticSrv.sendNonFatalCrash("Erro ao tentar acessar o anexo: " + item.arquivo, error);
        }
      );
  }

  ionViewDidLeave() {
    this.platform.registerBackButtonAction(() => {
      //
    });
  }
}
