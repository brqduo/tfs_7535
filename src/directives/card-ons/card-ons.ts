import { Directive, ElementRef } from '@angular/core';

/**
 * Generated class for the CardOnsDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[card-ons]' // Attribute selector
})
export class CardOnsDirective {


  constructor(el: ElementRef) {
    
    el.nativeElement.style.backgroundColor = 'yellow';

  }

}
