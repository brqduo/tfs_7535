import { NgModule } from '@angular/core';
import { CardOnsDirective } from './card-ons/card-ons';
@NgModule({
	declarations: [CardOnsDirective],
	imports: [],
	exports: [CardOnsDirective]
})
export class DirectivesModule {}
