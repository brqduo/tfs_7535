import { Injectable } from "@angular/core";
import { LoadingController, Platform } from "ionic-angular";
import { UIServices } from "./ui.service";
import { AnalyticsService } from "@ons/ons-mobile-analytics";

@Injectable()
export class UtilsService {
    private _ultimaVerificacaoEntrada = null;
    public get ultimaVerificacaoEntrada() {
        return this._ultimaVerificacaoEntrada;
    }
    public set ultimaVerificacaoEntrada(value) {
        this._ultimaVerificacaoEntrada = value;
    }
    constructor(
        private loadingCtrl: LoadingController,
        private uiServices: UIServices,
        private platform: Platform,
        private analyticSrv: AnalyticsService
    ) { }
    public launchApp(pageName: string, url: string, packageName: string, color: string, urlLoja: string) {

        if (this.platform.is('cordova')) {
            let loading = this.loadingCtrl.create({
                spinner: "crescent",
                content: "Carregando..."
            });
            loading.present();


            window["plugins"].launcher.launch({ packageName: packageName, uri: url },
                data => {
                    loading.dismiss();
                    console.log('app launched' + JSON.stringify(data));
                    if (!data.isLaunched) {
                        window.open(urlLoja, '_system');
                        this.analyticSrv.sendCustomEvent(pageName + ' - Abrindo loja para instalar app');
                    }
                    else {
                        this.analyticSrv.sendCustomEvent(pageName + ' - App acionado');
                    }
                },

                errMsg => {
                    loading.dismiss();
                    console.log('Erro no acionamento, abrindo loja para instalar app. Erro:' + errMsg);
                    window.open(urlLoja, '_system');
                    this.analyticSrv.sendNonFatalCrash(pageName + ' - Erro no acionamento, abrindo loja para instalar app. Erro: ', errMsg);

                }
            );
            //}

        }
        else {
            this.uiServices.showAlertMessage('Erro', 'Não é possível acionar um aplicativo no browser', color);

        }
    }
}