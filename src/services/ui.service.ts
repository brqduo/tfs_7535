
import { Injectable } from "@angular/core";
import { ModalController, ToastController } from "ionic-angular";
import { MsgModalPage } from '../components/msg-modal/msg-modal';
import { MsgAlertPage } from '../components/msg-alert/msg-alert';
import { MsgConfirmPage } from "../components/msg-input/msg-input";
import { Observable } from "rxjs";

@Injectable()
export class UIServices {
    posMsg: any = ['bottom', 'top', 'middle'];
    constructor(private modalCtrl: ModalController,private toast: ToastController) {
    }

    showToastAlert(mensagem, delay?: number, pos?: string, titulo?: string, btnClose?: boolean) {
       
        delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
        pos = ((this.posMsg.indexOf(pos) === -1) ? 'bottom' : pos);
        btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
        mensagem = (((titulo == undefined) || (titulo == null)) ? mensagem : titulo + '\n\n' + mensagem);
        const toast = this.toast.create({ message: mensagem, duration: delay, position: pos, showCloseButton: btnClose, closeButtonText: 'Fechar' })
        toast.present();
    }

    showFooterMessage(title: string, message: string, color: string) {

        this.modalCtrl.create(MsgModalPage,
            {
                title: title,
                message: message,
                color: color
            },
            { cssClass: 'msg-modal' })
            .present();
    }

    showAlertMessage(title: string, message: string, color: string) {

        this.modalCtrl.create(MsgAlertPage,
            {
                title: title,
                message: message,
                color: color
            },
            { cssClass: 'msg-alert' })
            .present();

    }

    showInputMessage(title: string, message: string, color: string): Observable<string> {
        return Observable.create(observer => {
            let modalConfirm = this.modalCtrl.create(
                MsgConfirmPage,
                {
                    title: title,
                    message: message,
                    color: color,
                   
                }, { cssClass: 'msg-input' });
            modalConfirm.onDidDismiss(data => {
                if (data) {
                    observer.next(data.message);
                }
                else
                    observer.next("");
            });

            modalConfirm.present();
        }
        )
    }

}