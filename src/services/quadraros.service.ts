import { Injectable } from '@angular/core';

/** CONFIG */
import { Parameters } from '../app/parameters';


@Injectable()
export class QuadradoService {
  qtdItems: number = 2;
  AppsAllowed: number = 0;
  constructor() {

    // for (var x = 2; x < 16; x++) {
    //     for (var i = 1; i <= x; i++) {
    //         console.log(this.classeQuadradoPerfeito(i, x));
    //     }
    // }
  }



  public get quadradosMockList() {
    return [
      {
        "ordem": 1,
        "nome": "GestorNet",
        "cor": "#4B80BD",
        "icone": "./assets/icon/icon-gestornet.png",
        "destino": "gestornet",
        "AppName": "GestorNetPage",
        "Operations": ["Menu:GestorNet"],
        "DeepLink": "https://onsgestornet.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.gestornet&ibi=br.org.ons.enterprise.gestornet&ius=gestornet&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&efr=1"
      }, {
        "ordem": 2,
        "nome": "Contatos",
        "cor": "#486018",
        "icone": "./assets/icon/icon-contatos.png",
        "destino": "contatos",
        "AppName": "ContatosPage",
        "Operations": ["Menu:Contatos"],
        "DeepLink": "https://onscontatos.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.contatos&ibi=br.org.ons.enterprise.contatos&ius=contatos&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&efr=1"
      }, {
        "ordem": 3,
        "nome": "Recebimento Físico",
        "cor": "#F76C00",
        "destino": "Recebimento_Fisico",
        "AppName": "RecebimentoPage",
        "sigla": "RF",
        "Operations": ["Menu:RecebimentoFísico"]
      }, {
        "ordem": 4,
        "nome": "Aprovação de Férias",
        "cor": "#D10429",
        "destino": "aprovacao-ferias",
        "AppName": "AprovacaoFeriasPage",
        "sigla": "AF",
        "Operations": ["Menu:AprovaçãoFérias"]
      }, {
        "ordem": 5,
        "nome": "Catraca Online",
        "cor": "#4f8ad8",
        "destino": "Catraca_Online",
        "AppName": "CatracaPage",
        "sigla": "CO",
        "Operations": ["Menu:CatracaOnline"]
      }, {
        "ordem": 6,
        "nome": "Aprovação de MO",
        "cor": "green",
        "destino": "aprovacao-mo",
        "AppName": "AprovacaoMOPage",
        "sigla": "MO",
        "Operations": ["Menu:AprovaçãoMO"]
      }, {
        "ordem": 7,
        "nome": "Portal de TI",
        "cor": "#a6aab2",
        "destino": "",
        "AppName": "",
        "sigla": "PTI",
        "Operations": ["Menu:PortalTI"],
        "externalUrl": "https://onsbrtst.sharepoint.com/sites/portalti"
      }
    ]
  }


  /**
   * Retorna a distância do quadrado perfeito anterior mais próximo
   * @param num quantidade de icones na tela. Varia de 2 a 15
   */
  private distanciaQuadradoPerfeito(num): any {
    var result: any = {};
    result.distancia = -1;
    for (var i = 0; i <= Parameters.quadradoPerfeito.length - 1; i++) {
      if (Parameters.quadradoPerfeito[i].qp === num) {
        result = Parameters.quadradoPerfeito[i];
        result.distancia = 0;
        // console.log('FINAL 1 RESULT', result);
      }
    }

    if (result.distancia === -1) {
      for (var x = 0; x <= Parameters.quadradoPerfeito.length - 1; x++) {

        if (Parameters.quadradoPerfeito[x].qp < num) {
          result = Parameters.quadradoPerfeito[x];
          result.distancia = (num - Parameters.quadradoPerfeito[x].qp);
          //   console.log('FINAL 2 RESULT', result);
          //this.Parameters.quadradoPerfeito[x - 1].qp
        }
      }
    }
    return result;
  }

  /**
   * Retorna a classe a ser utilizada no item
   * @param posicao Posição do Item a ser renderizado
   * @param totalPagina Total de itens da página que esta sendo renderizada
   */
  public classeQuadradoPerfeito(posicao, totalPagina): string {
    if (this.rangeValid(this.qtdItems)) {
      if (this.LimitesQuadrados(posicao, totalPagina)) {
        var quadrado = this.distanciaQuadradoPerfeito(totalPagina);
        //    console.log('hell', quadrado);
        if (totalPagina === 2 || totalPagina === 0) {
          //      console.log('Acho q pode ser');
          return 'w-100';
        } else {
          if (posicao > quadrado.distancia) {
            //     console.log('posição maior', quadrado.distancia);
            //    console.log(quadrado.classes[0]);

            return quadrado.classes[0];
          } else {
            //    console.log('posição menor');

            return quadrado.classes[quadrado.distancia];
          }
        }
      } else {
        return 'w-100'
        //return 'mensagem de erro posicao ' + posicao + '  -  ' + totalPagina
      }
    } else {
      console.log('Erro na qtd de quadrados');

    }
  }

  /**
   * 
   * rangeValid
   * 
   * Confere o tamanho do Quadrado e retorna bool conforme o críterio estabelecido
   * 
   * @param tamanho Tamanho do Quadrado
   */
  public rangeValid(tamanho): boolean {
    return ((tamanho >= 1) && (tamanho < 16))
  }

  /**
   * 
   * LimitesQuadrados
   * 
   * Retorna booleano conforme o atendimento dos critérios de limite de quadrados tela.
   * 
   * @param posicao Posicao do Quadrado
   * @param totalPagina Total de Quadrados da Pagina
   */
  private LimitesQuadrados(posicao: number, totalPagina: number): boolean {
    var retorno = (((posicao >= 1) && (posicao < 16)) && ((totalPagina > 1) && (totalPagina < 16)));
    // console.log(retorno);

    // if (retorno) {
    //     console.log(posicao, (posicao < totalPagina + 1));

    //     retorno = (posicao < totalPagina + 1)
    //     console.log(retorno);

    // }
    if (!retorno) {
      //   console.log('retorno false',posicao, totalPagina);
      //Gravar Log Error
    }
    return retorno;
  }

  /**
   * 
   * updateAppsAllowed
   * 
   * Atualiza o número de apps permitidos para o usuário.
   * 
   * @param totalPermitidos Número de Apps permitidos para o usuário
   */
  public updateAppsAllowed(totalPermitidos: number) {
    this.AppsAllowed = totalPermitidos == undefined ? 0 : totalPermitidos;
  }

}
