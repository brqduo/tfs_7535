/* ok */
import { Component } from '@angular/core';
import { Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage, ImageService, EnvironmentService, LoginService, UtilService } from '@ons/ons-mobile-login';
import { Push, PushOptions, PushObject } from '@ionic-native/push';
import { Config } from '../enviroment/environment';
import { AppVersion } from '@ionic-native/app-version';
import { SimulaSplashPage } from '../pages/simula-splash/simula-splash';

import { HomePage } from '../pages/home/home';
import { UtilsService } from '../services/utils.service';
/* new ... */

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  constructor(
    private platform: Platform,
    public statusBar: StatusBar,
    public loginSrv: LoginService,
    public imageSrv: ImageService,
    public envSrv: EnvironmentService,
    public alertCtrl: AlertController,
    public utilSrv: UtilService,
    public splashScreen: SplashScreen,
    public utilsSrv: UtilsService,
    private push: Push,
    public app: App,
    public alertController: AlertController
  ) {
    platform.ready().then(() => {

      console.log('MeuONS.platform.ready');

      statusBar.styleDefault();
      imageSrv.SetDefault();
      loginSrv.setAplicationName('Mobile.MeuONS', 'MeuONS');
      envSrv.setConfig(Config.loginServiceConfig);
      console.log('loginServiceConfig:' + JSON.stringify(Config.loginServiceConfig));
      this.app.getActiveNav().setRoot(HomePage);
      //forca entrar no splash simulado
      // this.app.getActiveNav().setRoot(SimulaSplashPage).then(() => {
      //   console.log('MeuONS.setroot');
      //   this.splashScreen.hide();
      //   this.loginSetup();
      // });

   //   this.loginSetup();

      //retorno ao aplicativo que estava em back
      platform.resume.subscribe(() => {
        console.log('MeuONS.platform.resume');
/*         if (utilsSrv.ultimaVerificacaoEntrada) {
          console.log('ultima verificacao versao: ' + utilsSrv.ultimaVerificacaoEntrada);
          let tempoMinutosUltimaVerificacao: Number;
          if (utilsSrv.ultimaVerificacaoEntrada) {
            tempoMinutosUltimaVerificacao = new Date().getMinutes() - utilsSrv.ultimaVerificacaoEntrada.getMinutes();
            console.log('Minutos da ultima verificacao: ' + tempoMinutosUltimaVerificacao);
          }
          if (utilsSrv.ultimaVerificacaoEntrada == null || tempoMinutosUltimaVerificacao >= 15) {
            console.log('vai rodar verificacoes do resume');
            utilsSrv.ultimaVerificacaoEntrada = new Date();
            this.loginSrv.checkVersion();
          }
        } */
      });

/*       this.loginSrv.onConectedChange
        .subscribe((u: any) => {
          if (u !== undefined) {
            if (u.Connected) {
              console.log('castle 2', u);
              this.app.getActiveNav().setRoot(HomePage);
            }
          }
        })

      if (this.platform.is('cordova')) {
        this.pushSetup();
      } */
    });
  }

  loginSetup(goToRoot: boolean = true) {
    this.loginSrv.checkLoginStatus().subscribe(canAutoLogin => {
      if (canAutoLogin) {
        console.log('permite login auto1');
        this.loginSrv.loginAutomatico3().subscribe(
          () => {
            this.splashScreen.hide();
            console.log('Logou com sucesso automaticamente');
            if (goToRoot) {
              this.app.getActiveNav().setRoot(HomePage);
            }
          },
          err => {
            console.log('Não inicializou automaticamente. ' + err);
            this.splashScreen.hide();
            this.app.getActiveNav().setRoot(LoginPage);
          }
        );
        console.log('permite login auto2');
      }
      else {
        this.splashScreen.hide();
        console.log('não permite login auto');
        this.app.getActiveNav().setRoot(LoginPage);
      }
    });
  }

  updateVersion(checkVersionReturn: any) {
    console.log('vai atualizar a versao');
    let url: string;
    if (this.platform.is('ios')) {
      url = checkVersionReturn.urlLojaIOS
    } else {
      url = checkVersionReturn.urlLojaAndroid
    }
    console.log('Update url: ' + url);
    window.open(url, '_system');
  }

  pushSetup() {

    // to check if we have permission
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We do not have permission to send push notifications');
        }

      });

    const options: PushOptions = {
      android: {
        senderID: '596167917795'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      console.log("Notificação recebida e o app está aberto: " + JSON.stringify(notification));
      if (notification.additionalData.foreground) {
        let youralert = this.alertCtrl.create({
          title: 'New Push notification',
          message: notification.message
        });
        youralert.present();
      }
    });

    pushObject.on('registration').subscribe((data: any) => {
      console.log("device registered -> ", JSON.stringify(data));
      // this.saveToken(data.registrationId);

      let topic = "publicacoes";
      pushObject.subscribe(topic).then((res: any) => {
        console.log("subscribed to topic: ", res);
      });
    });

    pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
  }

}