import { OnsCardComponent } from './../../components/ons-card/ons-card';
import { ModuleWithProviders, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { ExpandableComponent } from "../../components/expandable/expandable";
import { ApproveList } from "../../components/approve-list/approve-list";
import { ApproveItemComponent } from '../../components/approve-item/approve-item';
import { IonicModule } from "ionic-angular";
import { RoundProgressModule } from "angular-svg-round-progressbar";
import { MsgAlertPage } from "../../components/msg-alert/msg-alert";
import { MsgModalPage } from "../../components/msg-modal/msg-modal";
import { SegmentComponent } from '../../components/segment/segment';
import { ToolbarHeaderComponent } from '../../components/toolbar-header/toolbar-header';
import { DetalheAprovacaomoPage } from '../../Applications/aprovacao-mo/pages/detalhe-aprovacaomo/detalhe-aprovacaomo';
import { CelulaComponent } from '../../components/celula/celula';
import { SwiperPaginationComponent } from '../../components/swiper-pagination/swiper-pagination';
import { HourGlassComponent } from '../../components/hour-glass/hour-glass';
import { ItemMoComponent } from '../../Applications/aprovacao-mo/components/item-mo/item-mo';


@NgModule({
    imports: [
        IonicModule,
        RoundProgressModule
    ],
    entryComponents: [
        DetalheAprovacaomoPage
    ],
    providers: [

    ],
    declarations: [
        ExpandableComponent,
        ApproveList,
        MsgAlertPage,
        MsgModalPage,
        ApproveItemComponent,
        OnsCardComponent,
        SegmentComponent,
        ToolbarHeaderComponent,
 
        DetalheAprovacaomoPage,
        CelulaComponent,
        SwiperPaginationComponent,
        HourGlassComponent,
        ItemMoComponent,
        
    ],
    exports: [

        ExpandableComponent,
        MsgAlertPage,
        ApproveList,
        RoundProgressModule,
        MsgModalPage,
        ApproveItemComponent,
        OnsCardComponent,
        ToolbarHeaderComponent,
        ItemMoComponent,

        DetalheAprovacaomoPage,
        CelulaComponent,
        SwiperPaginationComponent

    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
})


export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}
