
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HttpClientModule } from '@angular/common/http';
import { Push } from '@ionic-native/push';
/** SERVICES */
import { QuadradoService } from './../services/quadraros.service';
import {
  LoginPage,
  LoginService,
  UtilService,
  TokenService,
  ErrorService,
  StorageService,
  ImageService,
  SecurityService,
  NetWorkService,
  EnvironmentService,
  UserService,
  OnsPackage
} from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer } from '@ionic-native/file-transfer';

import { CardFlipComponent } from '../components/cardFlip/cardFlip';

import { UIServices } from '../services/ui.service';
import { MsgAlertPageModule } from '../components/msg-alert/msg-alert.module';
import { UtilsService } from '../services/utils.service';
import { MsgModalPageModule } from '../components/msg-modal/msg-modal.module';
import { MsgConfirmPageModule } from '../components/msg-input/msg-input.module';
import { Crashlytics } from '@ionic-native/fabric';
import { SimulaSplashPage } from '../pages/simula-splash/simula-splash';
import { AppVersion } from '@ionic-native/app-version';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CardFlipComponent,
    SimulaSplashPage
  ],
  imports: [
    BrowserModule,
    OnsPackage.forRoot(),
    MsgAlertPageModule,
    HttpClientModule,
    MsgAlertPageModule,
    MsgModalPageModule,
    MsgConfirmPageModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      backButtonIcon: 'ios-arrow-back',
      backButtonText: ''
    }
    ),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    CardFlipComponent,
    SimulaSplashPage

  ],
  providers: [
    StatusBar,
    QuadradoService,
    SplashScreen,
    LoginService,
    UtilService,
    AnalyticsService,
    TokenService,
    ErrorService,
    StorageService,
    ImageService,
    SecurityService,
    NetWorkService,
    EnvironmentService,
    UserService,
    FingerprintAIO,
    File,
    FileTransfer,
    FileOpener,
    Crashlytics,
    UIServices,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UtilsService,
    Push,
    AppVersion
  ]
})
export class AppModule { }
