import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { QuadradoService } from '../../services/quadraros.service'
import { HttpClient } from '@angular/common/http';
import { LoginService, SecurityService } from '@ons/ons-mobile-login';
import { Config } from '../../enviroment/environment';
import { UtilsService } from '../../services/utils.service';
import { Parameters } from '../../app/parameters';
import { Observable } from 'rxjs';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  posicao = 0;
  totalPagina = 2;
  classe = '';

  flip = false;
  data: any;
  maxHeight = 90;
  cslasseColuna = 'w-100';
  flipcardOBJ = {
    isFlipAble: false,
    transitionClass: false,
    whenToFlip: Observable.interval(3000),
    timerFlipCardClass: Observable.timer(1000)
  }
  Tela_Saida_Visivel = false;
  IsAllowed = false;
  NoAppAllowed = Parameters.mensagemGenerica.SEM_APPS_PERMITDOS;

  constructor(public navCtrl: NavController
    , public quadradoSrv: QuadradoService
    , private loginSrv: LoginService
    , private securitySrv: SecurityService
    , private utilsSrv: UtilsService
    , private platform: Platform
    , public http: HttpClient) {
    this.flipcardOBJ.whenToFlip.subscribe((res) => {
      console.log('Ja rodou: ', res + ' vezes');
      this.startFlipCard();
    })

  }

  ionViewDidLoad() {
    // this.loadData();
    console.log('Carregou home');
    if (this.quadradoSrv.rangeValid(this.quadradoSrv.qtdItems)) {
      //this.loadData();
      this.data = Config.Menu;
      console.log('O q vem dentro de Data?', this.data);

    } else {
      alert('QTD de items invalida')
      this.navCtrl.pop()
    }
    //    console.log('Data DidLoad: ', this.data);

    /* somente para teste */
    /* exemplo chamando metodo para alterar o valor do badge */

    // setInterval(() => {
    //   this.showBadge(2, Math.floor(Math.random() * 4).toString() 
    //   );
    // },
    //   5000
    // );

  }

  getUrlParameter(url, name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(url);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };
  /**
   * 
   * goToApp
   * 
   * Abre aplicativo
   * 
   * @param item Objeto Menu
   */
  goToApp(item, backButton) {
    //debugger;
    if (item) {
      if (item.DeepLink) {
        let packageName: string;
        let urlLoja: string;
        if (this.platform.is("ios")) {
          packageName = this.getUrlParameter(item.DeepLink, 'ibi');
          urlLoja = this.getUrlParameter(item.DeepLink, 'ifl');
        }
        else {
          packageName = this.getUrlParameter(item.DeepLink, 'apn');
          urlLoja = this.getUrlParameter(item.DeepLink, 'afl');
        }
        console.log('Acionando: ' + packageName);
        this.utilsSrv.launchApp(item.nome, item.DeepLink, packageName, item.cor, urlLoja)
      }
      else if (item.externalUrl) {
        window.open(item.externalUrl, '_system');
      }
      else if (backButton != undefined)
        this.navCtrl.setRoot(item.AppName, { showBackButton: backButton });
      else
        this.navCtrl.setRoot(item.AppName, { showBackButton: true });
    }
  }

  startFlipCard() {
    this.flipcardOBJ.isFlipAble = !this.flipcardOBJ.isFlipAble;
    this.flipcardOBJ.transitionClass = !this.flipcardOBJ.transitionClass;
    this.flipcardOBJ.timerFlipCardClass.subscribe(() => {
      this.flipcardOBJ.transitionClass = !this.flipcardOBJ.transitionClass;
    })
  }


  renderBack(item: string) {
    let retorno = ''
    switch (item) {
      case 'Catraca':
        retorno = '<cardflip></cardflip>'
        break;

    }
    return retorno;
  }

  /**
   * 
   * getSomeClass
   * 
   * Recupera Classe
   * 
   * @param item Objeto Menu
   */
  getSomeClass(item: any) {
    const ordem = item == undefined ? '' : item.ordem;
    return this.quadradoSrv.classeQuadradoPerfeito(ordem, this.data.length);
  }

  /**
   * 
   * getMyStyles
   * 
   * Recupera caracteristicas do Objeto Menu.
   * 
   * @param item Objeto Menu
   */
  getMyStyles(item) {
    if (item) {
      let tam = this.SetHeigth(this.data.length);
      let myStyles = {
        'background-color': item.cor,
        'height': tam
      };
      return myStyles;
    }
  }

  /**
   * 
   * getBadgeStyles
   * 
   * Recupera caracteristicas do Objeto Menu.
   * 
   * @param item Objeto Menu
   */
  getBadgeStyles(item) {
    if (item) {
      let styles = {
        'color': item.cor
      };
      return styles;
    }
  }

  /**
   * 
   * SetHeigth
   * 
   * Define tamanho da página conforme a quantidade de apps.
   * 
   * @param tamanho Size
   * 
   */
  SetHeigth(tamanho): string {
    var result = '';
    switch (tamanho) {
      case (tamanho >= 2 && tamanho <= 4): {
        result = '50'
        break;
      }
      case (tamanho === 7 || tamanho === 8 || tamanho === 10 || tamanho === 11 || tamanho === 12): {
        result = '22.5'
        break;
      }
      case (tamanho === 6 || tamanho === 5 || tamanho === 9): {
        result = '30'
        break;
      }
      case (tamanho >= 13): {
        result = '18'
        break;
      }
    }
    return result + 'vh'
  }

  /**
   * getClass
   * 
   * Recupera Classes
   */
  getClass() {
    console.log('passei pro getClass');
    this.classe = this.quadradoSrv.classeQuadradoPerfeito(this.posicao, this.totalPagina)
  }

  /**
   * 
   * getJSON
   * 
   * Recupera menu.json com os apps cadastrados.
   * DOIS APPS - https://api.myjson.com/bins/16lsro
   * UM APP (Recebimento) - https://api.myjson.com/bins/1gmrd8
   * VAZIO https://api.myjson.com/bins/1bzskc
   */
  private getJSON() {

    const data = Config.Menu;
    //console.log('fff', data);
    var AppsAllowed: any = [];

    /*     if (data != undefined || data != null) {
    
          for (let index = 0; index < data.length; index++) {
            const menu = data[index];
    
            if (menu.Operations) {
              for (let index = 0; index < menu.Operations.length; index++) {
                const operation = menu.Operations[index];
    
                console.log('Type: ONS, Operation: ', operation);
    
                if (this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', operation)) {
                  AppsAllowed.push(menu);
                  console.log(operation);
                }
    
              }
            }
    
          }
        } */

    // data.forEach(element => {
    //   if(element.Operations)
    //   {

    //   }
    // });

    this.data = data;
    // this.data = AppsAllowed;
    this.quadradoSrv.updateAppsAllowed(this.data.length);
    console.log('Data:', this.data.length);
    console.log('Data Apps Allowed:', AppsAllowed);
    if (this.data.length === 1) {
      this.goToApp(this.data[0], false)
    }
    // this.cleanData(this.quadradoSrv.qtdItems);
    return data;

  }

  /**
   * 
   * loadData
   * 
   * Recupera Resultado do metódo getJson().
   * 
   */
  loadData() {
    return this.getJSON();
  }

  cleanData(registros: number) {
    var d = [];
    for (var i = 0; i <= registros - 1; i++) {
      d.push(this.data[i])
    }
    this.data = d;
    if (this.data.length === 1) {
      console.log(this.data);
      this.goToApp(this.data[0], false)
    }
  }

  /**
   * 
   * sair
   * 
   * Exibe opções de sair do aplicativo.
   * 
   */
  sair() {
    this.loginSrv.showLogoutOptions("Catraca", false, null);
  }



  /* mostra mensagem no badge do square especifico */

  showBadge(squareIndex: number, text: string) {
    console.log('atualizando badge', squareIndex, text);
    let badgeElement = document.getElementById('badge-' + squareIndex);
    if (badgeElement) {
      badgeElement.innerHTML = text;
      badgeElement.parentElement.hidden = (!parseInt(text) || text == '');
    }

  }

}
