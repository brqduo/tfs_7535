import { Component } from "@angular/core";

@Component({
  selector: 'atualizar',
  template: `
    <ion-content style="background-image: url('./assets/imgs/splash.png'); background-repeat:no-repeat; background-size:auto 100vh; background-position: center 0%;">

      <ion-spinner icon="ios-small" style="stroke: #ff0000;
        fill: #8b0000;
        position: fixed;
        z-index: 999;
        height: 5em;
        width: em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;">
      </ion-spinner>
    </ion-content>
    `,

})
export class SimulaSplashPage {

}    