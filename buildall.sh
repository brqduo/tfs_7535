if [ $2 == "forcereset" ]; then
    echo "Vai limpar o diretório, perdendo qualquer alteração"
    rm -rf ./node_modules
    rm -rf ./package-lock.json
    npm install
    ionic cordova platform remove ios
    ionic cordova platform add ios
    cd ./automatic-build
    fastlane resetepull
    cd ..
    
else
    echo "Use o parâmentro forcereset para limpar o diretorio, perdendo qualquer alteração local."
fi
node automatic-build/incrementVersion.js
sh ./androidBetaBuild.sh $1
sh ./iosBetaBuild.sh $1
git commit -m 'Build number changed' config.xml ./platforms/ios/MeuONS.xcodeproj/project.pbxproj ./platforms/ios/MeuONS/MeuONS-Info.plist
git push
