npm install --unsafe-perm=true
ionic cordova platform remove android
ionic cordova platform add android@7.1.4
cross-env ENV=$1 ionic cordova build android --release
if [ "$?" != "0" ]; then
echo "Erro ao compilar. Não prosseguirá com a distribuição" 1>&2
cd ..
exit 1
fi

Jarsigner -storepass $certpassword -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore  ./androidkey/CertificadoONS.jks ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk key

mkdir -p ./automatic-build/build/

rm ./automatic-build/build/*.apk


~/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ./automatic-build/build/MeuONS.apk

cd ./automatic-build
# if [ "$*" == "prd" ]; then
#     fastlane betaandroidgoogle
# fi
fastlane betaandroidfabric
cd ..


