webpackJsonp([5],{

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ui_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_analytics__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UtilsService = /** @class */ (function () {
    function UtilsService(loadingCtrl, uiServices, platform, analyticSrv) {
        this.loadingCtrl = loadingCtrl;
        this.uiServices = uiServices;
        this.platform = platform;
        this.analyticSrv = analyticSrv;
        this._ultimaVerificacaoEntrada = null;
    }
    Object.defineProperty(UtilsService.prototype, "ultimaVerificacaoEntrada", {
        get: function () {
            return this._ultimaVerificacaoEntrada;
        },
        set: function (value) {
            this._ultimaVerificacaoEntrada = value;
        },
        enumerable: true,
        configurable: true
    });
    UtilsService.prototype.launchApp = function (pageName, url, packageName, color, urlLoja) {
        var _this = this;
        if (this.platform.is('cordova')) {
            var loading_1 = this.loadingCtrl.create({
                spinner: "crescent",
                content: "Carregando..."
            });
            loading_1.present();
            window["plugins"].launcher.launch({ packageName: packageName, uri: url }, function (data) {
                loading_1.dismiss();
                console.log('app launched' + JSON.stringify(data));
                if (!data.isLaunched) {
                    window.open(urlLoja, '_system');
                    _this.analyticSrv.sendCustomEvent(pageName + ' - Abrindo loja para instalar app');
                }
                else {
                    _this.analyticSrv.sendCustomEvent(pageName + ' - App acionado');
                }
            }, function (errMsg) {
                loading_1.dismiss();
                console.log('Erro no acionamento, abrindo loja para instalar app. Erro:' + errMsg);
                window.open(urlLoja, '_system');
                _this.analyticSrv.sendNonFatalCrash(pageName + ' - Erro no acionamento, abrindo loja para instalar app. Erro: ', errMsg);
            });
            //}
        }
        else {
            this.uiServices.showAlertMessage('Erro', 'Não é possível acionar um aplicativo no browser', color);
        }
    };
    UtilsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__ui_service__["a" /* UIServices */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_analytics__["a" /* AnalyticsService */]])
    ], UtilsService);
    return UtilsService;
}());

//# sourceMappingURL=utils.service.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MsgModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MsgModalPage = /** @class */ (function () {
    function MsgModalPage(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.title = params.get('title');
        this.message = params.get('message');
        this.color = params.get('color');
        console.log(this);
    }
    MsgModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MsgModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-msg-modal',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/msg-modal/msg-modal.html"*/'<ion-content style="padding: 0">\n  <div class="titlebox" [ngStyle]="{\'background-color\': color}">\n    <span class="title">{{title}}</span>\n    <ion-buttons end>\n\n      <button ion-button icon-only class="bar-button" (click)="dismiss()">\n        <ion-icon name="close" [ngStyle]="{\'color\': color}"></ion-icon>\n      </button>\n\n    </ion-buttons>\n  </div>\n  <p class="content" [ngStyle]="{\'color\': color}">{{message}}</p>\n</ion-content>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/msg-modal/msg-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MsgModalPage);
    return MsgModalPage;
}());

//# sourceMappingURL=msg-modal.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MsgAlertPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MsgAlertPage = /** @class */ (function () {
    function MsgAlertPage(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.title = params.get('title');
        this.message = params.get('message');
        this.color = params.get('color');
    }
    MsgAlertPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MsgAlertPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-msg-alert',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/msg-alert/msg-alert.html"*/'<ion-content style="padding: 0">\n  <div class="titlebox" [ngStyle]="{\'background-color\': color}">\n    <div class="title">\n    <span class="text">{{title}}</span>\n    </div>\n    <ion-buttons end>\n\n      <button ion-button icon-only class="bar-button" (click)="dismiss()">\n        <ion-icon name="close" [ngStyle]="{\'color\': color}"></ion-icon>\n      </button>\n\n    </ion-buttons>\n  </div>\n  <p class="content" [ngStyle]="{\'color\': color}">{{message}}</p>\n</ion-content>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/msg-alert/msg-alert.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MsgAlertPage);
    return MsgAlertPage;
}());

//# sourceMappingURL=msg-alert.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
var Config = {
    production: false,
    loginServiceConfig: {
        enviroment: "DSV",
        urlRemoteConfig: "https://appapitst.ons.org.br/api.loja/"
    },
    API_URL_PEOPLE: 'https://dsvapis.ons.org.br',
    API_URL_CATRACA: 'https://appapitst.ons.org.br/API.Mobile.MeuONS',
    Menu: [
        {
            "ordem": 1,
            "nome": "GestorNet",
            "flipcard": "",
            "cor": "#4B80BD",
            "icone": "./assets/icon/icon-gestornet.png",
            "destino": "gestornet",
            "AppName": "GestorNetPage",
            "Operations": ["Menu:GestorNet"],
            "DeepLink": "https://onsgestornet.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.gestornet&ibi=br.org.ons.enterprise.gestornet&ius=gestornet&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&efr=1"
        }, {
            "ordem": 2,
            "nome": "Contatos",
            "flipcard": "",
            "cor": "#486018",
            "icone": "./assets/icon/icon-contatos.png",
            "destino": "contatos",
            "AppName": "ContatosPage",
            "Operations": ["Menu:Contatos"],
            "DeepLink": "https://onscontatos.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.contatos&ibi=br.org.ons.enterprise.contatos&ius=contatos&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&efr=1"
        }, {
            "ordem": 3,
            "nome": "Recebimento Físico",
            "flipcard": "",
            "cor": "#F76C00",
            "destino": "Recebimento_Fisico",
            "AppName": "RecebimentoPage",
            "sigla": "RF",
            "Operations": ["Menu:RecebimentoFísico"]
        }, {
            "ordem": 4,
            "nome": "Aprovação de Férias",
            "flipcard": "",
            "cor": "#D10429",
            "destino": "aprovacao-ferias",
            "AppName": "AprovacaoFeriasPage",
            "sigla": "AF",
            "Operations": ["Menu:AprovaçãoFérias"]
        }, {
            "ordem": 5,
            "nome": "Catraca Online",
            "flipcard": "Catraca",
            "cor": "#4f8ad8",
            "destino": "Catraca_Online",
            "AppName": "CatracaPage",
            "sigla": "CO",
            "Operations": ["Menu:CatracaOnline"]
        }, {
            "ordem": 6,
            "nome": "Aprovação de MO",
            "flipcard": "",
            "cor": "green",
            "destino": "aprovacao-mo",
            "AppName": "AprovacaoMOPage",
            "sigla": "MO",
            "Operations": ["Menu:AprovaçãoMO"]
        }, {
            "ordem": 7,
            "nome": "Portal de TI",
            "flipcard": "",
            "cor": "#a6aab2",
            "destino": "",
            "AppName": "",
            "sigla": "PTI",
            "Operations": ["Menu:PortalTI"],
            "externalUrl": "https://onsbrtst.sharepoint.com/sites/portalti"
        }
    ]
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApproveListPermissions; });
var ApproveListPermissions = /** @class */ (function () {
    function ApproveListPermissions() {
    }
    return ApproveListPermissions;
}());

//# sourceMappingURL=approve-listTypes.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalheAprovacaomoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__aprovacao_mo__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_ui_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aprovacao_ferias_model_helper_cor__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DetalheAprovacaomoPage = /** @class */ (function () {
    function DetalheAprovacaomoPage(navCtrl, navParams, uiServices, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.uiServices = uiServices;
        this.platform = platform;
        this.callbackVoltar = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.item = {}; // conteudo do cartao
        this.listTotal = {};
        this.listOrigem = {};
        this.listAplicacao = {};
        this.showBackButton = navParams.get("showBackButton");
        this.permiteAprovar = navParams.get("permiteAprovar");
        this.permiteReprovar = navParams.get("permiteReprovar");
        this.platform.registerBackButtonAction(function () {
            _this.voltar();
        });
        // cor da borda do cartao
        this.item = navParams.get('item');
        this.item.origensVerba.forEach(function (element) {
            element.index = 0;
        });
        this.item.index = 0;
        this.borderColorCard = __WEBPACK_IMPORTED_MODULE_4__aprovacao_ferias_model_helper_cor__["a" /* HelperCor */]['COR_' + this.item.status.toUpperCase()];
    }
    DetalheAprovacaomoPage.prototype.origemAnterior = function () {
        this.item.index--;
        this.slides1.slidePrev(200, false);
        this.item.origensVerba[this.item.index].index = 0;
    };
    DetalheAprovacaomoPage.prototype.origemProximo = function () {
        this.item.index++;
        this.slides1.slideNext(200, false);
        this.item.origensVerba[this.item.index].index = 0;
    };
    DetalheAprovacaomoPage.prototype.onSlideChanged = function () {
        this.item.index = this.slides1.getActiveIndex();
        this.item.origensVerba[this.item.index].index = 0;
        this.slides2.resize();
    };
    DetalheAprovacaomoPage.prototype.origemAnterior2 = function () {
        this.item.origensVerba[this.item.index].index--;
        this.slides2.slidePrev(200, false);
    };
    DetalheAprovacaomoPage.prototype.origemProximo2 = function () {
        this.item.origensVerba[this.item.index].index++;
        this.slides2.slideNext(200, false);
    };
    DetalheAprovacaomoPage.prototype.onSlideChanged2 = function () {
        this.item.origensVerba[this.item.index].index = this.slides2.getActiveIndex();
    };
    /**
     *
     * Clicou no botao voltar do header
     *
     */
    DetalheAprovacaomoPage.prototype.voltar = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__aprovacao_mo__["a" /* AprovacaoMOPage */], { showBackButton: true });
    };
    DetalheAprovacaomoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetalheAprovacaomoPage');
        this.carregaListaOrigem();
        this.carregaListaAplicacao();
    };
    DetalheAprovacaomoPage.prototype.getColor = function (status) {
        var result = '#868686';
        switch (status) {
            case 'aprovado':
                result = '#455E2B';
                break;
            case 'reprovado':
                result = '#D10429';
                break;
        }
        return result;
    };
    DetalheAprovacaomoPage.prototype.carregaListaOrigem = function () {
        if (this.listTotal && this.listTotal.imoveis && this.listTotal.imoveis.length > 0) {
            this.listOrigem = [];
            this.listOrigem.origem = '01';
            this.listOrigem.ua = '501';
            this.listOrigem.pdpi = '44411';
            this.listOrigem.etapa = '00';
            this.listOrigem.ngtg = '9111';
            this.listOrigem.imoveis = [];
            var total = this.listTotal.imoveis.length;
            var inicio = this.pageOrigem.skip;
            var fim = (inicio + this.pageOrigem.take) > total ? total : (inicio + this.pageOrigem.take);
            for (var index = inicio; index < fim; index++) {
                this.listOrigem.imoveis.push(this.listTotal.imoveis[index]);
            }
        }
    };
    DetalheAprovacaomoPage.prototype.carregaListaAplicacao = function () {
        if (this.listTotal && this.listTotal.imoveis && this.listTotal.imoveis.length > 0) {
            this.listAplicacao = [];
            this.listAplicacao.origem = '01';
            this.listAplicacao.ua = '501';
            this.listAplicacao.pdpi = '44411';
            this.listAplicacao.etapa = '00';
            this.listAplicacao.ngtg = '9111';
            this.listAplicacao.imoveis = [];
            var total = this.listTotal.imoveis.length;
            var inicio = this.pageAplicacao.skip;
            var fim = (inicio + this.pageAplicacao.take) > total ? total : (inicio + this.pageAplicacao.take);
            for (var index = inicio; index < fim; index++) {
                this.listAplicacao.imoveis.push(this.listTotal.imoveis[index]);
            }
        }
    };
    DetalheAprovacaomoPage.prototype.onApproveReprove = function (item, approved) {
        var _this = this;
        item.Approve = approved;
        if (!approved) {
            this.uiServices.showInputMessage("Justificativa da Reprovação", "", __WEBPACK_IMPORTED_MODULE_4__aprovacao_ferias_model_helper_cor__["a" /* HelperCor */].COR_REPROVADO)
                .subscribe(function (a) {
                item.observacao = a;
                _this.aprovarReprovar(item, approved);
            });
        }
        else {
            this.aprovarReprovar(item, approved);
        }
    };
    DetalheAprovacaomoPage.prototype.aprovarReprovar = function (item, aprovado) {
        //item.status = aprovado ? StatusType.Aprovado : StatusType.Reprovado;
        var callback = this.navParams.get('pageClosed');
        if (typeof callback == 'function')
            callback(this.item);
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slider1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Slides */])
    ], DetalheAprovacaomoPage.prototype, "slides1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slider2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Slides */])
    ], DetalheAprovacaomoPage.prototype, "slides2", void 0);
    DetalheAprovacaomoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-detalhe-aprovacaomo',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/pages/detalhe-aprovacaomo/detalhe-aprovacaomo.html"*/'<!-- inicio do cabecalho padrao -->\n\n<!-- titulo e tab bar -->\n<ion-header>\n    <ion-toolbar color="ons-green">\n        <ion-navbar [hidden]="showSearch" hideBackButton>\n            <ion-buttons *ngIf="showBackButton" left>\n                <button ion-button icon-only color="primary_20" (click)="voltar()">\n                    <ion-icon name="ios-arrow-back"></ion-icon>\n                </button>\n            </ion-buttons>\n            <ion-buttons end hidden>\n                <button ion-button icon-only color="primary_20" (click)="sair()">\n                    <ion-icon name="md-open"></ion-icon>\n                </button>\n            </ion-buttons>\n            <ion-title text-center>ID {{item.id}}</ion-title>\n        </ion-navbar>\n    </ion-toolbar>\n</ion-header>\n\n<!-- fim do header padrao -->\n\n<ion-content>\n\n    <celula [templateCelula]="templateCelula" [templateHeader]="templateHeader" [item]="item" [color]="getColor(item.status)"></celula>\n\n    <ons-card  [color]="borderColorCard">\n        <ng-container [ngTemplateOutlet]="templateSlider1" [ngTemplateOutletContext]="{data: item}"></ng-container>\n    </ons-card>\n    \n    <ons-card  [color]="borderColorCard">\n        <ng-container [ngTemplateOutlet]="templateSlider2" [ngTemplateOutletContext]="{data: item}"></ng-container>\n    </ons-card>\n\n</ion-content>\n\n<ion-footer>\n    <ion-row>\n        <ion-col class="botoesAprovarReprovar">\n            <button ion-button  color="ons-green"  (click)="onApproveReprove(item, true)" *ngIf="permiteAprovar">\n                <ion-icon name="checkmark"></ion-icon>\n                <span [style.marginLeft.px]="10">Aprovar</span>\n            </button>\n        </ion-col>\n        <ion-col class="botoesAprovarReprovar">\n            <button ion-button  color="ons-red" (click)="onApproveReprove(item, false)" *ngIf="permiteReprovar">\n                <ion-icon name="close"></ion-icon>\n                <span [style.marginLeft.px]="10">Reprovar</span>\n            </button>\n        </ion-col>\n    </ion-row>\n</ion-footer>\n\n<!-- templates -->\n\n<ng-template #templateHeader let-item="data">\n\n    <p>Status: {{item.status}}</p>\n\n</ng-template>\n\n<ng-template #templateCelula let-item="data">  \n    <ion-row>\n        <ion-col>\n            <label class="t2" text-wrap [style.color]="getColor(item.status)">{{item.title}}</label>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col>\n            <label class="low" text-wrap>Início: </label>\n            <p><label class="t2" text-wrap [style.color]="getColor(item.status)">{{item.data_inicio}}</label></p>\n        </ion-col>\n        <ion-col>\n            <label class="low" text-wrap>Término: </label>\n            <p><label class="t2" text-wrap [style.color]="getColor(item.status)">{{item.data_fim}}</label></p>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col>\n            <label class="t2" text-wrap>{{item.dateDescription}}</label>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col>\n            <label class="low" text-wrap>Tipo: </label>\n            <p><label class="t3" [style.color]="getColor(0)" text-wrap>{{item.tipo}}</label></p>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col>\n            <label class="t2" text-wrap>{{item.description2}}</label>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col>\n            <label class="t2" text-wrap>{{item.description}}</label>\n        </ion-col>\n    </ion-row>\n    <ion-row *ngIf="item.status != 0">\n        <ion-col>\n            <label class="t2" text-wrap [style.color]="getColor(item.status)">{{item.description3}}</label>\n        </ion-col>\n    </ion-row>\n\n</ng-template>\n\n<ng-template #templateSlider1 let-item="data">\n\n    <div class="cartao-slider">\n\n        <div class="titulo-slider" *ngIf="item">\n            <div class="texto">Origem da Verba</div>\n            <div class="botao push">\n                <button *ngIf="item.index > 0" ion-button icon-only small clear (click)="origemAnterior()">\n                    <ion-icon name="ios-arrow-back" color="white"></ion-icon>\n                </button>\n            </div>\n            <div class="texto">{{item.index + 1}} de {{item.origensVerba.length}}</div>\n            <div class="botao">\n                <button *ngIf="item.index +1 < item.origensVerba.length" ion-button icon-only small clear (click)="origemProximo()">\n                    <ion-icon name="ios-arrow-forward"></ion-icon>\n                </button>\n            </div>\n        </div>\n\n        <ion-slides #slider1 (ionSlideDidChange)="onSlideChanged($event)">\n            <ion-slide *ngFor="let origem of item.origensVerba;">\n\n                <item-mo [itemOrigem]="origem"></item-mo>\n            </ion-slide>\n        </ion-slides>\n\n    </div>\n</ng-template>\n\n<ng-template #templateSlider2 let-item="data">\n\n    <div class="cartao-slider">\n\n        <div class="titulo-slider" *ngIf="item">\n            <div class="texto">Aplicação da Verba</div>\n            <div class="botao push">\n                <button *ngIf="item.origensVerba[item.index].index > 0" ion-button icon-only small clear (click)="origemAnterior2()">\n                    <ion-icon name="ios-arrow-back" color="white"></ion-icon>\n                </button>\n            </div>\n            <div class="texto">{{item.origensVerba[item.index].index + 1}} de\n                {{item.origensVerba[item.index].aplicacoes.length}}</div>\n            <div class="botao">\n                <button *ngIf="item.origensVerba[item.index].index +1 < item.origensVerba[item.index].aplicacoes.length"\n                    ion-button icon-only small clear (click)="origemProximo2()">\n                    <ion-icon name="ios-arrow-forward"></ion-icon>\n                </button>\n            </div>\n        </div>\n\n        <ion-slides #slider2 (ionSlideDidChange)="onSlideChanged2($event)">\n            <ion-slide *ngFor="let aplicacao of item.origensVerba[item.index].aplicacoes;">\n\n                <item-mo [itemOrigem]="aplicacao"></item-mo>\n            </ion-slide>\n        </ion-slides>\n\n    </div>\n</ng-template>\n    '/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/pages/detalhe-aprovacaomo/detalhe-aprovacaomo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__services_ui_service__["a" /* UIServices */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]])
    ], DetalheAprovacaomoPage);
    return DetalheAprovacaomoPage;
}());

//# sourceMappingURL=detalhe-aprovacaomo.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../Applications/Catraca_Online/src/pages/catraca/catraca.module": [
		736,
		2
	],
	"../Applications/Recebimento_Fisico/pages/recebimento/recebimento.module": [
		737,
		1
	],
	"../Applications/aprovacao-ferias/pages/aprovacao-ferias/aprovacao-ferias.module": [
		733,
		0
	],
	"../Applications/aprovacao-mo/pages/aprovacao-mo.module": [
		734,
		4
	],
	"../Applications/aprovacao-mo/pages/detalhe-aprovacaomo/detalhe-aprovacaomo.module": [
		735,
		3
	],
	"../components/msg-alert/msg-alert.module": [
		335
	],
	"../components/msg-input/msg-input.module": [
		336
	],
	"../components/msg-modal/msg-modal.module": [
		337
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 227;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Parameters; });
var Parameters = {
    quadradoPerfeito: [
        { qp: 2, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 4, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 6, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 9, classes: ['w-33', 'w-100', 'w-50'] },
        { qp: 12, classes: ['w-33', 'w-100', 'w-50'] },
        { qp: 15, classes: ['w-33', 'w-50', 'w-50'] }
    ],
    mensagemGenerica: {
        ENTRANDO: 'Entrando...',
        SEM_APPS_PERMITDOS: 'Você não tem nenhum App autorizado para uso.',
        SAIR_CONFIRMAR_COM_VOLTAR: 'Você pode Voltar, Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.',
        SAIR_CONFIRMAR: 'Você pode Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.'
    },
    Error: {
        FINGER_ID_UNSUPORTED: 4400,
    }
};
//# sourceMappingURL=parameters.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MsgConfirmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MsgConfirmPage = /** @class */ (function () {
    function MsgConfirmPage(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.title = params.get("title");
        this.message = params.get("message");
        this.color = params.get("color");
        console.log(this);
    }
    MsgConfirmPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        console.log('força focus');
        setTimeout(function () {
            _this.inputToFocus.setFocus();
        }, 200);
    };
    MsgConfirmPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MsgConfirmPage.prototype.confirm = function () {
        if (!this.message || this.message.length === 0) {
            this.viewCtrl.dismiss({ message: "" });
        }
        else {
            this.viewCtrl.dismiss({ message: this.message });
        }
        console.log('Confirmar');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('msgInputBox'),
        __metadata("design:type", Object)
    ], MsgConfirmPage.prototype, "inputToFocus", void 0);
    MsgConfirmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-msg-input",template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/msg-input/msg-input.html"*/'<ion-content style="padding: 0">\n  <div class="titlebox" [ngStyle]="{\'background-color\': color}">\n    <span class="title">{{title}}</span>\n    <ion-buttons end>\n      <button ion-button icon-only class="bar-button" (click)="dismiss()">\n        <ion-icon name="close" [ngStyle]="{\'color\': color}"></ion-icon>\n      </button>\n    </ion-buttons>\n  </div>  \n  \n    <ion-row class="message">\n        <ion-col col-11>            \n            <ion-textarea #msgInputBox [(ngModel)]="message" rows="5"></ion-textarea>                      \n        </ion-col>\n        <ion-col col-1>\n          <ion-row>\n              <ion-col>\n                  <button ion-button icon-only outline item-right (click)="confirm()"  [outline]="true" [clear]="true" float-right>\n                      <ion-icon name="checkmark" color="ons-gray"></ion-icon>\n                   </button>\n              </ion-col>  \n          </ion-row>\n          <ion-row>\n              <ion-col>\n                  <button ion-button icon-only outline item-right (click)="dismiss()" [outline]="true" [clear]="true" float-right>\n                      <ion-icon name="close" color="ons-gray"></ion-icon>\n                    </button>\n              </ion-col>  \n          </ion-row>\n        </ion-col>\n    </ion-row>     \n\n</ion-content>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/msg-input/msg-input.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MsgConfirmPage);
    return MsgConfirmPage;
}());

//# sourceMappingURL=msg-input.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApproveList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__approve_listTypes__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApproveList = /** @class */ (function () {
    function ApproveList() {
        this.permissions = new __WEBPACK_IMPORTED_MODULE_1__approve_listTypes__["a" /* ApproveListPermissions */]();
        this.mainColor = 'black';
        this.approveReproveAction = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.expanded = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.canceled = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ApproveList.prototype.ngOnInit = function () {
        this.intervalHourGlass = 250; // intervalo (ms) entre atualizacoes na tela
        this.tempoHourGlass = this.actionDelay; // tempo em segundos de exibicao antes de efetivar operacao
        this.maxHourGlass = (1000 / this.intervalHourGlass) * this.tempoHourGlass; // qtd de intervalos
    };
    ApproveList.prototype.ngOnDestroy = function () {
        var _this = this;
        if (this.items)
            this.items.forEach(function (element) {
                if (element && element.inAction) {
                    _this.cancel(element);
                }
            });
    };
    /* expande/contrai o componente */
    ApproveList.prototype.expandItem = function (item) {
        var _this = this;
        this.items.map(function (listItem) {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                _this.expanded.emit({ "item": item, "expanded": item.expanded });
            }
            else {
                listItem.expanded = false;
            }
        });
    };
    /* reformata contadores na troca das listas */
    ApproveList.prototype.ngOnChanges = function (changes) {
        if (changes["items"] && changes["items"].currentValue != changes["items"].previousValue) {
            this.items = changes["items"].currentValue;
            this.configureItensStyle(this.items);
        }
    };
    ApproveList.prototype.configureItensStyle = function (value) {
        var _this = this;
        value.forEach(function (item) {
            // CardBlock 
            item.show = false;
            item.backColorCard = null;
            item.LetterColor = null;
            item.showText = null;
            // SVG countDown
            if (_this.maxHourGlass > 1)
                item.max = _this.maxHourGlass;
            else
                item.max = 40;
            item.radius = 40;
            item.color = "#ffffff";
            item.background = "transparent";
            item.stroke = 5;
            item.clockwise = false;
            item.animation = "linearEase";
            item.interval = null;
            if (!item.mainColor)
                item.mainColor = _this.mainColor;
        });
        this.itemExpandHeight = value;
    };
    /* formata contador para aprovar/reprovar */
    ApproveList.prototype.onApproveReprove = function (item, approved) {
        var _this = this;
        if (approved) {
            this.runApprove(item);
        }
        else {
            if (this.beforeReprove) {
                this.beforeReprove(item).subscribe(function (resp) {
                    item = resp;
                    _this.runReprove(item);
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                this.runReprove(item);
            }
        }
    };
    ApproveList.prototype.activeExternalApproveReprove = function (item) {
        var _this = this;
        //precisa aguardar pela tela atualizar para aplicar o efeito do relogio
        setTimeout(function () {
            _this.items.forEach(function (element) {
                if (element.id == item.id) {
                    for (var k in item)
                        element[k] = item[k];
                    if (item.Approve) {
                        _this.runApprove(element);
                    }
                    else {
                        _this.runReprove(element);
                    }
                }
            });
        }, 1000);
    };
    ApproveList.prototype.runApprove = function (item) {
        item.expanded = false;
        item.inAction = true;
        item.busy = false;
        item.showText = "Aprovação";
        //item.LetterColor = { color: "#8ca58c" }; /* para que serve? */
        item.backColorCard = { 'background-color': '#486018' }; // antigo '#60823d'
        item.acao = "aprovar";
        this.holdCard(item, true);
    };
    ApproveList.prototype.runReprove = function (item) {
        item.expanded = false;
        item.inAction = true;
        item.busy = false;
        item.showText = "Reprovação";
        //item.LetterColor = { 'color': '#aba9a9' } /* para que serve ? */
        item.backColorCard = { 'background-color': '#d10429' }; // antigo '#e88195'
        item.acao = "reprovar";
        this.holdCard(item, false);
    };
    /* mostra contador aguardando tempo determinado */
    ApproveList.prototype.holdCard = function (itemSelected, approved) {
        var _this = this;
        itemSelected.current = this.maxHourGlass;
        console.log("Aqui em Ativar>", itemSelected);
        itemSelected.show = true;
        this.expandList.closeSlidingItems();
        itemSelected.interval = setInterval(function () {
            console.log("Em interval");
            itemSelected.current = itemSelected.current - 1;
            if (itemSelected.current <= 0) {
                console.log("Em interval 2");
                /* obs: reformata contador ~ configurarEstiloItens() mantendo showText e interval */
                _this.expandList.sliding = true;
                itemSelected.show = false;
                itemSelected.backColorCard = null;
                itemSelected.LetterColor = null;
                itemSelected.current = _this.maxHourGlass;
                itemSelected.max = _this.maxHourGlass;
                /* redundante... */
                //itemSelected.color = "#ffffff";
                //itemSelected.background = "transparent";
                //itemSelected.stroke = 5;
                //itemSelected.clockwise = false;
                //itemSelected.animation = "linearEase";
                clearInterval(itemSelected.interval);
                _this.approveReproveAction.emit({ "item": itemSelected, "approved": approved });
                itemSelected.inAction = false;
            }
        }, this.intervalHourGlass);
    };
    /* cancela espera e esconde contador */
    ApproveList.prototype.cancel = function (itemSelected) {
        // this.expandList.sliding = true;
        console.log("Aqui em cancelar>", itemSelected);
        clearInterval(itemSelected.interval);
        /* obs: reformata contador ~ configurarEstiloItens() */
        itemSelected.show = false;
        itemSelected.inAction = false;
        itemSelected.backColorCard = null;
        itemSelected.LetterColor = null;
        itemSelected.current = this.maxHourGlass;
        itemSelected.max = this.maxHourGlass;
        this.canceled.emit(itemSelected);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], ApproveList.prototype, "items", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "templateHeader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "templateExpanded", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "itemExpandHeight", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "actionDelay", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "permissions", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveList.prototype, "mainColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ApproveList.prototype, "approveReproveAction", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ApproveList.prototype, "expanded", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ApproveList.prototype, "canceled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Function)
    ], ApproveList.prototype, "beforeReprove", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* List */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* List */])
    ], ApproveList.prototype, "expandList", void 0);
    ApproveList = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'approve-list',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/approve-list/approve-list.html"*/'<ion-list>\n\n  <ion-item-sliding #expandList *ngFor="let item of items">\n\n\n\n    <ion-item-options *ngIf="permissions?.canApprove && !item.inAction && !item.busy" side="left">\n\n      <button ion-button color="ons-green" (click)="onApproveReprove(item, true)">\n\n        <p>Aprovar</p>\n\n        <ion-icon name="checkmark"></ion-icon>\n\n      </button>\n\n    </ion-item-options>\n\n    <ion-item-options *ngIf="permissions?.canReprove && !item.inAction && !item.busy" side="right">\n\n      <button ion-button color="ons-red" (click)="onApproveReprove(item, false)">\n\n        <p>Reprovar</p>\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-item-options>\n\n\n\n    <ion-item>\n\n      <ons-card [color]="item.mainColor">\n\n\n\n        <ng-container *ngIf="templateHeader" [ngTemplateOutlet]="templateHeader" [ngTemplateOutletContext]="{data: item}"></ng-container>\n\n\n\n        <ion-icon *ngIf="templateExpanded" class="iconExpand" [ngStyle]="{\'color\': item.mainColor}" [hidden]="!item.expanded"\n\n          (click)="expandItem(item)" name="arrow-dropup"></ion-icon>\n\n        <ion-icon *ngIf="templateExpanded" class="iconExpand" [ngStyle]="{\'color\': item.mainColor}" [hidden]="item.expanded"\n\n          (click)="expandItem(item)" name="arrow-dropdown"></ion-icon>\n\n\n\n        <expandable [expandHeight]="itemExpandHeight" [expanded]="item.expanded">\n\n          <ng-container *ngIf="templateExpanded" [ngTemplateOutlet]="templateExpanded" [ngTemplateOutletContext]="{data: item}"></ng-container>\n\n\n\n          <ion-row class="bottomRow">\n\n            <ion-col col-6>\n\n              <button ion-button color="ons-red" *ngIf="permissions?.canReprove" (click)="onApproveReprove(item, false)">\n\n                <ion-icon name="close"></ion-icon>\n\n                Reprovar\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n              <button ion-button color="ons-green" *ngIf="permissions?.canApprove" (click)="onApproveReprove(item, true)">\n\n                <ion-icon name="checkmark"></ion-icon>\n\n                Aprovar\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </expandable>\n\n\n\n        <div class="hourGlassCard" [hidden]="!item.show" (click)="cancel(item)">\n\n          <div class="hourGlass">\n\n            <round-progress [current]="item.current" [color]="item.color" [radius]="item.radius" [stroke]="item.stroke"\n\n              [background]="item.background" [clockwise]="item.clockwise" [max]="item.max">\n\n            </round-progress>\n\n            <ion-icon name="close" class="hourGlassIcon"></ion-icon>\n\n          </div>\n\n          <h4 class="hourGlassHeader">Desfazer {{item.showText}}</h4>\n\n          <div class="hourGlassBackground" [ngStyle]="item.backColorCard"></div>\n\n        </div>\n\n        <div class="busyCard" [hidden]="!item.busy">\n\n          <div class="center-vertical-horizontal">\n\n            <ion-spinner></ion-spinner>\n\n          </div>\n\n        </div>\n\n      </ons-card>\n\n      <!-- </ion-card> -->\n\n    </ion-item>\n\n  </ion-item-sliding>\n\n\n\n  <h1 class="message-no-permission" *ngIf="!permissions?.canList">Sem permissão de acesso</h1>\n\n</ion-list>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/approve-list/approve-list.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ApproveList);
    return ApproveList;
}());

//# sourceMappingURL=approve-list.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgAlertPageModule", function() { return MsgAlertPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__msg_alert__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MsgAlertPageModule = /** @class */ (function () {
    function MsgAlertPageModule() {
    }
    MsgAlertPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__msg_alert__["a" /* MsgAlertPage */]),
                __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__["a" /* SharedModule */]
            ],
            exports: []
        })
    ], MsgAlertPageModule);
    return MsgAlertPageModule;
}());

//# sourceMappingURL=msg-alert.module.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgConfirmPageModule", function() { return MsgConfirmPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__msg_input__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MsgConfirmPageModule = /** @class */ (function () {
    function MsgConfirmPageModule() {
    }
    MsgConfirmPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__msg_input__["a" /* MsgConfirmPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__msg_input__["a" /* MsgConfirmPage */]),
                __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__msg_input__["a" /* MsgConfirmPage */]
            ],
        })
    ], MsgConfirmPageModule);
    return MsgConfirmPageModule;
}());

//# sourceMappingURL=msg-input.module.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgModalPageModule", function() { return MsgModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__msg_modal__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MsgModalPageModule = /** @class */ (function () {
    function MsgModalPageModule() {
    }
    MsgModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__msg_modal__["a" /* MsgModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__app_shared_shared_module__["a" /* SharedModule */]
            ],
            exports: []
        })
    ], MsgModalPageModule);
    return MsgModalPageModule;
}());

//# sourceMappingURL=msg-modal.module.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApproveItemPermissions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AproveItem; });
var ApproveItemPermissions = /** @class */ (function () {
    /**
     *
     */
    function ApproveItemPermissions(canApprove, canReprove, canList) {
        this.canApprove = canApprove;
        this.canReprove = canReprove;
        this.canList = canList;
    }
    return ApproveItemPermissions;
}());

var AproveItem = /** @class */ (function () {
    function AproveItem(actionDelay) {
        this.tempoHourGlass = 10; // tempo em segundos de exibicao antes de efetivar operacao
        this.showProgress = false;
        this.backColorCard = null;
        this.max = 40;
        this.radius = 40;
        this.color = "#ffffff";
        this.background = "transparent";
        this.stroke = 5;
        this.clockwise = false;
        this.showText = "";
        this.intervalHourGlass = 250;
        if (actionDelay > 0)
            this.tempoHourGlass = actionDelay;
        this.maxHourGlass = (1000 / this.intervalHourGlass) * this.tempoHourGlass;
    }
    AproveItem.prototype.approve = function () {
        this.showText = "Aprovação";
        this.backColorCard = { "background-color": "#486018" }; // antigo '#60823d'
    };
    AproveItem.prototype.disapprove = function () {
        this.showText = "Reprovação";
        this.backColorCard = { "background-color": "#d10429" };
    };
    AproveItem.prototype.show = function () {
        this.current = this.maxHourGlass;
        this.showProgress = true;
    };
    AproveItem.prototype.hide = function () {
        this.showProgress = false;
        this.backColorCard = null;
        this.current = this.maxHourGlass;
        this.max = this.maxHourGlass;
    };
    return AproveItem;
}());

//# sourceMappingURL=approve-itemtypes.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApproveItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__approve_itemtypes__ = __webpack_require__(382);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ApproveItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ApproveItemComponent = /** @class */ (function () {
    function ApproveItemComponent() {
        this.approveReproveAction = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.canceled = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ApproveItemComponent.prototype.ngOnInit = function () {
        this.approveItem = new __WEBPACK_IMPORTED_MODULE_2__approve_itemtypes__["b" /* AproveItem */](this.actionDelay);
        this.busy = false;
        this.selected = false;
    };
    ApproveItemComponent.prototype.ngOnDestroy = function () {
        if (this.selected) {
            this.cancel();
        }
    };
    ApproveItemComponent.prototype.isBusy = function () {
        return this.busy;
    };
    ApproveItemComponent.prototype.showBusy = function () {
        this.busy = true;
    };
    ApproveItemComponent.prototype.hideBusy = function () {
        this.busy = false;
    };
    ApproveItemComponent.prototype.onApproveReprove = function (approved) {
        this.selected = true;
        if (approved) {
            this.approveItem.approve();
            this.holdCard(true);
        }
        else {
            this.approveItem.disapprove();
            this.holdCard(false);
        }
    };
    ApproveItemComponent.prototype.holdCard = function (approved) {
        var _this = this;
        this.approveItem.show();
        this.idItemSliding.close();
        this.approveItem.interval = setInterval(function () {
            console.log("Em interval");
            _this.approveItem.current = _this.approveItem.current - 1;
            if (_this.approveItem.current <= 0) {
                console.log("Em interval 2");
                _this.approveItem.hide();
                clearInterval(_this.approveItem.interval);
                _this.approveReproveAction.emit({
                    item: _this.item,
                    approved: approved
                });
                _this.selected = false;
            }
        }, this.approveItem.intervalHourGlass);
    };
    ApproveItemComponent.prototype.cancel = function () {
        console.log("Aqui em cancelar>", this.item);
        clearInterval(this.approveItem.interval);
        this.approveItem.hide();
        this.canceled.emit(this.item);
        this.selected = false;
        this.busy = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveItemComponent.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveItemComponent.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveItemComponent.prototype, "actionDelay", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveItemComponent.prototype, "permissions", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ApproveItemComponent.prototype, "approveReproveAction", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ApproveItemComponent.prototype, "canceled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ItemSliding */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ItemSliding */])
    ], ApproveItemComponent.prototype, "idItemSliding", void 0);
    ApproveItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "approve-item",template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/approve-item/approve-item.html"*/'<!-- Generated template for the ApproveItemComponent component -->\n<ion-item-sliding #idItemSliding>\n  <ion-item> \n     \n    <!-- <ng-container *ngIf="template" [ngTemplateOutlet]="template" [ngTemplateOutletContext]="{data: item}"></ng-container> -->\n    <ng-content></ng-content>\n\n    <div class="hourGlassCard" [hidden]="!approveItem.showProgress" (click)="cancel()">\n        <div class="hourGlass">\n        <round-progress\n            [current]="approveItem.current"\n            [color]="approveItem.color"\n            [radius]="approveItem.radius"\n            [stroke]="approveItem.stroke"\n            [background]="approveItem.background"\n            [clockwise]="approveItem.clockwise"\n            [max]="approveItem.max"\n        >\n        </round-progress>\n        <ion-icon name="close" class="hourGlassIcon"></ion-icon>\n        </div>\n        <p class="hourGlassHeader">Desfazer {{ approveItem.showText }}</p>\n        <div class="hourGlassBackground" [ngStyle]="approveItem.backColorCard"></div>\n    </div>\n\n      <div class="busyCard" [hidden]="!isBusy()">\n        <div class="center-vertical-horizontal">\n          <ion-spinner></ion-spinner>\n        </div>\n      </div> \n\n  </ion-item>\n\n  <ion-item-options *ngIf="permissions?.canApprove && !selected && !busy" side="left">\n    <button ion-button icon-only color="ons-green" (click)="onApproveReprove(true)">\n      <ion-icon name="checkmark"></ion-icon>\n      <p>Aprovar</p>\n    </button>\n  </ion-item-options>\n\n  <ion-item-options *ngIf="permissions?.canApprove && !selected && !busy" side="right">\n    <button ion-button icon-only color="ons-red" (click)="onApproveReprove(false)">\n      <ion-icon name="close"></ion-icon>\n      <p>Reprovar</p>\n    </button>\n  </ion-item-options>      \n\n</ion-item-sliding>\n'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/approve-item/approve-item.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ApproveItemComponent);
    return ApproveItemComponent;
}());

//# sourceMappingURL=approve-item.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelperCor; });
var HelperCor = /** @class */ (function () {
    function HelperCor() {
    }
    HelperCor.COR_APROVADO = "#455E2B"; //verde
    HelperCor.COR_REPROVADO = '#D10429'; //Vermelho
    HelperCor.COR_PENDENTE_APROVACAO = "#868686"; //cinza
    HelperCor.COR_ALERT = "#D10429"; //
    return HelperCor;
}());

//# sourceMappingURL=helper-cor.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpandableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ExpandableComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ExpandableComponent = /** @class */ (function () {
    function ExpandableComponent(renderer) {
        this.renderer = renderer;
        console.log('Hello ExpandableComponent Component');
        this.text = 'Hello World';
    }
    ExpandableComponent.prototype.ngAfterViewInit = function () {
        this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + 'px');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('expandWrapper', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", Object)
    ], ExpandableComponent.prototype, "expandWrapper", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('expanded'),
        __metadata("design:type", Object)
    ], ExpandableComponent.prototype, "expanded", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('expandHeight'),
        __metadata("design:type", Object)
    ], ExpandableComponent.prototype, "expandHeight", void 0);
    ExpandableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'expandable',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/expandable/expandable.html"*/'<!-- Generated template for the ExpandableComponent component -->\n<div #expandWrapper class=\'expand-wrapper\' [class.collapsed]="!expanded">\n  <ng-content></ng-content>\n</div>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/expandable/expandable.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
    ], ExpandableComponent);
    return ExpandableComponent;
}());

//# sourceMappingURL=expandable.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AprovacaoMOPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_quadraros_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_analytics__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_ui_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_observable_interval__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_observable_interval__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detalhe_aprovacaomo_detalhe_aprovacaomo__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_approve_list_approve_listTypes__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__model_helper_cor__ = __webpack_require__(708);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_approve_list_approve_list__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/* pagina inicial */

/* tipo de permissao: pode aprovar, pode reprovar, pode listar */

/* todo: avertar definicoes de cor */



var AprovacaoMOPage = /** @class */ (function () {
    function AprovacaoMOPage(navCtrl, navParams, platform, loginSrv, quadradosSrv, analyticSrv, http, uiServices) {
        /**
         *
         * trata navegacao sair/voltar
         *
         */
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.loginSrv = loginSrv;
        this.quadradosSrv = quadradosSrv;
        this.analyticSrv = analyticSrv;
        this.http = http;
        this.uiServices = uiServices;
        this.callbackVoltar = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.isLoading = false; // esconde spinner
        this.showSearch = false; // esconde barra de pesquisa
        this.segment = 'pendentes'; // inicializa com uma das abas ativa
        this.item = {}; // conteudo do cartao
        // indica se as listas estao vazias
        this.semItensPendentes = false;
        this.semItensTodos = false;
        // ver mode enum
        this.ApproveItemStatus = {
            Aprovado: 'aprovado',
            Reprovado: 'reprovado',
            Pendente: 'pendente'
        };
        this.showBackButton = navParams.get("showBackButton");
        this.platform.registerBackButtonAction(function () {
            _this.voltar();
        });
        this.callbackVoltar.subscribe(function (u) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */]);
        });
        // cor da borda do cartao
        this.item = navParams.get('item');
        console.log('==> aprovacao.ts', this.item);
        //        this.borderColorCard = HelperCor['COR_' + this.item.status.toUpperCase()];
    }
    AprovacaoMOPage.prototype.ngOnInit = function () {
        this.obemObservacaoCallback = this.solicitaObservacao.bind(this);
    };
    /**
     *
     * mostra/esconde a barra de pesquisa
     *
     */
    AprovacaoMOPage.prototype.searchShow = function (ev) {
        if (ev === true) {
            this.showSearch = true;
        }
        else {
            this.showSearch = false;
        }
    };
    /**
     *
     * Clicou em uma tab
     *
     */
    AprovacaoMOPage.prototype.mudarTab = function () {
        this.analyticSrv.sendCustomEvent("Aprovação de MO - Selecionou: " + this.segment);
        console.log('mudarTab:', this.segment);
        console.log('pend:', !this.semItensPendentes, this.itensPendentes);
        console.log('outr:', !this.semItensTodos, this.itensTodos);
    };
    /**
     *
     * Clicou no botao voltar do header
     *
     */
    AprovacaoMOPage.prototype.voltar = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */]);
        this.analyticSrv.sendCustomEvent("Aprovação de MO - Voltar para home");
    };
    /**
     *
     * ignora botao do voltar do hardware do android
     *
     */
    AprovacaoMOPage.prototype.ionViewDidLeave = function () {
        this.platform.registerBackButtonAction(function () {
            // ignore
        });
    };
    /**
     *
     * Exibe opções de sair do aplicativo.
     *
     */
    AprovacaoMOPage.prototype.sair = function () {
        this.loginSrv.showLogoutOptions("MO", false, this.callbackVoltar);
    };
    /**
     *
     * Exibe mensagem padrao de erro
     *
     */
    AprovacaoMOPage.prototype.showAlert = function (titulo, mensagem) {
        this.uiServices.showAlertMessage(titulo, mensagem, __WEBPACK_IMPORTED_MODULE_12__model_helper_cor__["a" /* HelperCor */].COR_ALERT);
    };
    /**
     *
     * Verifica permissoes antes de entrar na pagina
     *
     */
    AprovacaoMOPage.prototype.ionViewWillEnter = function () {
        // const canApprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Aprovar");
        // const canReprove = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:Reprovar");
        // const canList = this.securitySrv.isOperationAllowedByScope("ONS", "ONS", "AprovaçãoFérias:ListarFérias");
        this.permissoesPendentes = new __WEBPACK_IMPORTED_MODULE_11__components_approve_list_approve_listTypes__["a" /* ApproveListPermissions */]();
        //todo: trocar por permissoes reais na versao integrada
        this.permissoesPendentes.canList = true;
        this.permissoesPendentes.canApprove = true;
        this.permissoesPendentes.canReprove = true;
        this.permissoesTodos = new __WEBPACK_IMPORTED_MODULE_11__components_approve_list_approve_listTypes__["a" /* ApproveListPermissions */]();
        this.permissoesTodos.canList = true;
        if (this.permissoesPendentes.canList) {
            this.carregarDados(true);
        }
        this.analyticSrv.sendCustomEvent("Entrou no Aprovacao de MO");
    };
    /**
     *
     * carrega dados do repositorio
     *
     */
    AprovacaoMOPage.prototype.carregarDados = function (bloqueiaTela, refresher) {
        var _this = this;
        if (refresher) {
            this.analyticSrv.sendCustomEvent("Atualizou tela via Refresher na tela " + this.segment);
        }
        if (bloqueiaTela && !refresher) {
            this.isLoading = true;
        }
        this.itensPendentes = [];
        this.itensTodos = [];
        this.semItensPendentes = false;
        this.semItensTodos = false;
        // busca dados do repositorio
        var listaPendente = [];
        this.http.get("./assets/data/mo.json").subscribe(function (dados) {
            console.log('==> get:', dados);
            console.log('==> get', dados, _this.ApproveItemStatus);
            // carrega dados nas listas
            for (var i = 0; i < dados.length; i++) {
                var dadoOriginal = dados[i];
                if (dadoOriginal.status == _this.ApproveItemStatus.Pendente) {
                    listaPendente.push(dadoOriginal);
                }
                else {
                    _this.itensTodos.push(dadoOriginal);
                }
            }
            _this.itensPendentes = listaPendente;
            _this.semItensPendentes = (_this.itensPendentes.length == 0);
            _this.semItensTodos = (_this.itensTodos.length == 0);
            _this.isLoading = false;
            if (refresher) {
                refresher.complete();
            }
            _this.analyticSrv.sendCustomEvent("Carregou dados Aprovação de MO", {
                Total: dados.length,
                Pendentes: _this.itensPendentes.length,
                Todos: _this.itensTodos.length
            });
        }, function (error) {
            _this.isLoading = false;
            _this.analyticSrv.sendNonFatalCrash("Erro ao carregar dados em Aprovação de MO", error);
            if (error && error.message) {
                _this.showAlert("Erro", "Ocorreu um erro ao atualizar a lista.");
            }
            if (error && error.mensagem) {
                _this.showAlert("Erro", error.mensagem);
            }
            if (refresher)
                refresher.complete();
            console.log("resultado com erro em business:" + JSON.stringify(error));
        });
    };
    /**
     *
     * evento disparado, aprovacao ou reprovacao do item
     *
     */
    AprovacaoMOPage.prototype.onCancelAction = function (item) {
        console.log("==> cancelamento da acao para o item ", item);
    };
    /**
     *
     * evento disparado, cancelamento da acao
     *
     */
    AprovacaoMOPage.prototype.onConfirmAction = function (item, aprovado) {
        if (aprovado) {
            console.log("==> item Aprovado", item);
        }
        else {
            console.log("==> item Reprovado ", item);
        }
        // localiza item pelo id
        // let itemSelecionado = this.approveItens.filter(
        //     approve =>
        //         approve.id === item.id
        // )[0];
        // let itemSelecionado=item;
        // // simula alguma atualizacao de dados com delay // todo: testar efeito visual
        // if (itemSelecionado) {
        //     console.log("==> vai simular a atualizacao do item no repositorio");
        //     itemSelecionado.showBusy();
        //     setTimeout(() => {
        //         console.log("==> terminou de simular a atualização");
        //         itemSelecionado.hideBusy();
        //         if (aprovado) item.status = this.ApproveItemStatus.Aprovado;
        //         else item.status = this.ApproveItemStatus.Reprovado;
        //         item.permissoes.canApprove = false;
        //         item.permissoes.canReprove = false;
        //     }, 2000);
        // }
    };
    /**
     *
     * evento disparado: mostrar tela de detalhes
     *
     */
    AprovacaoMOPage.prototype.showDetails = function (item) {
        var _this = this;
        if (item) {
            // ao chamar a pagina de detalhe, cria um callback para ao voltar, caso tenha sido aprovado/reprovado,
            // acione o metodo de aprovacao/reprovacao do component para iniciar o timer
            console.log('DetalheAprovacaomoPage');
            var pageClosed = function (retData) {
                //console.log('this came back:', retData);
                _this.approveList.activeExternalApproveReprove(retData);
            };
            var permiteAprovar = false;
            var permiteReprovar = false;
            if (item.status == this.ApproveItemStatus.Pendente) {
                permiteAprovar = this.permissoesPendentes.canApprove;
                permiteReprovar = this.permissoesPendentes.canReprove;
            }
            else {
                permiteAprovar = this.permissoesTodos.canApprove;
                permiteReprovar = this.permissoesTodos.canReprove;
            }
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__detalhe_aprovacaomo_detalhe_aprovacaomo__["a" /* DetalheAprovacaomoPage */], { showBackButton: true, item: item, pageClosed: pageClosed, permiteAprovar: permiteAprovar, permiteReprovar: permiteReprovar });
        }
    };
    /* todo: ver

      / * formata e mostra observacao no rodape da pagina * /

  mostraObs(periodo: IPeriodo) {
    this.periodoSelecionado = periodo;

    let titulo = "Justificativa";
    let cor = HelperCor.COR_PENDENTE;
    let mensagem = periodo.justificativa;;

    if (periodo.status === StatusPeriodo.Aprovado) {
      titulo = "Justificativa da Aprovação";
      cor = HelperCor.COR_APROVADO;
    } else {
      if (periodo.status == StatusPeriodo.Reprovado) {
        titulo = "Justificativa da Reprovação";
        cor = HelperCor.COR_REPROVADO;
      }
    }

    if (!periodo.permissoes.canApprove && !periodo.permissoes.canApprove) {
      this.uiServices.showFooterMessage(titulo, mensagem, cor);
    } else {

      this.uiServices.showInputMessage(titulo, mensagem, cor)
        .subscribe(a => {
          this.periodoSelecionado.justificativa = a;
          console.log('Obs: ' + this.periodoSelecionado.justificativa);
        });
    }
  }


  */
    AprovacaoMOPage.prototype.expandido = function (item, expandido) {
        console.log("registar log ", item, " expandido: " + expandido);
    };
    AprovacaoMOPage.prototype.solicitaObservacao = function (item) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_13_rxjs__["Observable"].create(function (observer) {
            _this.uiServices.showInputMessage("Justificativa da Reprovação", "", __WEBPACK_IMPORTED_MODULE_12__model_helper_cor__["a" /* HelperCor */].COR_REPROVADO)
                .subscribe(function (a) {
                item.observacao = a;
                observer.next(item);
            }, function (err) {
                observer.error(err);
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('approveListPendentes'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_14__components_approve_list_approve_list__["a" /* ApproveList */])
    ], AprovacaoMOPage.prototype, "approveList", void 0);
    AprovacaoMOPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-aprovacao-mo',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/pages/aprovacao-mo.html"*/'<!-- cabecalho -->\n\n<ion-header id="header">\n    <ion-toolbar color="ons-green">\n    \n        <ion-navbar [hidden]="showSearch">\n    \n        <!-- botao voltar -->\n    \n        <ion-buttons *ngIf="showBackButton" left>\n            <button ion-button icon-only color="primary_20" (click)="voltar()">\n            <ion-icon name="arrow-back"></ion-icon>\n            </button>\n        </ion-buttons>\n    \n        <!-- botao finalizar -->\n    \n        <ion-buttons end>\n            <button ion-button icon-only color="primary_20" (click)="sair()">\n            <ion-icon name="log-out"></ion-icon>\n            </button>\n        </ion-buttons>\n    \n        <!-- titulo -->\n    \n        <ion-title text-center>Aprovação MO</ion-title>\n        \n        <!-- barra de pesquisa -->\n    \n        <ion-buttons end  *ngIf="false"> <!-- botao pesquisa suprimido --> \n            <button ion-button icon-only (click)="searchShow(true)">\n            <ion-icon name="search"></ion-icon>\n            </button>\n        </ion-buttons>\n    \n        </ion-navbar>\n    \n        <!-- barra de pesquisa -->\n    \n        <ion-searchbar [(ngModel)]="myInput" [showCancelButton]="shouldShowCancel" [hidden]="!showSearch">\n        </ion-searchbar>\n    \n        <!-- menu de abas -->\n    \n        <ion-segment (click)="mudarTab()" [(ngModel)]="segment">\n        <ion-segment-button value="pendentes">\n            <span class="headColor">Pendentes</span>\n        </ion-segment-button>\n        <ion-segment-button value="todos">\n            <span class="headColor">Todos</span>\n        </ion-segment-button>\n        </ion-segment>\n    \n    </ion-toolbar>\n</ion-header>\n\n      \n<!-- conteudo das listas -->\n<ion-content (click)="searchShow(false)">\n    <ion-refresher (ionRefresh)="carregarDados(true, $event)" *ngIf="!isLoading">\n        <ion-refresher-content> </ion-refresher-content>\n    </ion-refresher>\n    <div class="spinner-aguarde" *ngIf="isLoading">\n        <ion-spinner></ion-spinner>\n    </div>\n    <!--ion-list #approveItens-->\n    <div [ngSwitch]="segment" class="segment">\n\n        <ion-list *ngSwitchCase="\'pendentes\'">\n\n            <approve-list #approveListPendentes [items]="itensPendentes" [templateHeader]="templateCardItem"\n                [templateExpanded]="" [permissions]="permissoesPendentes" [itemExpandHeight]="480" (expanded)="expandido($event.item, $event.expanded)"\n                [actionDelay]="10" (approveReproveAction)="onConfirmAction($event.item, $event.approved)" (canceled)="onCancelAction($event.item)"\n                [beforeReprove]="obemObservacaoCallback" mainColor=\'#A6A6A6\'>\n            </approve-list>\n\n            <h1 class="mensagem-sem-dados" *ngIf="semItensPendentes">Nenhuma aprovação pendente</h1>\n\n        </ion-list>\n\n        <ion-list *ngSwitchCase="\'todos\'">\n\n            <approve-list [items]="itensTodos" [templateHeader]="templateCardItem" [templateExpanded]="" [permissions]="permissoesTodos"\n                [itemExpandHeight]="480" (expanded)="expandido($event.item, $event.expanded)" [actionDelay]="10"\n                mainColor=\'#A6A6A6\'>\n            </approve-list>\n\n            <h1 class="mensagem-sem-dados" *ngIf="semItensTodos">\n                Não existem aprovações\n            </h1>\n\n        </ion-list>\n\n    </div>\n    <!--/ion-list-->\n</ion-content>\n\n<!-- fim do header padrao -->\n\n\n<!-- itens aprovacao de mo -->\n\n<ng-template #templateCardItem let-item="data">\n\n    <div class="cartao-mo status-{{item.status}}" (click)="showDetails(item)">\n\n        <div class="titulo">\n            <p class="t5">ID {{item.id}}</p>\n        </div>\n        <ion-row>\n            <ion-col>\n                <label class="t3 status-{{item.status}}" text-wrap>{{item.codigo}}</label>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col width-50>\n                <label class="t4" text-wrap>Início: </label>\n                <label class="t3 status-{{item.status}}" text-wrap>{{item.data_inicio}}</label>\n            </ion-col>\n            <ion-col width-50>\n                <label class="t4" text-wrap>Término: </label>\n                <label class="t3 status-{{item.status}}" text-wrap>{{item.data_fim}}</label>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <label class="t4" text-wrap>{{item.description}}</label>\n            </ion-col>\n        </ion-row>\n\n    </div>\n\n</ng-template>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/pages/aprovacao-mo.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["f" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_3__services_quadraros_service__["a" /* QuadradoService */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_analytics__["a" /* AnalyticsService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__services_ui_service__["a" /* UIServices */]])
    ], AprovacaoMOPage);
    return AprovacaoMOPage;
}());

//# sourceMappingURL=aprovacao-mo.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemMoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ItemMoComponent = /** @class */ (function () {
    function ItemMoComponent() {
    }
    ItemMoComponent.prototype.ngOnInit = function () {
        console.log(this.itemOrigem);
    };
    ItemMoComponent.prototype.totalOrcado = function () {
        var total = 0;
        if (this.itemOrigem.valores) {
            this.itemOrigem.valores.forEach(function (d) {
                total += parseFloat(d.orcado);
            });
        }
        return total;
    };
    ItemMoComponent.prototype.totalAretirar = function () {
        var total = 0;
        if (this.itemOrigem.valores) {
            this.itemOrigem.valores.forEach(function (d) {
                total += parseFloat(d.aretirar);
            });
        }
        return total;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ItemMoComponent.prototype, "itemOrigem", void 0);
    ItemMoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "item-mo",template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/components/item-mo/item-mo.html"*/'<ion-row>\n    <table>\n        <td class="th1">\n            <label class="t4">Seq Origem: </label><br>\n            <label class="t3 cor-dark" text-wrap>{{itemOrigem.origem}}</label>\n        </td>\n        <td class="th2">\n            <label class="t4" text-wrap>U.A.: </label><br>\n            <label class="t3 cor-dark" text-wrap>{{itemOrigem.ua}}</label>\n        </td>\n        <td class="th3">\n            <label class="t4" text-wrap>PD/PI: </label><br>\n            <label class="t3 cor-dark" text-wrap>{{itemOrigem.pdpi}}</label>\n        </td>\n        <td class="th4">\n            <label class="t4" text-wrap>Etapa: </label><br>\n            <label class="t3 cor-dark" text-wrap>{{itemOrigem.etapa}}</label>\n        </td>\n        <td class="th5">\n            <label class="t4" text-wrap>NG/TG: </label><br>\n            <label class="t3 cor-dark" text-wrap>{{itemOrigem.ngtg}}</label>\n        </td>\n    </table>\n    <p class="t4 sep" text-wrap>Imóveis</p>\n</ion-row>\n\n<ion-list>\n    <ion-item>\n        <ion-row>\n            <table>\n                <tr>\n                    <td class="td1 t3"></td>\n                    <td class="td2 t3 cor-blue" text-wrap>Orçado</td>\n                    <td class="td3 t3 cor-red" text-wrap>A ser Retirado</td>\n                </tr>\n            </table>\n        </ion-row>\n    </ion-item>\n    <ion-item *ngFor="let valor of itemOrigem.valores" class="row-numbers">\n        <ion-row>\n            <table>\n                <tr>\n                    <td class="td1 t3 cor-light">{{valor.data}}</td>\n                    <td class="td2 t3 cor-blue" text-wrap>{{valor.orcado | number:\'1.2-2\'}}</td>\n                    <td class="td3 t3 cor-red" text-wrap>{{valor.aretirar | number:\'1.2-2\'}}</td>\n                </tr>\n            </table>\n        </ion-row>\n    </ion-item>\n    <ion-item>\n        <ion-row>\n            <table>\n                <tr>\n                    <td class="td1 t3 cor-light bold">Total</td>\n                    <td class="td2 t3 cor-blue bold" text-wrap>{{totalOrcado() | number:\'1.2-2\'}}</td>\n                    <td class="td3 t3 cor-red bold" text-wrap>{{totalAretirar() | number:\'1.2-2\'}}</td>\n                </tr>\n            </table>\n        </ion-row>\n    </ion-item>\n</ion-list>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/Applications/aprovacao-mo/components/item-mo/item-mo.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ItemMoComponent);
    return ItemMoComponent;
}());

//# sourceMappingURL=item-mo.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(396);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_push__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_quadraros_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_analytics__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_fingerprint_aio__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_file_opener__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_file_transfer__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_cardFlip_cardFlip__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_ui_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_msg_alert_msg_alert_module__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_utils_service__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_msg_modal_msg_modal_module__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_msg_input_msg_input_module__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_fabric__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_simula_splash_simula_splash__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_app_version__ = __webpack_require__(331);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











/** SERVICES */















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_17__components_cardFlip_cardFlip__["a" /* CardFlipComponent */],
                __WEBPACK_IMPORTED_MODULE_24__pages_simula_splash_simula_splash__["a" /* SimulaSplashPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["h" /* OnsPackage */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_19__components_msg_alert_msg_alert_module__["MsgAlertPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_19__components_msg_alert_msg_alert_module__["MsgAlertPageModule"],
                __WEBPACK_IMPORTED_MODULE_21__components_msg_modal_msg_modal_module__["MsgModalPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__components_msg_input_msg_input_module__["MsgConfirmPageModule"],
                __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {
                    mode: 'md',
                    backButtonIcon: 'ios-arrow-back',
                    backButtonText: ''
                }, {
                    links: [
                        { loadChildren: '../Applications/aprovacao-ferias/pages/aprovacao-ferias/aprovacao-ferias.module#AprovacaoFeriasPageModule', name: 'AprovacaoFeriasPage', segment: 'aprovacao-ferias', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../Applications/aprovacao-mo/pages/aprovacao-mo.module#AprovacaoMOPageModule', name: 'AprovacaoMOPage', segment: 'aprovacao-mo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../Applications/aprovacao-mo/pages/detalhe-aprovacaomo/detalhe-aprovacaomo.module#DetalheAprovacaomoPageModule', name: 'DetalheAprovacaomoPage', segment: 'detalhe-aprovacaomo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../Applications/Catraca_Online/src/pages/catraca/catraca.module#CatracaPageModule', name: 'CatracaPage', segment: 'catraca', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../Applications/Recebimento_Fisico/pages/recebimento/recebimento.module#RecebimentoPageModule', name: 'RecebimentoPage', segment: 'recebimento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/msg-alert/msg-alert.module#MsgAlertPageModule', name: 'MsgAlertPage', segment: 'msg-alert', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/msg-input/msg-input.module#MsgConfirmPageModule', name: 'MsgConfirmPage', segment: 'msg-input', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/msg-modal/msg-modal.module#MsgModalPageModule', name: 'MsgModalPage', segment: 'msg-modal', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["e" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_17__components_cardFlip_cardFlip__["a" /* CardFlipComponent */],
                __WEBPACK_IMPORTED_MODULE_24__pages_simula_splash_simula_splash__["a" /* SimulaSplashPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__services_quadraros_service__["a" /* QuadradoService */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["f" /* LoginService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["m" /* UtilService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_analytics__["a" /* AnalyticsService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["k" /* TokenService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["c" /* ErrorService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["j" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["d" /* ImageService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["i" /* SecurityService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["g" /* NetWorkService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["b" /* EnvironmentService */],
                __WEBPACK_IMPORTED_MODULE_12__ons_ons_mobile_login__["l" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_fabric__["a" /* Crashlytics */],
                __WEBPACK_IMPORTED_MODULE_18__services_ui_service__["a" /* UIServices */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_20__services_utils_service__["a" /* UtilsService */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_app_version__["a" /* AppVersion */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuadradoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_parameters__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/** CONFIG */

var QuadradoService = /** @class */ (function () {
    function QuadradoService() {
        this.qtdItems = 2;
        this.AppsAllowed = 0;
        // for (var x = 2; x < 16; x++) {
        //     for (var i = 1; i <= x; i++) {
        //         console.log(this.classeQuadradoPerfeito(i, x));
        //     }
        // }
    }
    Object.defineProperty(QuadradoService.prototype, "quadradosMockList", {
        get: function () {
            return [
                {
                    "ordem": 1,
                    "nome": "GestorNet",
                    "cor": "#4B80BD",
                    "icone": "./assets/icon/icon-gestornet.png",
                    "destino": "gestornet",
                    "AppName": "GestorNetPage",
                    "Operations": ["Menu:GestorNet"],
                    "DeepLink": "https://onsgestornet.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.gestornet&ibi=br.org.ons.enterprise.gestornet&ius=gestornet&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.gestornet&efr=1"
                }, {
                    "ordem": 2,
                    "nome": "Contatos",
                    "cor": "#486018",
                    "icone": "./assets/icon/icon-contatos.png",
                    "destino": "contatos",
                    "AppName": "ContatosPage",
                    "Operations": ["Menu:Contatos"],
                    "DeepLink": "https://onscontatos.page.link/?link=http%3A%2F%2Fmeuons.ons.org.br%2F&apn=br.org.ons.enterprise.contatos&ibi=br.org.ons.enterprise.contatos&ius=contatos&ifl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&&afl=https%3A%2F%2Fappapitst.ons.org.br%2FApps%2Fview%2Fbr.org.ons.enterprise.contatos%2F&efr=1"
                }, {
                    "ordem": 3,
                    "nome": "Recebimento Físico",
                    "cor": "#F76C00",
                    "destino": "Recebimento_Fisico",
                    "AppName": "RecebimentoPage",
                    "sigla": "RF",
                    "Operations": ["Menu:RecebimentoFísico"]
                }, {
                    "ordem": 4,
                    "nome": "Aprovação de Férias",
                    "cor": "#D10429",
                    "destino": "aprovacao-ferias",
                    "AppName": "AprovacaoFeriasPage",
                    "sigla": "AF",
                    "Operations": ["Menu:AprovaçãoFérias"]
                }, {
                    "ordem": 5,
                    "nome": "Catraca Online",
                    "cor": "#4f8ad8",
                    "destino": "Catraca_Online",
                    "AppName": "CatracaPage",
                    "sigla": "CO",
                    "Operations": ["Menu:CatracaOnline"]
                }, {
                    "ordem": 6,
                    "nome": "Aprovação de MO",
                    "cor": "green",
                    "destino": "aprovacao-mo",
                    "AppName": "AprovacaoMOPage",
                    "sigla": "MO",
                    "Operations": ["Menu:AprovaçãoMO"]
                }, {
                    "ordem": 7,
                    "nome": "Portal de TI",
                    "cor": "#a6aab2",
                    "destino": "",
                    "AppName": "",
                    "sigla": "PTI",
                    "Operations": ["Menu:PortalTI"],
                    "externalUrl": "https://onsbrtst.sharepoint.com/sites/portalti"
                }
            ];
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Retorna a distância do quadrado perfeito anterior mais próximo
     * @param num quantidade de icones na tela. Varia de 2 a 15
     */
    QuadradoService.prototype.distanciaQuadradoPerfeito = function (num) {
        var result = {};
        result.distancia = -1;
        for (var i = 0; i <= __WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito.length - 1; i++) {
            if (__WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito[i].qp === num) {
                result = __WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito[i];
                result.distancia = 0;
                // console.log('FINAL 1 RESULT', result);
            }
        }
        if (result.distancia === -1) {
            for (var x = 0; x <= __WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito.length - 1; x++) {
                if (__WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito[x].qp < num) {
                    result = __WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito[x];
                    result.distancia = (num - __WEBPACK_IMPORTED_MODULE_1__app_parameters__["a" /* Parameters */].quadradoPerfeito[x].qp);
                    //   console.log('FINAL 2 RESULT', result);
                    //this.Parameters.quadradoPerfeito[x - 1].qp
                }
            }
        }
        return result;
    };
    /**
     * Retorna a classe a ser utilizada no item
     * @param posicao Posição do Item a ser renderizado
     * @param totalPagina Total de itens da página que esta sendo renderizada
     */
    QuadradoService.prototype.classeQuadradoPerfeito = function (posicao, totalPagina) {
        if (this.rangeValid(this.qtdItems)) {
            if (this.LimitesQuadrados(posicao, totalPagina)) {
                var quadrado = this.distanciaQuadradoPerfeito(totalPagina);
                //    console.log('hell', quadrado);
                if (totalPagina === 2 || totalPagina === 0) {
                    //      console.log('Acho q pode ser');
                    return 'w-100';
                }
                else {
                    if (posicao > quadrado.distancia) {
                        //     console.log('posição maior', quadrado.distancia);
                        //    console.log(quadrado.classes[0]);
                        return quadrado.classes[0];
                    }
                    else {
                        //    console.log('posição menor');
                        return quadrado.classes[quadrado.distancia];
                    }
                }
            }
            else {
                return 'w-100';
                //return 'mensagem de erro posicao ' + posicao + '  -  ' + totalPagina
            }
        }
        else {
            console.log('Erro na qtd de quadrados');
        }
    };
    /**
     *
     * rangeValid
     *
     * Confere o tamanho do Quadrado e retorna bool conforme o críterio estabelecido
     *
     * @param tamanho Tamanho do Quadrado
     */
    QuadradoService.prototype.rangeValid = function (tamanho) {
        return ((tamanho >= 1) && (tamanho < 16));
    };
    /**
     *
     * LimitesQuadrados
     *
     * Retorna booleano conforme o atendimento dos critérios de limite de quadrados tela.
     *
     * @param posicao Posicao do Quadrado
     * @param totalPagina Total de Quadrados da Pagina
     */
    QuadradoService.prototype.LimitesQuadrados = function (posicao, totalPagina) {
        var retorno = (((posicao >= 1) && (posicao < 16)) && ((totalPagina > 1) && (totalPagina < 16)));
        // console.log(retorno);
        // if (retorno) {
        //     console.log(posicao, (posicao < totalPagina + 1));
        //     retorno = (posicao < totalPagina + 1)
        //     console.log(retorno);
        // }
        if (!retorno) {
            //   console.log('retorno false',posicao, totalPagina);
            //Gravar Log Error
        }
        return retorno;
    };
    /**
     *
     * updateAppsAllowed
     *
     * Atualiza o número de apps permitidos para o usuário.
     *
     * @param totalPermitidos Número de Apps permitidos para o usuário
     */
    QuadradoService.prototype.updateAppsAllowed = function (totalPermitidos) {
        this.AppsAllowed = totalPermitidos == undefined ? 0 : totalPermitidos;
    };
    QuadradoService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], QuadradoService);
    return QuadradoService;
}());

//# sourceMappingURL=quadraros.service.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UIServices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_msg_modal_msg_modal__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_msg_alert_msg_alert__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_msg_input_msg_input__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UIServices = /** @class */ (function () {
    function UIServices(modalCtrl, toast) {
        this.modalCtrl = modalCtrl;
        this.toast = toast;
        this.posMsg = ['bottom', 'top', 'middle'];
    }
    UIServices.prototype.showToastAlert = function (mensagem, delay, pos, titulo, btnClose) {
        delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
        pos = ((this.posMsg.indexOf(pos) === -1) ? 'bottom' : pos);
        btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
        mensagem = (((titulo == undefined) || (titulo == null)) ? mensagem : titulo + '\n\n' + mensagem);
        var toast = this.toast.create({ message: mensagem, duration: delay, position: pos, showCloseButton: btnClose, closeButtonText: 'Fechar' });
        toast.present();
    };
    UIServices.prototype.showFooterMessage = function (title, message, color) {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_msg_modal_msg_modal__["a" /* MsgModalPage */], {
            title: title,
            message: message,
            color: color
        }, { cssClass: 'msg-modal' })
            .present();
    };
    UIServices.prototype.showAlertMessage = function (title, message, color) {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_msg_alert_msg_alert__["a" /* MsgAlertPage */], {
            title: title,
            message: message,
            color: color
        }, { cssClass: 'msg-alert' })
            .present();
    };
    UIServices.prototype.showInputMessage = function (title, message, color) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_5_rxjs__["Observable"].create(function (observer) {
            var modalConfirm = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_msg_input_msg_input__["a" /* MsgConfirmPage */], {
                title: title,
                message: message,
                color: color,
            }, { cssClass: 'msg-input' });
            modalConfirm.onDidDismiss(function (data) {
                if (data) {
                    observer.next(data.message);
                }
                else
                    observer.next("");
            });
            modalConfirm.present();
        });
    };
    UIServices = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */]])
    ], UIServices);
    return UIServices;
}());

//# sourceMappingURL=ui.service.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnsCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the OnsCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var OnsCardComponent = /** @class */ (function () {
    function OnsCardComponent() {
        this.class = "cardBorderline";
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], OnsCardComponent.prototype, "color", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
        __metadata("design:type", String)
    ], OnsCardComponent.prototype, "class", void 0);
    OnsCardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'ons-card',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/ons-card/ons-card.html"*/'<!-- Generated template for the OnsCardComponent component -->\n\n\n<ion-card class="cardBorderline" [ngStyle]="{\'border-color\': color}">      \n    <ng-content></ng-content>\n</ion-card>\n'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/ons-card/ons-card.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], OnsCardComponent);
    return OnsCardComponent;
}());

//# sourceMappingURL=ons-card.js.map

/***/ }),

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentComponent = /** @class */ (function () {
    //segment = "Pendentes";
    function SegmentComponent() {
        this.itens = [];
        this.selected = "Pendentes";
        this.changeTab = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    SegmentComponent.prototype.onChangeTab = function () {
        this.changeTab.emit(this.selected);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], SegmentComponent.prototype, "itens", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], SegmentComponent.prototype, "selected", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SegmentComponent.prototype, "changeTab", void 0);
    SegmentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'segment',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/segment/segment.html"*/'<ion-segment (click)="onChangeTab()" [(ngModel)]="selected">\n    <ion-segment-button *ngFor="let item of itens" [value]="item.value">\n        <!-- <div text-wrap class="vertical-align">\n            {{item.title}}\n        </div> -->\n        <span class="headColor">{{item.title}}</span>\n    </ion-segment-button>\n</ion-segment>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/segment/segment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SegmentComponent);
    return SegmentComponent;
}());

//# sourceMappingURL=segment.js.map

/***/ }),

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ons_ons_mobile_login__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_quadraros_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ToolbarHeaderComponent = /** @class */ (function () {
    function ToolbarHeaderComponent(loginSrv, quadradoSrv, navParams) {
        this.loginSrv = loginSrv;
        this.quadradoSrv = quadradoSrv;
        this.navParams = navParams;
        this.showTab = true;
        this.changeTab = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.callbackVoltar = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.segItens = [{ title: "Pendentes", value: "Pendentes" }, { title: "Todos", value: "Todos" }];
        this.segment = 'Pendentes'; // inicializa com uma das abas ativa
        this.showBackButton = navParams.get('showBackButton');
    }
    ToolbarHeaderComponent.prototype.sair = function () {
        this.loginSrv.showLogoutOptions('Recebimento Físico', false, this.callbackVoltar);
    };
    ToolbarHeaderComponent.prototype.voltar = function () {
        this.callbackVoltar.emit(event);
    };
    ToolbarHeaderComponent.prototype.onChangeTab = function (event) {
        this.changeTab.emit(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ToolbarHeaderComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ToolbarHeaderComponent.prototype, "color", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], ToolbarHeaderComponent.prototype, "showTab", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ToolbarHeaderComponent.prototype, "changeTab", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ToolbarHeaderComponent.prototype, "callbackVoltar", void 0);
    ToolbarHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'toolbar-header',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/toolbar-header/toolbar-header.html"*/'<!-- cabecalho -->\n\n<ion-header id="header">\n    <ion-toolbar color="{{color}}">\n\n        <ion-navbar>\n\n            <!-- botao voltar -->\n\n            <ion-buttons *ngIf="showBackButton" left>\n                <button ion-button icon-only color="primary_20" (click)="voltar()">\n                    <ion-icon name="arrow-back"></ion-icon>\n                </button>\n            </ion-buttons>\n\n            <!-- botao finalizar -->\n\n            <ion-buttons end>\n                <button ion-button icon-only color="primary_20" (click)="sair()">\n                    <ion-icon name="log-out"></ion-icon>\n                </button>\n            </ion-buttons>\n\n            <!-- titulo -->\n\n            <ion-title text-center>{{title}}</ion-title>\n\n            <!-- barra de pesquisa -->\n\n            <ion-buttons end *ngIf="false">\n                <!-- botao pesquisa suprimido -->\n                <button ion-button icon-only (click)="searchShow(true)">\n                    <ion-icon name="search"></ion-icon>\n                </button>\n            </ion-buttons>\n           \n\n        </ion-navbar>   \n        \n        <!-- menu de abas -->\n        <segment *ngIf="showTab" [itens]="segItens" [selected]="\'Pendentes\'" (changeTab)="onChangeTab($event)"></segment>\n\n    </ion-toolbar>\n</ion-header>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/toolbar-header/toolbar-header.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ons_ons_mobile_login__["f" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_2__services_quadraros_service__["a" /* QuadradoService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */]])
    ], ToolbarHeaderComponent);
    return ToolbarHeaderComponent;
}());

//# sourceMappingURL=toolbar-header.js.map

/***/ }),

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelperCor; });
var HelperCor = /** @class */ (function () {
    function HelperCor() {
    }
    HelperCor.COR_APROVADO = "#455E2B"; //verde
    HelperCor.COR_REPROVADO = '#D10429'; //Vermelho
    HelperCor.COR_PENDENTE = "#868686"; //cinza
    HelperCor.COR_ALERT = "#D10429"; // janela de alerta
    return HelperCor;
}());

//# sourceMappingURL=helper-cor.js.map

/***/ }),

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CelulaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CelulaComponent = /** @class */ (function () {
    function CelulaComponent() {
        this.color = '#A6A6A6';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CelulaComponent.prototype, "templateCelula", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CelulaComponent.prototype, "templateHeader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CelulaComponent.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CelulaComponent.prototype, "page", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], CelulaComponent.prototype, "color", void 0);
    CelulaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'celula',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/celula/celula.html"*/'<div class="celula">\n    <ion-card ion-item class="box" [style.borderColor]="color">\n\n        <div class="card">\n\n            <div class="titulo" [style.backgroundColor]="color">\n                <ng-container *ngIf="templateHeader" [ngTemplateOutlet]="templateHeader" [ngTemplateOutletContext]="{data: item}"></ng-container>\n            </div>\n\n            <ng-container *ngIf="templateCelula" [ngTemplateOutlet]="templateCelula" [ngTemplateOutletContext]="{data: item}">\n\n            </ng-container>\n        </div>\n\n\n    </ion-card>\n</div>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/celula/celula.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], CelulaComponent);
    return CelulaComponent;
}());

//# sourceMappingURL=celula.js.map

/***/ }),

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwiperPaginationComponent; });
/* unused harmony export ColorValues */
/* unused harmony export DirectionValues */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SwiperPaginationComponent = /** @class */ (function () {
    function SwiperPaginationComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.changePage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.items = [];
    }
    SwiperPaginationComponent.prototype.onSlideChanged = function (event) {
        console.log('mudou de pagina');
    };
    SwiperPaginationComponent.prototype.ngOnInit = function () {
    };
    SwiperPaginationComponent.prototype.back = function () {
        this.changePage.emit(DirectionValues.DIRECTION_RIGHT);
    };
    SwiperPaginationComponent.prototype.next = function () {
        this.changePage.emit(DirectionValues.DIRECTION_LEFT);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SwiperPaginationComponent.prototype, "page", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SwiperPaginationComponent.prototype, "templateCelula", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SwiperPaginationComponent.prototype, "changePage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SwiperPaginationComponent.prototype, "items", void 0);
    SwiperPaginationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'swiper-pagination',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/swiper-pagination/swiper-pagination.html"*/'<celula [templateCelula]="templateCelula" [templateHeader]="templateHeader" [item]="page" [color]="\'#A6A6A6\'    "></celula>\n\n\n<ng-template #templateHeader let-item="data">\n\n    <ion-toolbar>\n\n        <ion-navbar *ngIf="item">\n\n            <ion-buttons *ngIf="item.skip > 1" left>\n                <button ion-button icon-only color="primary_20" (click)="back()">\n                    <ion-icon name="arrow-back"></ion-icon>\n                </button>\n            </ion-buttons>\n            \n            <ion-title text-left>{{item.title}}</ion-title>\n            <ion-title text-right>{{item.skip + 1}} de {{item.total}}</ion-title>\n\n            <ion-buttons *ngIf="item.skip + item.take < item.total" end>\n                <button ion-button icon-only color="primary_20" (click)="next()">\n                    <ion-icon name="arrow-forward"></ion-icon>\n                </button>\n            </ion-buttons>\n\n\n        </ion-navbar>\n\n\n    </ion-toolbar>\n\n</ng-template>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/swiper-pagination/swiper-pagination.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
    ], SwiperPaginationComponent);
    return SwiperPaginationComponent;
}());

var ColorValues;
(function (ColorValues) {
    ColorValues[ColorValues["blue"] = 1] = "blue";
    ColorValues[ColorValues["red"] = 2] = "red";
})(ColorValues || (ColorValues = {}));
var DirectionValues;
(function (DirectionValues) {
    DirectionValues[DirectionValues["DIRECTION_NONE"] = 1] = "DIRECTION_NONE";
    DirectionValues[DirectionValues["DIRECTION_LEFT"] = 2] = "DIRECTION_LEFT";
    DirectionValues[DirectionValues["DIRECTION_RIGHT"] = 4] = "DIRECTION_RIGHT";
    DirectionValues[DirectionValues["DIRECTION_UP"] = 8] = "DIRECTION_UP";
    DirectionValues[DirectionValues["DIRECTION_DOWN"] = 16] = "DIRECTION_DOWN";
    DirectionValues[DirectionValues["DIRECTION_HORIZONTAL"] = 6] = "DIRECTION_HORIZONTAL";
    DirectionValues[DirectionValues["DIRECTION_VERTICAL"] = 24] = "DIRECTION_VERTICAL";
    DirectionValues[DirectionValues["DIRECTION_ALL"] = 30] = "DIRECTION_ALL";
})(DirectionValues || (DirectionValues = {}));
//# sourceMappingURL=swiper-pagination.js.map

/***/ }),

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HourGlassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HourGlassComponent = /** @class */ (function () {
    function HourGlassComponent() {
        this.show = false;
        this.backColorCard = 'transparent';
        this.showText = '';
        this.current = 27; //current progress
        this.max = 40; //maximum value.
        this.canceled = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.stroke = 5; //espessura
        this.radius = 40; //raio 
        this.semicircle = false;
        this.rounded = false;
        this.responsive = false;
        this.clockwise = false; //sentido horário
        this.color = '#ffffff';
        this.background = 'transparent';
        this.duration = 800;
        this.animation = 'linearEase';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], HourGlassComponent.prototype, "show", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], HourGlassComponent.prototype, "backColorCard", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], HourGlassComponent.prototype, "showText", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], HourGlassComponent.prototype, "current", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], HourGlassComponent.prototype, "max", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], HourGlassComponent.prototype, "canceled", void 0);
    HourGlassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'hour-glass',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/hour-glass/hour-glass.html"*/'<div class="hourGlassCard" [hidden]="!show">\n    <div class="hourGlass">\n        <round-progress\n            [current]="current"\n            [max]="max"\n            [color]="color"\n            [background]="background"\n            [radius]="radius"\n            [stroke]="stroke"\n            [semicircle]="semicircle"\n            [rounded]="rounded"\n            [clockwise]="clockwise"\n            [responsive]="responsive"\n            [animation]="\'easeInOutQuart\'"\n            [duration]="duration">\n        </round-progress>\n        <ion-icon name="close" class="hourGlassIcon"></ion-icon>\n    </div>\n    <h4 class="hourGlassHeader">{{showText}}</h4>\n    <div class="hourGlassBackground" [style.backgroundColor]="backColorCard"></div>\n  </div>\n'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/hour-glass/hour-glass.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], HourGlassComponent);
    return HourGlassComponent;
}());

//# sourceMappingURL=hour-glass.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__enviroment_environment__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_utils_service__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/* ok */









/* new ... */
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, loginSrv, imageSrv, envSrv, alertCtrl, utilSrv, splashScreen, utilsSrv, push, app, alertController) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.loginSrv = loginSrv;
        this.imageSrv = imageSrv;
        this.envSrv = envSrv;
        this.alertCtrl = alertCtrl;
        this.utilSrv = utilSrv;
        this.splashScreen = splashScreen;
        this.utilsSrv = utilsSrv;
        this.push = push;
        this.app = app;
        this.alertController = alertController;
        platform.ready().then(function () {
            console.log('MeuONS.platform.ready');
            statusBar.styleDefault();
            imageSrv.SetDefault();
            loginSrv.setAplicationName('Mobile.MeuONS', 'MeuONS');
            envSrv.setConfig(__WEBPACK_IMPORTED_MODULE_6__enviroment_environment__["a" /* Config */].loginServiceConfig);
            console.log('loginServiceConfig:' + JSON.stringify(__WEBPACK_IMPORTED_MODULE_6__enviroment_environment__["a" /* Config */].loginServiceConfig));
            _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
            //forca entrar no splash simulado
            // this.app.getActiveNav().setRoot(SimulaSplashPage).then(() => {
            //   console.log('MeuONS.setroot');
            //   this.splashScreen.hide();
            //   this.loginSetup();
            // });
            //   this.loginSetup();
            //retorno ao aplicativo que estava em back
            platform.resume.subscribe(function () {
                console.log('MeuONS.platform.resume');
                /*         if (utilsSrv.ultimaVerificacaoEntrada) {
                          console.log('ultima verificacao versao: ' + utilsSrv.ultimaVerificacaoEntrada);
                          let tempoMinutosUltimaVerificacao: Number;
                          if (utilsSrv.ultimaVerificacaoEntrada) {
                            tempoMinutosUltimaVerificacao = new Date().getMinutes() - utilsSrv.ultimaVerificacaoEntrada.getMinutes();
                            console.log('Minutos da ultima verificacao: ' + tempoMinutosUltimaVerificacao);
                          }
                          if (utilsSrv.ultimaVerificacaoEntrada == null || tempoMinutosUltimaVerificacao >= 15) {
                            console.log('vai rodar verificacoes do resume');
                            utilsSrv.ultimaVerificacaoEntrada = new Date();
                            this.loginSrv.checkVersion();
                          }
                        } */
            });
            /*       this.loginSrv.onConectedChange
                    .subscribe((u: any) => {
                      if (u !== undefined) {
                        if (u.Connected) {
                          console.log('castle 2', u);
                          this.app.getActiveNav().setRoot(HomePage);
                        }
                      }
                    })
            
                  if (this.platform.is('cordova')) {
                    this.pushSetup();
                  } */
        });
    }
    MyApp.prototype.loginSetup = function (goToRoot) {
        var _this = this;
        if (goToRoot === void 0) { goToRoot = true; }
        this.loginSrv.checkLoginStatus().subscribe(function (canAutoLogin) {
            if (canAutoLogin) {
                console.log('permite login auto1');
                _this.loginSrv.loginAutomatico3().subscribe(function () {
                    _this.splashScreen.hide();
                    console.log('Logou com sucesso automaticamente');
                    if (goToRoot) {
                        _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
                    }
                }, function (err) {
                    console.log('Não inicializou automaticamente. ' + err);
                    _this.splashScreen.hide();
                    _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["e" /* LoginPage */]);
                });
                console.log('permite login auto2');
            }
            else {
                _this.splashScreen.hide();
                console.log('não permite login auto');
                _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["e" /* LoginPage */]);
            }
        });
    };
    MyApp.prototype.updateVersion = function (checkVersionReturn) {
        console.log('vai atualizar a versao');
        var url;
        if (this.platform.is('ios')) {
            url = checkVersionReturn.urlLojaIOS;
        }
        else {
            url = checkVersionReturn.urlLojaAndroid;
        }
        console.log('Update url: ' + url);
        window.open(url, '_system');
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // to check if we have permission
        this.push.hasPermission()
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
            }
            else {
                console.log('We do not have permission to send push notifications');
            }
        });
        var options = {
            android: {
                senderID: '596167917795'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            console.log("Notificação recebida e o app está aberto: " + JSON.stringify(notification));
            if (notification.additionalData.foreground) {
                var youralert = _this.alertCtrl.create({
                    title: 'New Push notification',
                    message: notification.message
                });
                youralert.present();
            }
        });
        pushObject.on('registration').subscribe(function (data) {
            console.log("device registered -> ", JSON.stringify(data));
            // this.saveToken(data.registrationId);
            var topic = "publicacoes";
            pushObject.subscribe(topic).then(function (res) {
                console.log("subscribed to topic: ", res);
            });
        });
        pushObject.on('error').subscribe(function (error) { return alert('Error with Push plugin' + error); });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["f" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["d" /* ImageService */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["b" /* EnvironmentService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["m" /* UtilService */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__services_utils_service__["a" /* UtilsService */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardFlipComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CardFlipComponent = /** @class */ (function () {
    function CardFlipComponent() {
    }
    CardFlipComponent.prototype.ngOnInit = function () { };
    CardFlipComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'cardflip',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/components/cardFlip/cardflip.html"*/'<!--   <div class="cardFlip">\n    <div class="cardFlipContainer" [class.is-flipped]="((item.flipcard !== \'\' ) && (flipcardOBJ.isFlipAble))">\n\n      <div class="cardFlip__face cardFront"\n        [class.transitionBoder]="((item.flipcard !== \'\' ) && (flipcardOBJ.transitionClass))">\n      <ng-content>\n      </div>\n      <div class="cardFlip__face cardBack"\n        [class.transitionBoder]="((item.flipcard !== \'\' ) && (flipcardOBJ.transitionClass))">\n        <div [innerHTML]="renderBack(item.flipcard)">\n\n        </div>\n      </div>\n    </div>\n  </div> -->\n<div class="cardShow">\n\n  <div class="BackHeader">Catraca Online</div>\n  <div class="lineDiv"> </div>\n  <div class="hourToShow">00:00</div>\n  <div class="lineDiv"> </div>\n  <div class="BackFooter">Banco de Compensação</div>\n</div>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/components/cardFlip/cardflip.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], CardFlipComponent);
    return CardFlipComponent;
}());

//# sourceMappingURL=cardFlip.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimulaSplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SimulaSplashPage = /** @class */ (function () {
    function SimulaSplashPage() {
    }
    SimulaSplashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'atualizar',
            template: "\n    <ion-content style=\"background-image: url('./assets/imgs/splash.png'); background-repeat:no-repeat; background-size:auto 100vh; background-position: center 0%;\">\n\n      <ion-spinner icon=\"ios-small\" style=\"stroke: #ff0000;\n        fill: #8b0000;\n        position: fixed;\n        z-index: 999;\n        height: 5em;\n        width: em;\n        overflow: show;\n        margin: auto;\n        top: 0;\n        left: 0;\n        bottom: 0;\n        right: 0;\">\n      </ion-spinner>\n    </ion-content>\n    ",
        })
    ], SimulaSplashPage);
    return SimulaSplashPage;
}());

//# sourceMappingURL=simula-splash.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_ons_card_ons_card__ = __webpack_require__(705);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_expandable_expandable__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_approve_list_approve_list__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_approve_item_approve_item__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_msg_alert_msg_alert__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_msg_modal_msg_modal__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_segment_segment__ = __webpack_require__(706);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_toolbar_header_toolbar_header__ = __webpack_require__(707);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Applications_aprovacao_mo_pages_detalhe_aprovacaomo_detalhe_aprovacaomo__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_celula_celula__ = __webpack_require__(709);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_swiper_pagination_swiper_pagination__ = __webpack_require__(710);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_hour_glass_hour_glass__ = __webpack_require__(711);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__Applications_aprovacao_mo_components_item_mo_item_mo__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule_1 = SharedModule;
    SharedModule.forRoot = function () {
        return {
            ngModule: SharedModule_1
        };
    };
    var SharedModule_1;
    SharedModule = SharedModule_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicModule */],
                __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__["RoundProgressModule"]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_11__Applications_aprovacao_mo_pages_detalhe_aprovacaomo_detalhe_aprovacaomo__["a" /* DetalheAprovacaomoPage */]
            ],
            providers: [],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__components_expandable_expandable__["a" /* ExpandableComponent */],
                __WEBPACK_IMPORTED_MODULE_3__components_approve_list_approve_list__["a" /* ApproveList */],
                __WEBPACK_IMPORTED_MODULE_7__components_msg_alert_msg_alert__["a" /* MsgAlertPage */],
                __WEBPACK_IMPORTED_MODULE_8__components_msg_modal_msg_modal__["a" /* MsgModalPage */],
                __WEBPACK_IMPORTED_MODULE_4__components_approve_item_approve_item__["a" /* ApproveItemComponent */],
                __WEBPACK_IMPORTED_MODULE_0__components_ons_card_ons_card__["a" /* OnsCardComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_segment_segment__["a" /* SegmentComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_toolbar_header_toolbar_header__["a" /* ToolbarHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_11__Applications_aprovacao_mo_pages_detalhe_aprovacaomo_detalhe_aprovacaomo__["a" /* DetalheAprovacaomoPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_celula_celula__["a" /* CelulaComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_swiper_pagination_swiper_pagination__["a" /* SwiperPaginationComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_hour_glass_hour_glass__["a" /* HourGlassComponent */],
                __WEBPACK_IMPORTED_MODULE_15__Applications_aprovacao_mo_components_item_mo_item_mo__["a" /* ItemMoComponent */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__components_expandable_expandable__["a" /* ExpandableComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_msg_alert_msg_alert__["a" /* MsgAlertPage */],
                __WEBPACK_IMPORTED_MODULE_3__components_approve_list_approve_list__["a" /* ApproveList */],
                __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__["RoundProgressModule"],
                __WEBPACK_IMPORTED_MODULE_8__components_msg_modal_msg_modal__["a" /* MsgModalPage */],
                __WEBPACK_IMPORTED_MODULE_4__components_approve_item_approve_item__["a" /* ApproveItemComponent */],
                __WEBPACK_IMPORTED_MODULE_0__components_ons_card_ons_card__["a" /* OnsCardComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_toolbar_header_toolbar_header__["a" /* ToolbarHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_15__Applications_aprovacao_mo_components_item_mo_item_mo__["a" /* ItemMoComponent */],
                __WEBPACK_IMPORTED_MODULE_11__Applications_aprovacao_mo_pages_detalhe_aprovacaomo_detalhe_aprovacaomo__["a" /* DetalheAprovacaomoPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_celula_celula__["a" /* CelulaComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_swiper_pagination_swiper_pagination__["a" /* SwiperPaginationComponent */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["CUSTOM_ELEMENTS_SCHEMA"],
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_quadraros_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__enviroment_environment__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_utils_service__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_parameters__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, quadradoSrv, loginSrv, securitySrv, utilsSrv, platform, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.quadradoSrv = quadradoSrv;
        this.loginSrv = loginSrv;
        this.securitySrv = securitySrv;
        this.utilsSrv = utilsSrv;
        this.platform = platform;
        this.http = http;
        this.posicao = 0;
        this.totalPagina = 2;
        this.classe = '';
        this.flip = false;
        this.maxHeight = 90;
        this.cslasseColuna = 'w-100';
        this.flipcardOBJ = {
            isFlipAble: false,
            transitionClass: false,
            whenToFlip: __WEBPACK_IMPORTED_MODULE_8_rxjs__["Observable"].interval(3000),
            timerFlipCardClass: __WEBPACK_IMPORTED_MODULE_8_rxjs__["Observable"].timer(1000)
        };
        this.Tela_Saida_Visivel = false;
        this.IsAllowed = false;
        this.NoAppAllowed = __WEBPACK_IMPORTED_MODULE_7__app_parameters__["a" /* Parameters */].mensagemGenerica.SEM_APPS_PERMITDOS;
        this.flipcardOBJ.whenToFlip.subscribe(function (res) {
            console.log('Ja rodou: ', res + ' vezes');
            _this.startFlipCard();
        });
    }
    HomePage.prototype.ionViewDidLoad = function () {
        // this.loadData();
        console.log('Carregou home');
        if (this.quadradoSrv.rangeValid(this.quadradoSrv.qtdItems)) {
            //this.loadData();
            this.data = __WEBPACK_IMPORTED_MODULE_5__enviroment_environment__["a" /* Config */].Menu;
            console.log('O q vem dentro de Data?', this.data);
        }
        else {
            alert('QTD de items invalida');
            this.navCtrl.pop();
        }
        //    console.log('Data DidLoad: ', this.data);
        /* somente para teste */
        /* exemplo chamando metodo para alterar o valor do badge */
        // setInterval(() => {
        //   this.showBadge(2, Math.floor(Math.random() * 4).toString() 
        //   );
        // },
        //   5000
        // );
    };
    HomePage.prototype.getUrlParameter = function (url, name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(url);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    ;
    /**
     *
     * goToApp
     *
     * Abre aplicativo
     *
     * @param item Objeto Menu
     */
    HomePage.prototype.goToApp = function (item, backButton) {
        //debugger;
        if (item) {
            if (item.DeepLink) {
                var packageName = void 0;
                var urlLoja = void 0;
                if (this.platform.is("ios")) {
                    packageName = this.getUrlParameter(item.DeepLink, 'ibi');
                    urlLoja = this.getUrlParameter(item.DeepLink, 'ifl');
                }
                else {
                    packageName = this.getUrlParameter(item.DeepLink, 'apn');
                    urlLoja = this.getUrlParameter(item.DeepLink, 'afl');
                }
                console.log('Acionando: ' + packageName);
                this.utilsSrv.launchApp(item.nome, item.DeepLink, packageName, item.cor, urlLoja);
            }
            else if (item.externalUrl) {
                window.open(item.externalUrl, '_system');
            }
            else if (backButton != undefined)
                this.navCtrl.setRoot(item.AppName, { showBackButton: backButton });
            else
                this.navCtrl.setRoot(item.AppName, { showBackButton: true });
        }
    };
    HomePage.prototype.startFlipCard = function () {
        var _this = this;
        this.flipcardOBJ.isFlipAble = !this.flipcardOBJ.isFlipAble;
        this.flipcardOBJ.transitionClass = !this.flipcardOBJ.transitionClass;
        this.flipcardOBJ.timerFlipCardClass.subscribe(function () {
            _this.flipcardOBJ.transitionClass = !_this.flipcardOBJ.transitionClass;
        });
    };
    HomePage.prototype.renderBack = function (item) {
        var retorno = '';
        switch (item) {
            case 'Catraca':
                retorno = '<cardflip></cardflip>';
                break;
        }
        return retorno;
    };
    /**
     *
     * getSomeClass
     *
     * Recupera Classe
     *
     * @param item Objeto Menu
     */
    HomePage.prototype.getSomeClass = function (item) {
        var ordem = item == undefined ? '' : item.ordem;
        return this.quadradoSrv.classeQuadradoPerfeito(ordem, this.data.length);
    };
    /**
     *
     * getMyStyles
     *
     * Recupera caracteristicas do Objeto Menu.
     *
     * @param item Objeto Menu
     */
    HomePage.prototype.getMyStyles = function (item) {
        if (item) {
            var tam = this.SetHeigth(this.data.length);
            var myStyles = {
                'background-color': item.cor,
                'height': tam
            };
            return myStyles;
        }
    };
    /**
     *
     * getBadgeStyles
     *
     * Recupera caracteristicas do Objeto Menu.
     *
     * @param item Objeto Menu
     */
    HomePage.prototype.getBadgeStyles = function (item) {
        if (item) {
            var styles = {
                'color': item.cor
            };
            return styles;
        }
    };
    /**
     *
     * SetHeigth
     *
     * Define tamanho da página conforme a quantidade de apps.
     *
     * @param tamanho Size
     *
     */
    HomePage.prototype.SetHeigth = function (tamanho) {
        var result = '';
        switch (tamanho) {
            case (tamanho >= 2 && tamanho <= 4): {
                result = '50';
                break;
            }
            case (tamanho === 7 || tamanho === 8 || tamanho === 10 || tamanho === 11 || tamanho === 12): {
                result = '22.5';
                break;
            }
            case (tamanho === 6 || tamanho === 5 || tamanho === 9): {
                result = '30';
                break;
            }
            case (tamanho >= 13): {
                result = '18';
                break;
            }
        }
        return result + 'vh';
    };
    /**
     * getClass
     *
     * Recupera Classes
     */
    HomePage.prototype.getClass = function () {
        console.log('passei pro getClass');
        this.classe = this.quadradoSrv.classeQuadradoPerfeito(this.posicao, this.totalPagina);
    };
    /**
     *
     * getJSON
     *
     * Recupera menu.json com os apps cadastrados.
     * DOIS APPS - https://api.myjson.com/bins/16lsro
     * UM APP (Recebimento) - https://api.myjson.com/bins/1gmrd8
     * VAZIO https://api.myjson.com/bins/1bzskc
     */
    HomePage.prototype.getJSON = function () {
        var data = __WEBPACK_IMPORTED_MODULE_5__enviroment_environment__["a" /* Config */].Menu;
        //console.log('fff', data);
        var AppsAllowed = [];
        /*     if (data != undefined || data != null) {
        
              for (let index = 0; index < data.length; index++) {
                const menu = data[index];
        
                if (menu.Operations) {
                  for (let index = 0; index < menu.Operations.length; index++) {
                    const operation = menu.Operations[index];
        
                    console.log('Type: ONS, Operation: ', operation);
        
                    if (this.securitySrv.isOperationAllowedByScope('ONS', 'ONS', operation)) {
                      AppsAllowed.push(menu);
                      console.log(operation);
                    }
        
                  }
                }
        
              }
            } */
        // data.forEach(element => {
        //   if(element.Operations)
        //   {
        //   }
        // });
        this.data = data;
        // this.data = AppsAllowed;
        this.quadradoSrv.updateAppsAllowed(this.data.length);
        console.log('Data:', this.data.length);
        console.log('Data Apps Allowed:', AppsAllowed);
        if (this.data.length === 1) {
            this.goToApp(this.data[0], false);
        }
        // this.cleanData(this.quadradoSrv.qtdItems);
        return data;
    };
    /**
     *
     * loadData
     *
     * Recupera Resultado do metódo getJson().
     *
     */
    HomePage.prototype.loadData = function () {
        return this.getJSON();
    };
    HomePage.prototype.cleanData = function (registros) {
        var d = [];
        for (var i = 0; i <= registros - 1; i++) {
            d.push(this.data[i]);
        }
        this.data = d;
        if (this.data.length === 1) {
            console.log(this.data);
            this.goToApp(this.data[0], false);
        }
    };
    /**
     *
     * sair
     *
     * Exibe opções de sair do aplicativo.
     *
     */
    HomePage.prototype.sair = function () {
        this.loginSrv.showLogoutOptions("Catraca", false, null);
    };
    /* mostra mensagem no badge do square especifico */
    HomePage.prototype.showBadge = function (squareIndex, text) {
        console.log('atualizando badge', squareIndex, text);
        var badgeElement = document.getElementById('badge-' + squareIndex);
        if (badgeElement) {
            badgeElement.innerHTML = text;
            badgeElement.parentElement.hidden = (!parseInt(text) || text == '');
        }
    };
    var _a, _b, _c, _d, _e, _f, _g;
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/brq/TFS_7535_Bitbucket/src/pages/home/home.html"*/'<ion-header>\n  <ion-toolbar color="ons-green">\n    <ion-navbar>\n      <ion-buttons end>\n        <button ion-button icon-only color="primary_20" (click)="sair()">\n          <ion-icon name="log-out"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title text-center>Meu ONS</ion-title>\n    </ion-navbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!--   <ion-grid *ngIf="data == undefined">\n    <ion-row text-center>\n      <ion-col>\n        <ion-spinner name="crescent"></ion-spinner>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n  <!--   <ion-grid *ngIf="data != undefined && data.length < 1">\n    <ion-row text-center>\n      <ion-col>\n        <ion-label>\n          {{ NoAppAllowed }}\n        </ion-label>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n  <!-- *ngIf="data" -->\n  <div class="squares">\n\n    <div class="square" [ngClass]="getSomeClass(item)" [ngStyle]="getMyStyles(item)"\n      *ngFor="let item of data; trackBy: let badgeId = index;">\n      <div class="cardFlip">\n        <div class="cardFlipContainer" [class.is-flipped]="((item.flipcard !== \'\' ) && (flipcardOBJ.isFlipAble))">\n          <div class="cardFlip__face cardFront"\n            [class.transitionBoder]="((item.flipcard !== \'\' ) && (flipcardOBJ.transitionClass))">\n            <div class="header">\n              <div *ngIf="item.DeepLink || item.externalUrl" class="hlink"></div>\n              <div class="hbadge" [ngStyle]="getBadgeStyles(item)">\n                <ion-badge item-end id="badge-{{badgeId}}"></ion-badge>\n              </div>\n            </div>\n            <div class="content">\n              <figure>\n\n                <div *ngIf="item.icone" class="icon">\n                  <img src="{{item.icone}}">\n                </div>\n\n                <p *ngIf="!item.icone">{{item.sigla}}</p>\n                <figcaption *ngIf="item">{{item.nome}}</figcaption>\n\n              </figure>\n            </div>\n          </div>\n          <div class="cardFlip__face cardBack"\n            [class.transitionBoder]="((item.flipcard !== \'\' ) && (flipcardOBJ.transitionClass))">\n            <cardflip></cardflip>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/brq/TFS_7535_Bitbucket/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_quadraros_service__["a" /* QuadradoService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_quadraros_service__["a" /* QuadradoService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["f" /* LoginService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["f" /* LoginService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["i" /* SecurityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["i" /* SecurityService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_utils_service__["a" /* UtilsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_utils_service__["a" /* UtilsService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClient */]) === "function" && _g || Object])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[391]);
//# sourceMappingURL=main.js.map